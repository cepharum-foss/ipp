/**
 * Maps operation names into operation-id values used in IPP request messages.
 */
export enum Operation {
    PrintJob = 0x02,
    PrintURI = 0x03,
    ValidateJob = 0x04,
    CreateJob = 0x05,
    GetPrinterAttributes = 0x0b,
    GetJobs = 0x0a,
    PausePrinter = 0x10,
    ResumePrinter = 0x11,
    PurgeJobs = 0x12,

    SendDocument = 0x06,
    SendURI = 0x07,
    CancelJob = 0x08,
    GetJobAttributes = 0x09,
    HoldJob = 0x0c,
    ReleaseJob = 0x0d,
    RestartJob = 0x0e
}

/**
 * Maps status names into status-code values used in IPP response messages.
 */
export enum Status {
    successfulOk = 0x0000,
    successfulOkIgnoredOrSubstitutedAttributes = 0x0001,
    successfulOkConflictingAttributes = 0x0002,

    clientErrorBadRequest = 0x0400,
    clientErrorForbidden = 0x0401,
    clientErrorNotAuthenticated = 0x0402,
    clientErrorNotAuthorized = 0x0403,
    clientErrorNotPossible = 0x0404,
    clientErrorTimeout = 0x0405,
    clientErrorNotFound = 0x0406,
    clientErrorGone = 0x0407,
    clientErrorRequestEntityTooLarge = 0x0408,
    clientErrorRequestValueTooLong = 0x0409,
    clientErrorDocumentFormatNotSupported = 0x040a,
    clientErrorAttributesOrValuesNotSupported = 0x040b,
    clientErrorUriSchemeNotSupported = 0x040c,
    clientErrorCharsetNotSupported = 0x040d,
    clientErrorConflictingAttributes = 0x040e,
    clientErrorCompressionNotSupported = 0x040f,
    clientErrorCompressionError = 0x0410,
    clientErrorDocumentFormatError = 0x0411,
    clientErrorDocumentAccessError = 0x0412,

    serverErrorInternalError = 0x0500,
    serverErrorOperationNotSupported = 0x0501,
    serverErrorServiceUnavailable = 0x0502,
    serverErrorVersionNotSupported = 0x0503,
    serverErrorDeviceError = 0x0504,
    serverErrorTemporaryError = 0x0505,
    serverErrorNotAcceptingJobs = 0x0506,
    serverErrorBusy = 0x0507,
    serverErrorJobCanceled = 0x0508,
    serverErrorMultipleDocumentJobsNotSupported = 0x0509
}

/**
 * Maps identifier addressing group of attributes into human-readable name of
 * either group. Special _name_ `null` is used to mark end-of-groups identifier.
 */
export const AttributeGroup: { [key:number]: ( string | null ) };

/**
 * Maps type names of attribute values into either type's identifier for use
 * in encoded IPP messages.
 */
export enum AttributeType {
    unsupported = 0x10,
    default = 0x11,
    unknown = 0x12,
    noValue = 0x13,

    integer = 0x21,
    boolean = 0x22,
    enum = 0x23,

    octetString = 0x30,
    dateTime = 0x31,
    resolution = 0x32,
    rangeOfInteger = 0x33,
    textWithLanguage = 0x35,
    nameWithLanguage = 0x36,

    textWithoutLanguage = 0x41,
    nameWithoutLanguage = 0x42,
    keyword = 0x44,
    uri = 0x45,
    uriScheme = 0x46,
    charset = 0x47,
    naturalLanguage = 0x48,
    mimeMediaType = 0x49
}

/**
 * Provides enumeration suitable for creating `printer-state` values.
 */
export const EnumPrinterStates: string[];

/**
 * Provides enumeration suitable for creating `operations-supported` values.
 */
export const EnumOperations: string[];

/**
 * Manages single IPP message.
 */
export class IPPMessage {
    /**
     * Exposes operation-id in request messages and status-code in response
     * messages.
     */
    code: number;

    /**
     * Exposes request-id suitable for associating response messages with their
     * request messages.
     */
    id: number;

    /**
     * Provides IPP message version identifier in human-readable format,
     * e.g. "1.1".
     */
    version: string;

    /**
     * Exposes grouped sets of attributes.
     */
    attributes: {
        /**
         * Exposes all operation-related attributes.
         */
        operation: GuidedAttributes;

        /**
         * Exposes all printer-related attributes.
         */
        printer: GuidedAttributes;

        /**
         * Exposes all job-related attributes.
         */
        job: GuidedAttributes;

        /**
         * Exposes all attributes not particularly related to the operation, some
         * printer or job.
         */
        unsupported: GuidedAttributes;
    };

    /**
     * Exposes body data considered payload of IPP message.
     */
    data: Buffer;

    /**
     * Creates IPP request message.
     *
     * @param operationName name of requested IPP operation
     */
    static createRequest( operationName: string ): IPPMessage;

    /**
     * Maps provided operation ID into operation name.
     *
     * @param code operation-id
     */
    static getOperationName( code: number ): string;

    /**
     * Derives IPP response message matching current request.
     *
     * @param statusCode status-code
     * @param statusText short description of status
     */
    deriveResponse( statusCode: number, statusText?: string ): IPPMessage;

    /**
     * Sets (another) value for selected attribute.
     *
     * @param groupName name of attribute group
     * @param name name of attribute
     * @param value value to set
     * @param add set true if value should be appended to existing list of values
     */
    setAttribute( groupName: string, name: string, value: ( Type | any ), add?: boolean ): IPPMessage;

    /**
     * Sets (another) value for selected operation-related attribute.
     *
     * @param name name of attribute
     * @param value value to set
     * @param add set true if value should be appended to existing list of values
     */
    setOperationAttribute( name: string, value: ( Type | any ), add?: boolean ): IPPMessage;

    /**
     * Sets (another) value for selected printer-related attribute.
     *
     * @param name name of attribute
     * @param value value to set
     * @param add set true if value should be appended to existing list of values
     */
    setPrinterAttribute( name: string, value: ( Type | any ), add?: boolean ): IPPMessage;

    /**
     * Sets (another) value for selected job-related attribute.
     *
     * @param name name of attribute
     * @param value value to set
     * @param add set true if value should be appended to existing list of values
     */
    setJobAttribute( name: string, value: ( Type | any ), add?: boolean ): IPPMessage;

    /**
     * Sets (another) value for selected attribute not particularly related to
     * the operation, some printer of job.
     *
     * @param name name of attribute
     * @param value value to set
     * @param add set true if value should be appended to existing list of values
     */
    setUnsupportedAttribute( name: string, value: ( Type | any ), add?: boolean ): IPPMessage;

    /**
     * Generates binary-encoded representation of current message.
     */
    toBuffer(): Buffer;
}

/**
 * Implements duplex stream reading raw IPP message on its input side and
 * delivering payload following discovered IPP Message on its output side.
 */
export class IPPMessageStreamParser {}

/**
 * Represents single typed value of IPP message attribute.
 */
export abstract class Type {
    constructor( type: number, octetLimit?: number );

    /**
     * Exposes immutable type identifier as defined in RFC-2911.
     */
    readonly type: number;

    /**
     * Exposes maximum number of octets applied on encoded value in compliance
     * with RFC 2911.
     */
    octetLimit: number;

    /**
     * Reduces limit on size of encoded value.
     *
     * @param octetLimit new limit applied on number of octets when encoding value
     */
    limit( octetLimit: number ): Type;

    /**
     * Creates buffer containing typed value in its binary encoded form.
     */
    toBuffer(): Buffer;

    /**
     * Detects if current value is equal provided one.
     *
     * @param value value to compare with
     * @param context additional information on context, e.g. providing default locale
     * @returns true if current and provided value are considered equal
     */
    equals( value: any, context?: object ): boolean;
}

/**
 * Implements representation of _default_ value.
 */
export class TypeDefault extends Type {}

/**
 * Implements representation of _unset_ value.
 */
export class TypeNoValue extends Type {}

/**
 * Implements representation of _unknown_ value.
 */
export class TypeUnknown extends Type {}

/**
 * Implements representation of integer value in IPP attributes.
 */
export class TypeInteger extends Type {
    constructor( value: any );

    /**
     * Exposes wrapped integer value.
     */
    value: number;
}

/**
 * Implements representation of enumeration value in IPP attributes.
 */
export class TypeEnum extends Type {
    constructor( valueOrIndex: any, enumeration: string[] );

    /**
     * Exposes value picked out of enumeration of values.
     */
    value: string;

    /**
     * Exposes number of current value into sorted set of accepted values.
     */
    index: number;

    /**
     * Exposes sorted set of accepted values.
     */
    enumeration: string[];
}

/**
 * Implements representation of boolean value in IPP attributes.
 */
export class TypeBoolean extends Type {
    constructor( value: any );

    /**
     * Exposes wrapped boolean value.
     */
    value: boolean;
}

/**
 * Implements representation of numeric range of values.
 */
export class TypeRangeOfInteger extends Type {
    constructor( lower: number, upper: number );

    /**
     * Exposes lower boundary of range.
     */
    lower: number;

    /**
     * Exposes upper boundary of range.
     */
    upper: number;
}

/**
 * Implements representation of binary octet string.
 */
export class TypeOctetString extends Type {
    constructor( value: any, octetLimit?: number );

    /**
     * Exposes binary octet string.
     */
    value: Buffer;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of two-dimensional resolution.
 */
export class TypeResolution extends Type {
    constructor( x: number, y: number, unit: ResolutionUnit );

    /**
     * Exposes horizontal resolution value.
     */
    x: number;

    /**
     * Exposes vertical resolution value.
     */
    y: number;

    /**
     * Exposes unit of either resolution value-
     */
    unit: ResolutionUnit;

    /**
     * Exposes opaque unit value indicating resolution given in dots per inch.
     */
    static PerInch: number;

    /**
     * Exposes opaque unit value indicating resolution given in dots per cm.
     */
    static PerCm: number;
}

/**
 * Lists supported units for resolution values.
 */
export enum ResolutionUnit {
    /** Resolution is in dots per inch. */
    PerInch = 3,

    /** Resolution is in dots per cm. */
    PerCm = 4
}

/**
 * Implements representation of timestamp.
 */
export class TypeDateTime extends Type {
    constructor( timestamp: string | number | Date );

    /**
     * Exposes timestamp value.
     */
    value: Date;
}

/**
 * Implements representation of localized string combined with information on
 * its locale.
 */
export class TypeTextWithLanguage extends Type {
    constructor( string: string, language: string, octetLimit?: number );

    /**
     * Exposes localized string.
     */
    text: string;

    /**
     * Exposes localized string. This is an alias for TypeTextWithLanguage#text.
     */
    value: TypeTextWithoutLanguage;

    /**
     * Exposes information on string's locale.
     */
    language: string;
}

/**
 * Implements representation of string localized according to some default
 * locale managed separately.
 */
export class TypeTextWithoutLanguage extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes localized string.
     */
    text: string;

    /**
     * Exposes localized string. This is an alias for
     * TypeTextWithoutLanguage#text.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of localized string considered a name combined with
 * information on its locale.
 */
export class TypeNameWithLanguage extends Type {
    constructor( string: string, language: string, octetLimit?: number );

    /**
     * Exposes localized string.
     */
    name: string;

    /**
     * Exposes localized string. This is an alias for TypeTextWithLanguage#name.
     */
    value: TypeNameWithLanguage;

    /**
     * Exposes information on string's locale.
     */
    language: string;
}

/**
 * Implements representation of string considered a name localized according to
 * some default locale managed separately.
 */
export class TypeNameWithoutLanguage extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes localized string.
     */
    name: string;

    /**
     * Exposes localized string. This is an alias for
     * TypeTextWithoutLanguage#name.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of value of type _keyword_.
 */
export class TypeKeyword extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes keyword.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of value of type _charset_.
 */
export class TypeCharset extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes charset.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of value of type _uri_.
 */
export class TypeUri extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes normalized URI.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;

    /**
     * Exposes scheme of normalized URI as defined in RFC 3986.
     */
    scheme: string;

    /**
     * Exposes authority of normalized URI as defined in RFC 3986.
     */
    authority: string;

    /**
     * Exposes userinfo of normalized URI as defined in RFC 3986.
     */
    userinfo: string;

    /**
     * Exposes host of normalized URI as defined in RFC 3986.
     */
    host: string;

    /**
     * Exposes port of normalized URI as defined in RFC 3986.
     */
    port: string;

    /**
     * Exposes path of normalized URI as defined in RFC 3986.
     */
    path: string;

    /**
     * Exposes query of normalized URI as defined in RFC 3986.
     */
    query: string;

    /**
     * Exposes fragment of normalized URI as defined in RFC 3986.
     */
    fragment: string;
}

/**
 * Implements representation of value of type _uriScheme_.
 */
export class TypeUriScheme extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes normalized URI.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Implements representation of value of type _mimeMediaType_.
 */
export class TypeMimeMediaType extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes MIME information.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;

    /**
     * Exposes type component of media type information.
     */
    major: string;

    /**
     * Exposes subtype component of media type information.
     */
    minor: string;

    /**
     * Exposes parameters optionally included with media type information.
     */
    parameters: { [ key: string ]: string };
}

/**
 * Implements representation of value of type _naturalLanguage_.
 */
export class TypeNaturalLanguage extends Type {
    constructor( string: string, octetLimit?: number );

    /**
     * Exposes natural language.
     */
    value: string;

    /**
     * Exposes defined limit on number of octets consumed on encoding current
     * value.
     *
     * This limit may be reduced.
     */
    octetLimit: number;
}

/**
 * Parses raw encoded IPP message returning its description.
 *
 * @param rawMessage raw encoded IPP message to be parsed
 */
export function parse( rawMessage: Buffer ): IPPMessage;

/**
 * Creates instance of stream parser extracting IPP message from stream
 * forwarding any payload succeeding extracted IPP message.
 */
export function getParsingStream(): IPPMessageStreamParser;

/**
 * Generates request handler for use with _express_ and compatible server
 * frameworks to discover and parse IPP messages in incoming requests.
 */
export function middleware(): ( req: object, res: object, next: ( error?: Error ) => void ) => void;

/**
 * Implements pool of named (homogenic sets of) values with each value
 */
declare class GuidedAttributes extends Map<string,(Type|Type[])> {
    /**
     * Adds another value to named attribute.
     *
     * @param {string} key name of attribute to extend, gets created if missing
     * @param {Type} value value to add, must of same type as any existing value
     */
    add( key: string, value: Type ): GuidedAttributes;

    /**
     * Removes value matching provided one from named attribute.
     *
     * @param {string} key name of attribute to extend, gets created if missing
     * @param {Type} value value to be equivalent with the one to be removed
     */
    remove( key: string, value: Type ): GuidedAttributes;
}

/**
 * Fetches current value of named global option.
 *
 * @param name name of option to fetch
 */
export function getGlobalOption( name: string ): any;

/**
 * Adjusts value of named global option.
 *
 * @param name name of option to adjust
 * @param value new value of named option
 */
export function setGlobalOption( name: string, value: any ): void;
