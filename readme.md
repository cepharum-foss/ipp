# IPP Message Processor

[![pipeline status](https://gitlab.com/cepharum-foss/ipp/badges/master/pipeline.svg)](https://gitlab.com/cepharum-foss/ipp/-/commits/master)
[![coverage report](https://gitlab.com/cepharum-foss/ipp/badges/master/coverage.svg)](https://gitlab.com/cepharum-foss/ipp/-/commits/master)

(c) cepharum GmbH, http://cepharum.de

## License

MIT

## About

This library provides parsing and generation of RFC8011 IPP messages. In addition it contains code for parsing IPP message from stream forwarding all body data following parsed IPP message to be processed separately, e.g. for storing or instantly processing it. Eventually, there is a middleware for use with express or compatible server applications to conveniently discover IPP messages in incoming requests.

## Install

```bash
npm i @cepharum/ipp
```

## Usage

```javascript
const { IPPMessage } = require( "@cepharum/ipp" );

const parsed = new IPPMessage( rawMessage );
const response = parsed.deriveResponse();
const encoded = response.toBuffer();
```

It comes with a middleware generator suitable for use with [Express server-side framework](https://expressjs.com/):

```javascript
const IPP = require( "@cepharum/ipp" );

app.use( IPP.middleware() );

app.post( ( req, res ) => {
    // req.body.message is instance of IPPMessage here
    // req.body.data is a Readable stream providing payload of IPP message
} );
```

The same middleware generator is suitable for injecting into [Hitchy server-side framework](https://hitchyjs.github.io/) as well:

```javascript
const IPP = require( "@cepharum/ipp" );

exports.policies = {
    "POST /": IPP.middleware(),
};

exports.routes = {
    "POST /"( req, res ) {
        // this.local.ipp.message is instance of IPPMessage here
        // this.local.ipp.data is a Readable stream providing payload of IPP message
    }
};
```

### Relax Syntax Rules

When implementing IPP services there are some situations when RFC 8011 is [contradictory in itself, when it comes to enforcing syntax rules on keywords in section](https://www.rfc-editor.org/errata/eid6085). By supporting global options you can put this library into relaxed mode:

```javascript
const IPP = require( "@cepharum/ipp" );

IPP.setGlobalOption( "relaxKeywordSyntax", true );

...
```
