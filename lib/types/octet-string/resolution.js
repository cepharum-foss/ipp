/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const { AttributeType, MaxInteger } = require( "../../data" );

/**
 * Represents attribute's value of type "resolution" consisting of resolution
 * on x- and y-axis and some unit information.
 */
class TypeResolution extends Type {
	/**
	 * @param {int} x horizontal resolution
	 * @param {?int} y vertical resolution, omit to use same as horizontal resolution
	 * @param {int} unit unit identifier, either TypeResolution.PerInch [default] or TypeResolution.PerCm
	 */
	constructor( x, y = null, unit = null ) {
		let _x, _y, _unit;

		super( AttributeType.resolution );

		Object.defineProperties( this, {
			/**
			 * Exposes horizontal resolution value.
			 *
			 * @name TypeResolution#x
			 * @property {int}
			 */
			x: {
				get: () => _x,
				set: value => {
					const _value = value instanceof TypeResolution ? value.x : value instanceof Type ? value.value : value;
					const asInteger = parseInt( _value );

					if ( isNaN( asInteger ) || asInteger < 1 || asInteger > MaxInteger ) {
						throw new TypeError( "invalid resolution value" );
					}

					_x = asInteger;
				},
				enumerable: true,
			},

			/**
			 * Exposes vertical resolution value.
			 *
			 * @name TypeResolution#y
			 * @property {int}
			 */
			y: {
				get: () => _y,
				set: value => {
					const _value = value instanceof TypeResolution ? value.y : value instanceof Type ? value.value : value;
					const asInteger = parseInt( _value );

					if ( isNaN( asInteger ) || asInteger < 1 || asInteger > MaxInteger ) {
						throw new TypeError( "invalid resolution value" );
					}

					_y = asInteger;
				},
				enumerable: true,
			},

			/**
			 * Exposes unit of both resolution values.
			 *
			 * @name TypeResolution#x
			 * @property {int}
			 */
			unit: {
				get: () => _unit,
				set: value => {
					const _value = value instanceof TypeResolution ? value.unit : value instanceof Type ? value.value : value;

					if ( _value !== TypeResolution.PerInch && _value !== TypeResolution.PerCm ) {
						throw new TypeError( "invalid resolution unit" );
					}

					_unit = _value;
				},
				enumerable: true,
			},
		} );

		this.x = x;
		this.y = y == null ? x : y;
		this.unit = unit == null ? x instanceof TypeResolution ? x.unit : TypeResolution.PerInch : unit;
	}

	/** @inheritDoc */
	toString() {
		return `[resolution ${this.x}x${this.y}${this.unit === TypeResolution.PerCm ? "dpcm" : "dpi"}]`;
	}

	/** @inheritDoc */
	toBuffer() {
		const data = Buffer.alloc( 9 );

		data.writeInt32BE( this.x, 0 );
		data.writeInt32BE( this.y, 4 );
		data.writeInt8( this.unit, 8 );

		return data;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		if ( !super.equals( instance, context ) ) {
			return false;
		}

		return this.x === instance.x && this.y === instance.y && this.unit === instance.unit;
	}
}

Object.defineProperties( TypeResolution, {
	/**
	 * Selects resolution value in dots per inch.
	 *
	 * @name TypeResolution.PerInch
	 * @property {int}
	 * @readonly
	 */
	PerInch: { value: 3, enumerable: true },

	/**
	 * Selects resolution value in dots per centimeter.
	 *
	 * @name TypeResolution.PerCm
	 * @property {int}
	 * @readonly
	 */
	PerCm: { value: 4, enumerable: true },
} );

module.exports = TypeResolution;
