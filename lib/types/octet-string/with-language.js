/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const TypeTextWithoutLanguage = require( "../string/text-without-language" );
const TypeNameWithoutLanguage = require( "../string/name-without-language" );
const TypeNaturalLanguage = require( "../string/natural-language" );
const { AttributeType: { nameWithLanguage, textWithLanguage }, AttributeTypeGroup } = require( "../../data" );

/**
 * Represents *WithLanguage values in attributes.
 */
class TypeStringWithLanguage extends Type {
	/**
	 * @param {int} type type identifier
	 * @param {TypeString} string representation of localized string
	 * @param {string} language locale of represented string
	 */
	constructor( type, string, language ) {
		super( type, string.octetLimit );

		const _language = new TypeNaturalLanguage( language );

		Object.defineProperties( this, {
			/**
			 * Exposes inner instance managing *WithoutLocale instance.
			 *
			 * @name TypeStringWithLanguage#_inner
			 * @property {TypeString}
			 * @readonly
			 * @protected
			 */
			_inner: { value: string },

			/**
			 * Exposes inner instance managing naturalLanguage instance.
			 *
			 * @name TypeStringWithLanguage#_language
			 * @property {TypeNaturalLanguage}
			 * @readonly
			 * @protected
			 */
			_language: { value: _language },

			/**
			 * Exposes localized string value.
			 *
			 * @name TypeStringWithLanguage#value
			 * @property {string}
			 */
			value: {
				get: () => string.value,
				set: newValue => {
					string.value = newValue;
				},
				enumerable: true,
			},

			/**
			 * Exposes naturalLanguage-type value selecting locale of localized
			 * string value.
			 *
			 * @name TypeStringWithLanguage#language
			 * @property {TypeNaturalLanguage}
			 */
			language: {
				get: () => _language.value,
				set: newLanguage => {
					_language.value = newLanguage;
				},
				enumerable: true,
			},
		} );

		_language.value = language;

		const replaced = string._onReduceOctetLimit;
		string._onReduceOctetLimit = () => {
			replaced.call( string );

			this.octetLimit = string.octetLimit;
		};
	}

	/** @inheritDoc */
	toBuffer() {
		const language = this._language.toBuffer();
		const string = this._inner.toBuffer();

		const languageLength = Buffer.alloc( 2 );
		const stringLength = Buffer.alloc( 2 );

		languageLength.writeInt16BE( language.length );
		stringLength.writeInt16BE( string.length );

		return Buffer.concat( [ languageLength, language, stringLength, string ] );
	}

	/** @inheritDoc */
	toString() {
		return `[${this.constructor.name.replace( /^Type(.)/, ( _, l ) => l.toLowerCase() )}(${this.value.length}) ${this.value.replace( /^(.{48}).+$/, ( _, text ) => text.slice( 0, -3 ) + "..." )} (${this.language})]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		let _instance;

		try {
			_instance = instance instanceof Type ? instance : new this.constructor( instance, context.defaultLanguage );
		} catch ( error ) {
			return false;
		}

		if ( _instance instanceof TypeStringWithLanguage ) {
			return this._inner.equals( _instance._inner ) && this._language.equals( _instance.language );
		}

		const myGroup = AttributeTypeGroup[this.type];
		const itsGroup = AttributeTypeGroup[_instance.type];

		if ( myGroup === itsGroup ) {
			return this._inner.equals( _instance ) && this._language.equals( context.defaultLanguage );
		}

		return false;
	}

	/** @inheritDoc */
	_onReduceOctetLimit() {
		if ( this._inner ) {
			this._inner.octetLimit = this.octetLimit;
		}
	}
}

/**
 * Represents text value in attributes with its natural language different from
 * some operation's default.
 */
class TypeTextWithLanguage extends TypeStringWithLanguage {
	/**
	 * @param {string} text initial text value
	 * @param {string} language natural language of provided text
	 * @param {int} octetLimit limit on number of encoded octets
	 */
	constructor( text, language, octetLimit = Infinity ) {
		super( textWithLanguage, new TypeTextWithoutLanguage( text, octetLimit ), language );

		Object.defineProperties( this, {
			/**
			 * Exposes value of internally managed textWithoutLanguage.
			 *
			 * @name TypeTextWithLanguage#text
			 * @property {string}
			 */
			text: {
				get: () => this.value,
				set: newValue => {
					this.value = newValue;
				},
				enumerable: true,
			},
		} );
	}
}

/**
 * Represents name value in attributes with its natural language different from
 * some operation's default.
 */
class TypeNameWithLanguage extends TypeStringWithLanguage {
	/**
	 * @param {string} name initial name value
	 * @param {string} language natural language of provided name
	 * @param {int} octetLimit limit on number of encoded octets
	 */
	constructor( name, language, octetLimit = Infinity ) {
		super( nameWithLanguage, new TypeNameWithoutLanguage( name, octetLimit ), language );

		Object.defineProperties( this, {
			/**
			 * Exposes value of internally managed nameWithoutLanguage.
			 *
			 * @name TypeTextWithLanguage#name
			 * @property {string}
			 */
			name: {
				get: () => this.value,
				set: newValue => {
					this.value = newValue;
				},
				enumerable: true,
			},
		} );
	}
}


module.exports = {
	TypeTextWithLanguage,
	TypeNameWithLanguage,
};
