/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const { AttributeType, MinInteger, MaxInteger } = require( "../../data" );

/**
 * Represents attribute's value of type "resolution".
 */
class TypeRangeOfInteger extends Type {
	/**
	 * @param {int} lower lower limit
	 * @param {int} upper upper limit
	 */
	constructor( lower, upper ) {
		super( AttributeType.rangeOfInteger );

		let _lower, _upper;

		Object.defineProperties( this, {
			/**
			 * Exposes lower limit of range.
			 *
			 * @name TypeRangeOfInteger#lower
			 * @property {int}
			 */
			lower: {
				get: () => _lower,
				set: newLimit => {
					const _newLimit = newLimit instanceof TypeRangeOfInteger ? newLimit.lower : newLimit instanceof Type ? newLimit.value : newLimit;
					const normalized = parseInt( _newLimit );
					if ( isNaN( normalized ) ) {
						throw new TypeError( "invalid type of lower limit" );
					}

					if ( normalized < MinInteger || normalized > MaxInteger ) {
						throw new TypeError( "lower limit out of range" );
					}

					if ( _upper != null && normalized > _upper ) {
						this.upper = normalized;
					} else {
						_lower = normalized;
					}
				},
				enumerable: true,
			},

			/**
			 * Exposes upper limit of range.
			 *
			 * @name TypeRangeOfInteger#upper
			 * @property {int}
			 */
			upper: {
				get: () => _upper,
				set: newLimit => {
					const _newLimit = newLimit instanceof TypeRangeOfInteger ? newLimit.upper : newLimit instanceof Type ? newLimit.value : newLimit;
					const normalized = parseInt( _newLimit );
					if ( isNaN( normalized ) ) {
						throw new TypeError( "invalid type of upper limit" );
					}

					if ( normalized < MinInteger || normalized > MaxInteger ) {
						throw new TypeError( "upper limit out of range" );
					}

					if ( _lower != null && normalized < _lower ) {
						this.lower = normalized;
					} else {
						_upper = normalized;
					}
				},
				enumerable: true,
			},
		} );

		this.lower = lower > upper ? upper : lower;
		this.upper = lower > upper ? lower : upper;
	}

	/** @inheritDoc */
	toBuffer() {
		const data = Buffer.alloc( 8 );

		data.writeInt32BE( this.lower, 0 );
		data.writeInt32BE( this.upper, 4 );

		return data;
	}

	/** @inheritDoc */
	toString() {
		return `[rangeOfInteger ${this.lower}-${this.upper}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		if ( !super.equals( instance, context ) ) {
			return false;
		}

		return this.lower === instance.lower && this.upper === instance.upper;
	}
}

module.exports = TypeRangeOfInteger;
