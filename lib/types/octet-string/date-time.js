/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const { AttributeType } = require( "../../data" );

/**
 * Represents dateTime-type values in attributes.
 */
class TypeDateTime extends Type {
	/**
	 * @param {*} value initial value
	 */
	constructor( value ) {
		super( AttributeType.dateTime );

		let _value;

		Object.defineProperties( this, {
			/**
			 * Exposes wrapped dateTime value.
			 *
			 * @name TypeDateTime#value
			 * @property {*}
			 */
			value: {
				get: () => _value,
				set: newValue => {
					const _newValue = newValue instanceof Type ? newValue.value : newValue;
					const newType = typeof _newValue;

					switch ( newType ) {
						case "object" :
							if ( _newValue instanceof Date ) {
								_value = _newValue;
								break;
							}

						// falls through
						case "function" :
						case "boolean" :
							throw new TypeError( `cannot cast to Date: ${newType}` );

						case "undefined" :
							throw new TypeError( "value must be defined" );

						case "string" :
							if ( /^\s*[+-]?\d+(?:\.\d+)?\s*$/.test( _newValue ) ) {
								_value = new Date( parseInt( _newValue ) * 1000 );
							} else {
								_value = new Date( _newValue );
								if ( isNaN( _value ) ) {
									throw new TypeError( `cannot cast string to Date: ${_newValue}` );
								}
							}
							break;

						case "number" :
							_value = new Date( parseInt( _newValue ) * 1000 );
					}
				},
				enumerable: true,
			}
		} );

		this.value = value == null ? new Date() : value;
	}

	/** @inheritDoc */
	toBuffer() {
		const { value } = this;
		const data = Buffer.alloc( 11 );

		data.writeUInt16BE( value.getFullYear(), 0 );
		data.writeUInt8( value.getMonth() + 1, 2 );
		data.writeUInt8( value.getDate(), 3 );
		data.writeUInt8( value.getHours(), 4 );
		data.writeUInt8( value.getMinutes(), 5 );
		data.writeUInt8( value.getSeconds(), 6 );
		data.writeUInt8( Math.floor( value.getMilliseconds() / 100 ), 7 );

		const shift = value.getTimezoneOffset();

		data.write( shift < 0 ? "+" : "-", 8, 1, "ascii" );
		data.writeUInt8( Math.floor( Math.abs( shift ) / 60 ), 9 );
		data.writeUInt8( Math.abs( shift ) % 60, 10 );

		return data;
	}

	/** @inheritDoc */
	toString() {
		return `[dateTime ${this.value.toISOString()}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		if ( !super.equals( instance, context ) ) {
			return false;
		}

		return this.value.getTime() === instance.value.getTime();
	}
}

module.exports = TypeDateTime;
