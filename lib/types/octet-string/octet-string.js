/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const { AttributeType: { octetString } } = require( "../../data" );

/**
 * Represents octetString values in attributes.
 */
class TypeOctetString extends Type {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit limit on number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( octetString, octetLimit );

		let _value;

		Object.defineProperties( this, {
			/**
			 * Exposes wrapped octetString value.
			 *
			 * @name TypeOctetString#value
			 * @property {*}
			 */
			value: {
				get: () => _value,
				set: newValue => {
					let _newValue = newValue instanceof Type ? newValue.value : newValue;
					const newType = typeof _newValue;

					switch ( newType ) {
						case "object" :
							if ( Buffer.isBuffer( _newValue ) ) {
								break;
							}

						// falls through
						case "boolean" :
						case "number" :
						case "function" :
							throw new TypeError( `cannot cast to octetString: ${newType}` );

						case "undefined" :
							throw new TypeError( "value must be defined" );

						case "string" :
							_newValue = Buffer.from( _newValue, "utf8" );
							break;
					}

					if ( _newValue.length > this.octetLimit ) {
						throw new TypeError( "octet string too string" );
					}

					_value = _newValue;
				},
				enumerable: true,
			}
		} );

		this.value = value;
	}

	/** @inheritDoc */
	toBuffer() {
		return this.value;
	}

	/** @inheritDoc */
	toString() {
		return `[octetString(${this.value.length}) ${this.value.toString( "hex" ).replace( /^(.{48}).+$/, ( _, text ) => text.slice( 0, -3 ) + "..." )}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		return _instance && Buffer.isBuffer( _instance.value ) && this.value.equals( _instance.value );
	}
}

module.exports = TypeOctetString;
