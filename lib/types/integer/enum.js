/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const TypeKeyword = require( "../string/keyword" );
const TypeInteger = require( "./integer" );
const { AttributeType, MaxInteger } = require( "../../data" );

/**
 * Represents enum values in attributes.
 */
class TypeEnum extends Type {
	/**
	 * @param {string|int} value initial value or its index into list of accepted values
	 * @param {string[]} enumeration list of accepted values
	 */
	constructor( value, enumeration ) {
		super( AttributeType.enum );

		let _value, _enumeration;

		Object.defineProperties( this, {
			/**
			 * Exposes wrapped enum value.
			 *
			 * @name TypeEnum#value
			 * @property {*}
			 */
			value: {
				get: () => _enumeration[_value - 1],
				set: newValue => {
					if ( newValue instanceof TypeEnum && newValue.enumeration == null ) {
						throw new TypeError( "rejecting enumeration-type value lacking accepted values" );
					}

					if ( newValue instanceof TypeInteger ) {
						throw new TypeError( "rejecting integer-type value for missing related value" );
					}

					const _newValue = newValue instanceof Type ? newValue.value : newValue;
					let _index;

					if ( typeof _newValue === "number" ) {
						if ( _value != null ) {
							throw new TypeError( "adjusting index into enumeration rejected, assign value instead" );
						}

						_index = parseInt( _newValue );
					} else if ( Array.isArray( _enumeration ) ) {
						_index = _enumeration.indexOf( _newValue );
						if ( _index < 0 ) {
							throw new TypeError( "no such value in enumeration" );
						}

						_index++;
					} else {
						throw new TypeError( "missing definition of enumeration" );
					}

					if ( _index < 1 || ( Array.isArray( _enumeration ) && _index > _enumeration.length ) ) {
						throw new TypeError( "invalid index of enumeration value" );
					}

					_value = _index;
				},
				enumerable: true,
			},

			/**
			 * Exposes index of current value int provided set of enumerated
			 * values.
			 *
			 * @name TypeEnum#index
			 * @property {int}
			 * @readonly
			 */
			index: {
				get: () => _value,
			},

			/**
			 * Exposes enumeration of accepted values.
			 *
			 * @note This enumeration can't be replaced. However, it is possible
			 *       to provide it late when it wasn't available on creating
			 *       this instance of TypeEnum.
			 *
			 * @name TypeEnum#enumeration
			 * @property {string[]}
			 */
			enumeration: {
				get: () => ( _enumeration ? _enumeration.slice( 0 ) : null ),
				set: newEnumeration => {
					if ( _enumeration ) {
						throw new TypeError( "replacing existing enumeration rejected" );
					}

					let _newEnumeration;

					if ( newEnumeration instanceof TypeEnum ) {
						_newEnumeration = newEnumeration.enumeration ? newEnumeration.enumeration : {};
					} else {
						_newEnumeration = newEnumeration;

						this.constructor.checkEnumeration( _newEnumeration, true );
					}

					if ( _value > _newEnumeration.length ) {
						throw new TypeError( "enumeration does not cover selected value" );
					}

					_enumeration = _newEnumeration;
				},
				enumerable: false,
			}
		} );

		if ( enumeration ) {
			this.enumeration = enumeration;
		} else if ( value instanceof TypeEnum ) {
			this.enumeration = value;
		}

		this.value = value;
	}

	/** @inheritDoc */
	toBuffer() {
		const data = Buffer.alloc( 4 );
		data.writeInt32BE( this.index, 0 );

		return data;
	}

	/** @inheritDoc */
	toString() {
		return `[enum ${this.enumeration ? this.value : "#" + this.index}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		const value = this._wrap( instance );

		if ( !value || !super.equals( value, context ) ) {
			return false;
		}

		return this.value === value.value;
	}

	/**
	 * Checks if provided set of values is complying with constraints on
	 * enumeration values.
	 *
	 * @param {string[]} enumeration set of values to check
	 * @param {boolean} thorough set true to inspect every listed item in particular
	 * @returns {void}
	 * @throws TypeError on invalid enumeration
	 */
	static checkEnumeration( enumeration, thorough = false ) {
		if ( !Array.isArray( enumeration ) ) {
			throw new TypeError( "missing enumeration of accepted values" );
		}

		if ( enumeration.length < 1 ) {
			throw new TypeError( "enumeration of accepted values must not be empty" );
		}

		if ( enumeration.length > MaxInteger ) {
			throw new TypeError( "enumeration of accepted values too huge" );
		}

		if ( thorough ) {
			const numValues = enumeration.length;

			for ( let i = 0; i < numValues; i++ ) {
				const value = enumeration[i];

				if ( typeof value !== "string" || !TypeKeyword.pattern.test( value ) ) {
					throw new TypeError( `enumerated value #${i + 1} is not a keyword` );
				}
			}
		}
	}
}

module.exports = TypeEnum;
