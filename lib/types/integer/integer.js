/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );
const { AttributeType, MaxInteger, MinInteger } = require( "../../data" );

/**
 * Represents integer values in attributes.
 */
class TypeInteger extends Type {
	/**
	 * @param {*} value initial value
	 */
	constructor( value ) {
		super( AttributeType.integer );

		let _value;

		Object.defineProperties( this, {
			/**
			 * Exposes wrapped integer value.
			 *
			 * @name TypeInteger#value
			 * @property {*}
			 */
			value: {
				get: () => _value,
				set: newValue => {
					let _newValue = newValue instanceof Type ? newValue.value : newValue;

					const newType = typeof _newValue;

					switch ( newType ) {
						case "object" :
						case "function" :
						case "boolean" :
							throw new TypeError( `cannot cast to integer: ${newType}` );

						case "undefined" :
							throw new TypeError( "value must be defined" );
					}

					_newValue = parseInt( _newValue );

					if ( isNaN( _newValue ) ) {
						throw new TypeError( "integer value is not a number" );
					}

					if ( _newValue < MinInteger || _newValue > MaxInteger ) {
						throw new TypeError( "integer value out of range" );
					}

					_value = _newValue;
				},
				enumerable: true,
			}
		} );

		this.value = value;
	}

	/** @inheritDoc */
	toBuffer() {
		const { value } = this;

		const data = Buffer.alloc( 4 );
		data.writeInt32BE( value, 0 );

		return data;
	}

	/** @inheritDoc */
	toString() {
		return `[integer ${this.value}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) {
		const value = this._wrap( instance );

		if ( !value || !super.equals( value, context ) ) {
			return false;
		}

		return this.value === value.value;
	}
}

module.exports = TypeInteger;
