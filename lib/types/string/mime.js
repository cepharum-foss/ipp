/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const TypeString = require( "./string" );
const { AttributeType: { mimeMediaType } } = require( "../../data" );


/**
 * Represents mimeMediaType-type values in attributes.
 */
class TypeMimeMediaType extends TypeString {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( mimeMediaType, value, octetLimit );

		Object.defineProperties( this, {
			/**
			 * Exposes currently selected media type. Value is always converted
			 * to lowercase.
			 *
			 * @name TypeMimeMediaType#major
			 * @property {string}
			 * @readonly
			 */
			major: {
				get: () => this.constructor.pattern.exec( this.value )[1].toLowerCase(),
				enumerable: true,
			},

			/**
			 * Exposes subtype of currently selected media type. Value is always
			 * converted to lowercase.
			 *
			 * @name TypeMimeMediaType#minor
			 * @property {string}
			 * @readonly
			 */
			minor: {
				get: () => this.constructor.pattern.exec( this.value )[2].toLowerCase(),
				enumerable: true,
			},

			/**
			 * Exposes optional set of parameters customizing current seelction
			 * of media type.
			 *
			 * @note Parameter names are always converted to lowercase. Values
			 *       are trimmed and provided without any wrapping quotes.
			 *
			 * @name TypeMimeMediaType#parameters
			 * @property {object<string,string>}
			 * @readonly
			 */
			parameters: {
				get: () => {
					const map = {};
					const raw = this.constructor.pattern.exec( this.value )[3];

					if ( raw ) {
						const matcher = /\s*;\s*([^=;\s]+)\s*=\s*/g;
						let match;

						while ( ( match = matcher.exec( raw ) ) != null ) {
							const beyondEnd = match.index + match[0].length;
							const sub = raw.charAt( beyondEnd ) === '"' ? /"([^"]+)"/g : /([^\s;]+)/g;
							sub.lastIndex = beyondEnd;

							map[match[1].toLowerCase()] = sub.exec( raw )[1];
							matcher.lastIndex = sub.lastIndex;
						}
					}

					return Object.freeze( map );
				},
				enumerable: true,
			},
		} );
	}

	/** @inheritDoc */
	filterText( text ) {
		if ( text.length < 1 ) {
			throw new TypeError( "MIME descriptor is missing" );
		}

		if ( !this.constructor.patternAscii.test( text ) ) {
			throw new TypeError( "mimeMediaType must be US ASCII string" );
		}

		if ( !this.constructor.pattern.test( text ) ) {
			throw new TypeError( "mimeMediaType must comply with syntax defined in 5.1 of RFC 2045" );
		}

		if ( text.length > this.octetLimit ) {
			throw new TypeError( "provided mimeMediaType is too long" );
		}

		return text;
	}

	/** @inheritDoc */
	toBuffer() {
		return Buffer.from( this.value, "ascii" );
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		return _instance instanceof TypeString && _instance.type === mimeMediaType && this.value.toLowerCase() === _instance.value.toLowerCase();
	}
}

Object.defineProperties( TypeMimeMediaType, {
	/**
	 * Exposes pattern validating syntax of media type information accepted as value.
	 *
	 * @name TypeMimeMediaType.pattern
	 * @property {RegExp}
	 * @readonly
	 */
	pattern: {
		value: /^\s*([^\s\x00-\x1f()<>@,;:\\"/[\]?=]+)\/([^\s\x00-\x1f()<>@,;:\\"/[\]?=]+)((?:\s*;\s*(?:[^\s\x00-\x1f()<>@,;:\\"/[\]?=]+)\s*=\s*(?:[^\s\x00-\x1f()<>@,;:\\"/[\]?=]+|"[^"]*"))*)\s*$/, // eslint-disable-line no-control-regex
		enumerable: true,
	},
} );

module.exports = TypeMimeMediaType;
