/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const TypeString = require( "./string" );
const { AttributeType: { uri } } = require( "../../data" );


/**
 * Represents uri-type values in attributes.
 */
class TypeUri extends TypeString {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( uri, value, octetLimit );

		Object.defineProperties( this, {
			/**
			 * Exposes scheme of current URI value as defined in 3.1 RFC 3986.
			 *
			 * @name TypeUri#scheme
			 * @property {string}
			 * @readonly
			 */
			scheme: {
				get: () => this.constructor.pattern.exec( this.value )[1],
				enumerable: true,
			},

			/**
			 * Exposes authority of current URI value as defined in 3.2 RFC 3986.
			 *
			 * @name TypeUri#authority
			 * @property {string}
			 * @readonly
			 */
			authority: {
				get: () => this.constructor.pattern.exec( this.value )[2],
				enumerable: true,
			},

			/**
			 * Exposes userinfo of current URI value as defined in 3.2.1 RFC 3986.
			 *
			 * @name TypeUri#userinfo
			 * @property {string}
			 * @readonly
			 */
			userinfo: {
				get: () => this.constructor.pattern.exec( this.value )[3],
				enumerable: true,
			},

			/**
			 * Exposes host of current URI value as defined in 3.2.2 RFC 3986.
			 *
			 * @name TypeUri#host
			 * @property {string}
			 * @readonly
			 */
			host: {
				get: () => this.constructor.pattern.exec( this.value )[4],
				enumerable: true,
			},

			/**
			 * Exposes port of current URI value as defined in 3.2.3 RFC 3986.
			 *
			 * @name TypeUri#port
			 * @property {string}
			 * @readonly
			 */
			port: {
				get: () => this.constructor.pattern.exec( this.value )[5],
				enumerable: true,
			},

			/**
			 * Exposes path of current URI value as defined in 3.3 RFC 3986.
			 *
			 * @name TypeUri#path
			 * @property {string}
			 * @readonly
			 */
			path: {
				get: () => this.constructor.pattern.exec( this.value )[6],
				enumerable: true,
			},

			/**
			 * Exposes query of current URI value as defined in 3.4 RFC 3986.
			 *
			 * @name TypeUri#query
			 * @property {string}
			 * @readonly
			 */
			query: {
				get: () => this.constructor.pattern.exec( this.value )[7],
				enumerable: true,
			},

			/**
			 * Exposes fragment of current URI value as defined in 3.5 RFC 3986.
			 *
			 * @name TypeUri#fragment
			 * @property {string}
			 * @readonly
			 */
			fragment: {
				get: () => this.constructor.pattern.exec( this.value )[8],
				enumerable: true,
			},
		} );
	}

	/** @inheritDoc */
	filterText( text ) {
		if ( text.length < 1 ) {
			throw new TypeError( "URI is missing" );
		}

		const decoded = text.replace( /%([0-9a-f]{2})/ig, ( all, code ) => {
			const _code = parseInt( code, 16 );
			if ( _code < 0x20 || _code > 0x7e ) {
				return all;
			}

			const char = String.fromCharCode( _code );

			return ":/?#[]@!$&'()*+,;=".indexOf( char ) > -1 ? all : char;
		} );

		const match = this.constructor.pattern.exec( decoded );
		if ( !match ) {
			throw new TypeError( "URI must basically comply with RFC 3986" );
		}

		const url = ( match[1].toLowerCase() + ":" +
		              ( match[2] ? "//" + ( match[3] ? match[3] + "@" : "" ) + match[4].toLowerCase() + ( match[5] ? ":" + match[5] : "" ) : "" ) +
		              match[6] +
		              ( match[7] ? "?" + match[7] : "" ) +
		              ( match[8] ? "#" + match[8] : "" ) ).replace( /%[0-9a-f]{2}/g, c => c.toUpperCase() );


		if ( !this.constructor.patternAscii.test( url ) ) {
			throw new TypeError( "URI must be US ASCII string" );
		}

		if ( url.length > this.octetLimit ) {
			throw new TypeError( "provided URI is too long" );
		}

		return url;
	}

	/** @inheritDoc */
	toBuffer() {
		return Buffer.from( this.value, "ascii" );
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		return _instance instanceof TypeString && _instance.type === uri && this.value === _instance.value;
	}
}

Object.defineProperties( TypeUri, {
	/**
	 * Exposes pattern suitable for testing if value complies with current
	 * type's syntax.
	 *
	 * @name TypeUri.pattern
	 * @property {RegExp}
	 * @readonly
	 */
	pattern: { value: /^([a-z][a-z0-9.+-]*):(?:\/\/((?:([^/?#@:]*)@)?([^/?#:]*)(?::(\d*))?))?([^?#]*)(?:\?([^#]*))?(?:#(.*))?$/i, enumerable: true },
} );

module.exports = TypeUri;
