/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const Type = require( "../abstract" );

/**
 * Commonly implements several string-based representations of attribute values.
 */
class TypeString extends Type {
	/**
	 * @param {int} type type identifier
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( type, value, octetLimit = Infinity ) {
		super( type, octetLimit );

		let _value;

		Object.defineProperties( this, {
			/**
			 * Exposes wrapped text.
			 *
			 * @name TypeString#value
			 * @property {*}
			 */
			value: {
				get: () => _value,
				set: newValue => {
					const _newValue = newValue instanceof Type ? newValue.value : newValue;
					const newType = typeof _newValue;

					switch ( newType ) {
						case "object" :
							if ( _newValue ) {
								break;
							}

						// falls through
						case "function" :
						case "undefined" :
							throw new TypeError( `cannot cast to text: ${newType}` );
					}

					_value = this.filterText( String( _newValue ).trim() );
				},
				enumerable: true,
			}
		} );

		this.value = value;
	}

	/**
	 * Applies type-specific validators and filters to provided text value on
	 * assigment.
	 *
	 * @param {string} text text value assigned
	 * @returns {string} filtered text value to adopt eventually
	 */
	filterText( text ) {
		const limit = this.constructor.limitUnicodeOctets( text, this.octetLimit );

		return limit < Infinity ? text.substr( 0, limit ) : text;
	}

	/** @inheritDoc */
	toBuffer() {
		return Buffer.from( this.value, "utf8" );
	}

	/** @inheritDoc */
	toString() {
		return `[${this.constructor.name.replace( /^Type(.)/, ( _, l ) => l.toLowerCase() )}(${this.value.length}) ${this.value.replace( /^(.{48}).+$/, ( _, text ) => text.slice( 0, -3 ) + "..." )}]`;
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		return _instance instanceof TypeString && this.value === _instance.value;
	}

	/**
	 * Calculates number of UTF-8 encoded characters fitting into provided limit
	 * on number of octets.
	 *
	 * @param {string} string string to be UTF-8 encoded
	 * @param {int} octetLimit maximum number of octets permitted when encoding
	 * @returns {number} number of fitting characters
	 */
	static limitUnicodeOctets( string, octetLimit ) {
		const numCharacters = string.length;
		let octets = 0;

		for ( let i = 0; i < numCharacters; i++ ) {
			const codePoint = string.codePointAt( i );
			let size;

			if ( codePoint < 0x80 ) {
				size = 1;
			} else if ( codePoint < 0x0800 ) {
				size = 2;
			} else if ( codePoint < 0x10000 ) {
				size = 3;
			} else {
				size = 4;
			}

			if ( octets + size > octetLimit ) {
				return i;
			}

			if ( size === 4 ) {
				const next = string.codePointAt( i + 1 );
				if ( next >= 0xd800 && next < 0xe000 ) {
					i++;
				}
			}

			octets += size;
		}

		return Infinity;
	}
}

module.exports = TypeString;
