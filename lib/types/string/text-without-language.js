/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const TypeString = require( "./string" );
const { AttributeType: { textWithoutLanguage } } = require( "../../data" );


/**
 * Represents textWithoutLanguage-type values in attributes.
 */
class TypeTextWithoutLanguage extends TypeString {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( textWithoutLanguage, value, octetLimit );
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		if ( _instance instanceof TypeString ) {
			return _instance.type === textWithoutLanguage && this.value === _instance.value;
		}

		if ( _instance instanceof require( "../octet-string/with-language" ).TypeTextWithLanguage ) {
			return this.value === _instance.value && _instance._language.equals( context.defaultLanguage );
		}

		return false;
	}
}

module.exports = TypeTextWithoutLanguage;
