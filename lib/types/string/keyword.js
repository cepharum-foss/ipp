/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const TypeString = require( "./string" );
const { AttributeType: { keyword } } = require( "../../data" );
const Options = require( "../../options" );


/**
 * Represents keyword-type values in attributes.
 */
class TypeKeyword extends TypeString {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( keyword, value, octetLimit );
	}

	/** @inheritDoc */
	filterText( text ) {
		if ( text.length < 1 ) {
			throw new TypeError( "keyword is missing" );
		}

		if ( !this.constructor.pattern.test( text ) ) {
			throw new TypeError( "provided string is not a valid keyword" );
		}

		if ( text.length > this.octetLimit ) {
			throw new TypeError( "provided keyword is too long" );
		}

		return text.substr( 0, this.octetLimit );
	}

	/** @inheritDoc */
	toBuffer() {
		return Buffer.from( this.value, "ascii" );
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		return _instance instanceof TypeString && _instance.type === keyword && this.value === _instance.value;
	}
}

Object.defineProperties( TypeKeyword, {
	/**
	 * Exposes pattern used for checking string complying with keyword syntax.
	 *
	 * @name TypeKeyword.pattern
	 * @property {RegExp}
	 * @readonly
	 */
	pattern: {
		get: () => ( Options.get( "relaxKeywordSyntax" ) ? /^[a-z0-9][a-z0-9_.-]{0,254}$/i : /^[a-z][a-z0-9_.-]{0,254}$/ ),
		enumerable: true,
	},
} );

module.exports = TypeKeyword;
