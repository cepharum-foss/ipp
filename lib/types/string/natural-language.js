/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const TypeString = require( "./string" );
const { AttributeType: { naturalLanguage } } = require( "../../data" );


/**
 * Represents naturalLanguage-type values in attributes.
 */
class TypeNaturalLanguage extends TypeString {
	/**
	 * @param {*} value initial value
	 * @param {int} octetLimit maximum number of encoded octets
	 */
	constructor( value, octetLimit = Infinity ) {
		super( naturalLanguage, value, octetLimit );

		Object.defineProperties( this, {
			/**
			 * Exposes language component of natural language selector according
			 * to section 2.1 of RFC 5646.
			 *
			 * @name TypeNaturalLanguage#language
			 * @property {string}
			 * @readonly
			 */
			language: {
				get: () => this.constructor.pattern.exec( this.value )[1],
				enumerable: true,
			},

			/**
			 * Exposes script component of natural language selector according
			 * to section 2.1 of RFC 5646.
			 *
			 * @name TypeNaturalLanguage#script
			 * @property {string}
			 * @readonly
			 */
			script: {
				get: () => this.constructor.pattern.exec( this.value )[2],
				enumerable: true,
			},

			/**
			 * Exposes region component of natural language selector according
			 * to section 2.1 of RFC 5646.
			 *
			 * @name TypeNaturalLanguage#region
			 * @property {string}
			 * @readonly
			 */
			region: {
				get: () => this.constructor.pattern.exec( this.value )[3],
				enumerable: true,
			},

			/**
			 * Exposes variant component of natural language selector according
			 * to section 2.1 of RFC 5646.
			 *
			 * @name TypeNaturalLanguage#variant
			 * @property {string}
			 * @readonly
			 */
			variant: {
				get: () => {
					const variant = this.constructor.pattern.exec( this.value )[4];

					return variant ? variant.substr( 1 ) : variant;
				},
				enumerable: true,
			},

			/**
			 * Exposes extension component of natural language selector according
			 * to section 2.1 of RFC 5646.
			 *
			 * @name TypeNaturalLanguage#extension
			 * @property {string}
			 * @readonly
			 */
			extension: {
				get: () => {
					const variant = this.constructor.pattern.exec( this.value )[5];

					return variant ? variant.substr( 1 ) : variant;
				},
				enumerable: true,
			},
		} );
	}

	/** @inheritDoc */
	filterText( text ) {
		if ( text.length < 1 ) {
			throw new TypeError( "natural language identifier is missing" );
		}

		if ( !this.constructor.pattern.test( text ) ) {
			throw new TypeError( "natural language identifier must comply with 2.1 of RFC 5646" );
		}


		if ( this.octetLimit < text.length ) {
			let start = this.octetLimit;
			let trimAt;

			while ( ( trimAt = text.lastIndexOf( "-", start ) ) > -1 ) {
				const trimmed = text.substring( 0, trimAt );
				if ( this.constructor.pattern.test( trimmed ) ) {
					return trimmed.toLowerCase();
				}

				start = trimAt - 1;
			}

			throw new TypeError( "natural language is too long" );
		}

		return text.toLowerCase();
	}

	/** @inheritDoc */
	toBuffer() {
		return Buffer.from( this.value, "ascii" );
	}

	/** @inheritDoc */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		const _instance = this._wrap( instance );

		if ( !( _instance instanceof TypeString ) || _instance.type !== naturalLanguage ) {
			return false;
		}

		const a = this.value;
		const b = _instance.value;

		const aTags = a.split( /-/ );
		const bTags = b.split( /-/ );
		const numTags = Math.min( aTags.length, bTags.length );

		for ( let i = 0; i < numTags; i++ ) {
			const aTag = aTags[i];
			const bTag = bTags[i];

			if ( aTag.length < bTag.length ) {
				if ( !bTag.startsWith( aTag ) ) {
					return false;
				}
			} else if ( !aTag.startsWith( bTag ) ) {
				return false;
			}
		}

		return true;
	}
}

Object.defineProperties( TypeNaturalLanguage, {
	/**
	 * Exposes pattern suitable for testing whether some value complies with
	 * syntax rules for describing natural language.
	 *
	 * @name TypeNaturalLanguage.pattern
	 * @property {RegExp}
	 * @readonly
	 */
	pattern: {
		value: /^\s*(?:x(?:-[a-z0-9]{1,8})+|([a-z]{2,3}(?:-[a-z]{3}){0,3}|[a-z]{4,8})(?:-([a-z]{4}))?(?:-([a-z]{2}|\d{3}))?(-(?:[a-z0-9]{5,8}|\d[a-z0-9]{3}))*(-[0-9a-wyz](?:-[a-z0-9]{2,8})+)*(-x(?:-[a-z0-9]{1,8})+)?)\s*$/i,
		enumerable: true,
	},
} );

module.exports = TypeNaturalLanguage;
