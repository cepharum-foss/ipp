/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { AttributeTypeLimit } = require( "../data" );

/**
 * Implements common API for all types of IPP values.
 */
class Type {
	/**
	 * @param {int} type type identifier
	 * @param {int} octetLimit maximum number of octets permitted when encoding typed value
	 */
	constructor( type, octetLimit = Infinity ) {
		let _octetLimit = Infinity;

		Object.defineProperties( this, {
			/**
			 * Exposes immutable type identifier.
			 *
			 * @name Type#type
			 * @property {int}
			 * @readonly
			 */
			type: { value: type, enumerable: true },

			/**
			 * Exposes immutable limit on number of octets in encoded value.
			 *
			 * @note This limit isn't applied to the encoded buffer as a whole
			 *       but to some encoded value contained in it.
			 *
			 * @note This limit may be reduced, only.
			 *
			 * @name Type#octetLimit
			 * @property {int}
			 * @readonly
			 */
			octetLimit: {
				get: () => _octetLimit,
				set: newLimit => {
					let _newLimit = parseInt( newLimit );

					if ( isNaN( _newLimit ) ) {
						_newLimit = _octetLimit;
					}

					if ( type && _newLimit > AttributeTypeLimit[type] ) {
						_newLimit = AttributeTypeLimit[type];
					}

					if ( _newLimit === _octetLimit ) {
						return;
					}

					if ( _newLimit > 0 && _newLimit <= _octetLimit ) {
						const oldLimit = _octetLimit;
						_octetLimit = _newLimit;

						try {
							this._onReduceOctetLimit();
						} catch ( error ) {
							_octetLimit = oldLimit;
							throw error;
						}
					} else {
						throw new TypeError( "invalid request for adjusting limit of octets in encoding of typed value" );
					}
				},
			},
		} );

		this.octetLimit = octetLimit;
	}

	/**
	 * Applies reduced limit on encoded octets.
	 *
	 * @returns {void}
	 * @protected
	 */
	_onReduceOctetLimit() {
		if ( this.value ) {
			this.value = this.value; // eslint-disable-line no-self-assign
		}
	}

	/**
	 * Reduces limit on size of encoded value.
	 *
	 * @param {int} octetLimit maximum number of octets to generate on encoding typed value
	 * @returns {Type} current instance, fluent interface
	 */
	limit( octetLimit ) {
		this.octetLimit = octetLimit;

		return this;
	}

	/**
	 * Compiles buffer containing binary-encoded value according to IPP format.
	 *
	 * @returns {Buffer} current value as buffer
	 * @abstract
	 */
	toBuffer() {
		throw new Error( "generic type must not be converted to buffer" );
	}

	/**
	 * Converts current value to string.
	 *
	 * @note The resulting string is basically meant for use in dumps and logs
	 *       and may omit parts of current value.
	 *
	 * @returns {string} current value rendered as string
	 * @abstract
	 */
	toString() {
		throw new Error( "generic type must not be converted to buffer" );
	}

	/**
	 * Tests if provided value is equivalent to current one.
	 *
	 * @param {Type} instance value to compare current one with
	 * @param {object} context context of comparison, e.g. optionally providing some default locale
	 * @returns {boolean} true if both values are equivalent
	 */
	equals( instance, context = {} ) { // eslint-disable-line no-unused-vars
		return instance instanceof this.constructor && this.type === instance.type;
	}

	/**
	 * Wraps provided value not derived from Type in instance of current Type.
	 *
	 * @param {*} value value probably not derived from Type
	 * @returns {Type|boolean} instance of Type, false if wrapping failed
	 * @protected
	 */
	_wrap( value ) {
		try {
			const _instance = value instanceof Type ? value : new this.constructor( this );

			_instance.value = value;

			return _instance;
		} catch ( error ) {
			return false;
		}
	}
}

Object.defineProperties( Type, {
	/**
	 * Provides singleton empty buffer for re-use by out-of-band types on
	 * generating encoded data.
	 *
	 * @name Type.emptyBuffer
	 * @property {Buffer}
	 * @readonly
	 * @protected
	 */
	emptyBuffer: { value: Buffer.alloc( 0 ) },

	/**
	 * Exposes commonly available pattern for testing whether some string is
	 * encoded in US ASCII or not.
	 *
	 * @name Type.patternAscii
	 * @property {RegExp}
	 * @readonly
	 */
	patternAscii: { value: /^[\x00-\x7f]*$/ },  // eslint-disable-line no-control-regex
} );

module.exports = Type;
