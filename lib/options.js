/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const globalOptions = {
	relaxKeywordSyntax: false,  // ignore issues with keyword syntax due to RFC 8011 erratum? https://www.rfc-editor.org/errata/eid6085
};

/**
 * Adjusts value of named global option.
 *
 * @param {string} name name of option to adjust
 * @param {*} value new value of option
 * @returns {void}
 */
exports.set = function( name, value ) {
	switch ( name ) {
		case "relaxKeywordSyntax" :
			globalOptions.relaxKeywordSyntax = Boolean( value );
			break;

		default :
			throw new TypeError( "invalid global option" );
	}
};

/**
 * Fetches value of named global option.
 *
 * @param {string} name name of global option to fetch
 * @returns {*} value of selected option
 */
exports.get = function( name ) {
	switch ( name ) {
		case "relaxKeywordSyntax" :
			return globalOptions.relaxKeywordSyntax;

		default :
			throw new TypeError( "invalid global option" );
	}
};
