/**
 * (c) 2018 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: Thomas Urban
 */

"use strict";

const { AttributeType } = require( "./data" );
const Types = require( "./types" );


/**
 * Extracts ASCII string ensuring selected slice of buffer actually containing
 * ASCII octets, only.
 *
 * @param {Buffer} buffer buffer to check
 * @param {int} start index into buffer of first octet to check
 * @param {int} length number of octets to check
 * @param {string} message custom message to throw
 * @returns {string} slice as ASCII-encoded string
 * @throws Error on discovering non-ASCII octet in slice
 */
function getASCII( buffer, start, length, message = "invalid ASCII character" ) {
	for ( let i = 0; i < length; i++ ) {
		if ( buffer[start + i] > 0x7f ) {
			throw new Error( message );
		}
	}

	return buffer.toString( "ascii", start, start + length );
}

/**
 * Maps value type identifiers into functions capable of extracting either type
 * of value.
 *
 * This map is used to extract values from raw binary encoded IPP message. Every
 * method is invoked with raw IPP message, index into that message of first
 * octet of value to extract and the declared number of octets used to describe
 * the value. Finally the tag on value's type is passed in fourth argument to
 * support shared methods.
 *
 * @type {Object<int,function(data:Buffer,pos:int,length:int,type:int):Type>}
 */
const Decoders = {
	// out-of-band data
	[AttributeType.default]: () => new Types.TypeDefault(),
	[AttributeType.unknown]: () => new Types.TypeUnknown(),
	[AttributeType.noValue]: () => new Types.TypeNoValue(),

	// integer data
	[AttributeType.integer]: ( data, pos, length ) => {
		if ( length !== 4 ) {
			throw new Error( "invalid integer size" );
		}

		return new Types.TypeInteger( data.readInt32BE( pos ) );
	},
	[AttributeType.boolean]: ( data, pos, length ) => {
		if ( length !== 1 ) {
			throw new Error( "invalid size of bool" );
		}

		return new Types.TypeBoolean( data.readInt8( pos ) !== 0 );
	},
	[AttributeType.enum]: ( data, pos, length ) => {
		if ( length !== 4 ) {
			throw new Error( "invalid integer size" );
		}

		return new Types.TypeEnum( data.readInt32BE( pos ) );
	},

	// binary / octet-string data
	[AttributeType.octetString]: ( data, pos, length ) => {
		return new Types.TypeOctetString( data.slice( pos, pos + length ) );
	},
	[AttributeType.dateTime]: ( data, pos, length ) => {
		if ( length !== 11 ) {
			throw new Error( "invalid size" );
		}

		// extract date/time
		const year = data.readUInt16BE( pos );
		const month = data.readUInt8( pos + 2 );
		const day = data.readUInt8( pos + 3 );
		const hour = data.readUInt8( pos + 4 );
		const minute = data.readUInt8( pos + 5 );
		const second = data.readUInt8( pos + 6 );
		const deci = data.readUInt8( pos + 7 );

		// extract information on distance from UTC (timezone)
		const utcDir = getASCII( data, pos + 8, 1 );
		let utcMinutes = ( 60 * data.readUInt8( pos + 9 ) ) + data.readUInt8( pos + 10 );
		if ( utcDir === "-" ) {
			utcMinutes = -utcMinutes;
		} else if ( utcDir !== "+" ) {
			throw new Error( "invalid timezone offset" );
		}

		// compile into native timestamp
		const timestamp = new Date( year, month - 1, day, hour, minute, second, deci * 100 );

		// date was falsely considering given time to be local time above
		// -> translate from local timezone to declared one (so Date() is storing proper UTC finally)
		let ts = timestamp.getTime();
		// - convert from local timezone to UTC (e.g. from local 10am to 10am UTC)
		ts -= timestamp.getTimezoneOffset() * 60 * 1000;  // on +02:00 this is returning -120, thus adding
		// - convert from UTC to declared timezone (e.g. from 10am UTC to declared 10am)
		ts -= utcMinutes * 60000;                         // on +02:00 utcMinutes is 120, thus correcting properly
		timestamp.setTime( ts );


		return new Types.TypeDateTime( timestamp );
	},
	[AttributeType.resolution]: ( data, pos, length ) => {
		if ( length !== 9 ) {
			throw new Error( "invalid size" );
		}

		const x = data.readInt32BE( pos );
		const y = data.readInt32BE( pos + 4 );
		const unit = data.readInt8( pos + 8 );

		return new Types.TypeResolution( x, y, unit );
	},
	[AttributeType.rangeOfInteger]: ( data, pos, length ) => {
		if ( length !== 8 ) {
			throw new Error( "invalid size" );
		}

		const lower = data.readInt32BE( pos );
		const upper = data.readInt32BE( pos + 4 );

		return new Types.TypeRangeOfInteger( lower, upper );
	},
	[AttributeType.textWithLanguage]: ( data, pos, length, type ) => {
		const typeName = ( type === AttributeType.textWithLanguage ? "text" : "name" ) + "WithLanguage";
		let _pos = pos;

		// read length of included naturalLanguage
		if ( length < 2 ) {
			throw new Error( "met EOM while reading length of naturalLanguage" );
		}

		const languageLength = data.readInt16BE( _pos );
		_pos += 2;

		// read included naturalLanguage
		if ( length < 2 + languageLength ) {
			throw new Error( "met EOM while reading naturalLanguage" );
		}

		const language = getASCII( data, _pos, languageLength );
		_pos += languageLength;

		// read length of included *WithoutLanguage
		if ( length < 4 + languageLength ) {
			throw new Error( "met EOM while reading length of included " + typeName );
		}

		const stringLength = data.readInt16BE( _pos );
		_pos += 2;

		// read included *WithoutLanguage
		if ( length < 4 + languageLength + stringLength ) {
			throw new Error( "met EOM while reading included " + typeName );
		}

		const string = data.toString( "utf8", _pos, _pos + stringLength );
		_pos += stringLength;

		if ( _pos < length ) {
			throw new Error( "invalid extra content" );
		}

		return new ( type === AttributeType.textWithLanguage ? Types.TypeTextWithLanguage : Types.TypeNameWithLanguage )( string, language );
	},
	[AttributeType.nameWithLanguage]: AttributeType.textWithLanguage,

	// string data
	[AttributeType.textWithoutLanguage]: ( data, pos, length ) => {
		return new Types.TypeTextWithoutLanguage( data.toString( "utf8", pos, pos + length ) );
	},
	[AttributeType.nameWithoutLanguage]: ( data, pos, length ) => {
		return new Types.TypeNameWithoutLanguage( data.toString( "utf8", pos, pos + length ) );
	},
	[AttributeType.keyword]: ( data, pos, length ) => {
		return new Types.TypeKeyword( getASCII( data, pos, length ) );
	},
	[AttributeType.uri]: ( data, pos, length ) => {
		return new Types.TypeUri( getASCII( data, pos, length ) );
	},
	[AttributeType.uriScheme]: ( data, pos, length ) => {
		return new Types.TypeUriScheme( getASCII( data, pos, length ) );
	},
	[AttributeType.charset]: ( data, pos, length ) => {
		return new Types.TypeCharset( getASCII( data, pos, length ) );
	},
	[AttributeType.naturalLanguage]: ( data, pos, length ) => {
		return new Types.TypeNaturalLanguage( getASCII( data, pos, length ) );
	},
	[AttributeType.mimeMediaType]: ( data, pos, length ) => {
		return new Types.TypeMimeMediaType( getASCII( data, pos, length ) );
	},
};

Object.keys( Decoders )
	.forEach( id => {
		const ref = Decoders[id];
		if ( ref > 0 ) {
			Decoders[id] = Decoders[ref];
		}
	} );

module.exports = Decoders;
