/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { Type, TypeKeyword } = require( "./types" );
const { AttributeTypeGroup } = require( "./data" );

/**
 * Implements guided map of attributes.
 */
class GuidedAttributes extends Map {
	/**
	 * Safeguards creation of map using valid attributes, only.
	 */
	constructor() { // eslint-disable-line no-useless-constructor
		super();
	}

	/**
	 * Puts provided attribute.
	 *
	 * @param {string} key name of attribute to write
	 * @param {Type|Type[]} value value of attribute to write
	 * @returns {GuidedAttributes} fluent interface
	 */
	set( key, value ) {
		if ( typeof key !== "string" || !TypeKeyword.pattern.test( key ) ) {
			throw new TypeError( "attribute name must be valid keyword" );
		}

		const _value = Array.isArray( value ) ? value : [value];
		const numValues = _value.length;
		let first;

		if ( numValues < 1 ) {
			throw new TypeError( "set of values must not be empty" );
		}

		for ( let i = 0; i < numValues; i++ ) {
			const v = _value[i];

			if ( !( v instanceof Type ) ) {
				throw new TypeError( "all listed values must be instances of Type" );
			}

			if ( i > 0 ) {
				if ( v.type !== first.type && ( !AttributeTypeGroup[v.type] || AttributeTypeGroup[v.type] !== AttributeTypeGroup[first.type] ) ) {
					throw new TypeError( "invalid mix of types in list of values" );
				}
			} else {
				first = v;
			}
		}

		super.set( key, _value.slice( 0 ) );

		return this;
	}

	/**
	 * Adds another value to selected attribute.
	 *
	 * @param {string} key name of attribute to extend
	 * @param {Type} value value to add
	 * @returns {GuidedAttributes} fluent interface
	 */
	add( key, value ) {
		if ( this.has( key ) ) {
			if ( !( value instanceof Type ) ) {
				throw new TypeError( "value must be instance of Type" );
			}

			const target = super.get( key );
			const first = target[0];

			if ( value.type !== first.type && ( !AttributeTypeGroup[value.type] || AttributeTypeGroup[value.type] !== AttributeTypeGroup[first.type] ) ) {
				throw new TypeError( "invalid mix of types in list of values" );
			}

			target.push( value );

			return this;
		}

		return this.set( key, [value] );
	}

	/**
	 * Removes single value from named attribute's list of values.
	 *
	 * @param {string} key name of attribute to adjust
	 * @param {Type} value value to remove
	 * @return {GuidedAttributes} fluent interface
	 */
	remove( key, value ) {
		if ( !( value instanceof Type ) ) {
			throw new TypeError( "value must be instance of Type" );
		}

		if ( this.has( key ) ) {
			const list = super.get( key );
			let numValues = list.length;

			for ( let i = 0; i < numValues; i++ ) {
				if ( value.equals( list[i] ) ) {
					if ( numValues === 1 ) {
						this.delete( key );

						return this;
					}

					list.splice( i--, 1 );
					numValues--;
				}
			}
		}

		return this;
	}

	/**
	 * Fetches named attribute's value(s).
	 *
	 * @param {string} key name of attribute to fetch
	 * @param {Type} fallback fallback to return when selected value isn't found
	 * @returns {Type|Type[]} found attribute's single value or list of values
	 * @throws Error if named attribute is missing
	 */
	get( key, fallback = null ) {
		const values = super.get( key );
		if ( values == null ) {
			if ( fallback instanceof Type ) {
				return fallback;
			}

			throw new Error( "no such attribute: " + key );
		}

		return values.length === 1 ? values[0] : values;
	}
}

module.exports = GuidedAttributes;
