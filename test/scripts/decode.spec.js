/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Decoders = require( "../../lib/decode" );
const {
	AttributeType,
	TypeUnknown, TypeDefault, TypeNoValue,
	TypeInteger, TypeBoolean, TypeEnum,
	TypeOctetString, TypeDateTime, TypeResolution, TypeRangeOfInteger,
	TypeTextWithLanguage, TypeNameWithLanguage,
	TypeTextWithoutLanguage, TypeNameWithoutLanguage, TypeKeyword, TypeCharset,
	TypeUriScheme, TypeUri, TypeMimeMediaType, TypeNaturalLanguage,
} = require( "../../" );


describe( "Set of type-specific decoders", () => {
	it( "maps numeric type identifiers into functions", () => {
		Decoders.should.be.Object().which.is.not.empty();

		Object.keys( Decoders )
			.forEach( code => {
				code.should.match( /^[1-9]\d*$/ );

				Decoders[code].should.be.Function();
			} );
	} );

	it( "supports unknown-type", () => {
		Decoders[AttributeType.unknown]().should.be.instanceOf( TypeUnknown );
	} );

	it( "supports default-type", () => {
		Decoders[AttributeType.default]().should.be.instanceOf( TypeDefault );
	} );

	it( "supports noValue-type", () => {
		Decoders[AttributeType.noValue]().should.be.instanceOf( TypeNoValue );
	} );

	it( "supports integer-type", () => {
		Decoders[AttributeType.integer]( Buffer.from( [ 1, 2, 3, 4 ] ), 0, 4 ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.integer]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 1, 4 ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 0x2030405 );
		Decoders[AttributeType.integer]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 0, 4 ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 0x1020304 );

		( () => Decoders[AttributeType.integer]( Buffer.alloc( 0 ), 0, 0 ) ).should.throw();
		( () => Decoders[AttributeType.integer]( Buffer.from( [0] ), 0, 1 ) ).should.throw();
		( () => Decoders[AttributeType.integer]( Buffer.from( [ 0, 0 ] ), 0, 2 ) ).should.throw();
		( () => Decoders[AttributeType.integer]( Buffer.from( [ 0, 0, 0 ] ), 0, 3 ) ).should.throw();
		( () => Decoders[AttributeType.integer]( Buffer.from( [ 0, 0, 0, 0 ] ), 0, 3 ) ).should.throw();
		( () => Decoders[AttributeType.integer]( Buffer.from( [ 0, 0, 0, 0 ] ), 1, 3 ) ).should.throw();
	} );

	it( "supports boolean-type", () => {
		Decoders[AttributeType.boolean]( Buffer.from( [0] ), 0, 1 ).should.be.instanceOf( TypeBoolean ).which.has.property( "value" ).which.is.false();
		Decoders[AttributeType.boolean]( Buffer.from( [ 0, 1 ] ), 0, 1 ).should.be.instanceOf( TypeBoolean ).which.has.property( "value" ).which.is.false();
		Decoders[AttributeType.boolean]( Buffer.from( [ 0, 1 ] ), 1, 1 ).should.be.instanceOf( TypeBoolean ).which.has.property( "value" ).which.is.true();

		for ( let i = 1; i < 255; i++ ) {
			Decoders[AttributeType.boolean]( Buffer.from( [i] ), 0, 1 ).should.be.instanceOf( TypeBoolean ).which.has.property( "value" ).which.is.true();
			Decoders[AttributeType.boolean]( Buffer.from( [ i, 0 ] ), 1, 1 ).should.be.instanceOf( TypeBoolean ).which.has.property( "value" ).which.is.false();
		}

		( () => Decoders[AttributeType.boolean]( Buffer.alloc( 0 ), 0, 0 ) ).should.throw();
		( () => Decoders[AttributeType.boolean]( Buffer.from( [0] ), 0, 0 ) ).should.throw();
		( () => Decoders[AttributeType.boolean]( Buffer.from( [0] ), 1, 0 ) ).should.throw();
	} );

	it( "supports enum-type", () => {
		Decoders[AttributeType.enum]( Buffer.from( [ 1, 2, 3, 4 ] ), 0, 4 ).should.be.instanceOf( TypeEnum ).which.has.property( "index" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.enum]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 1, 4 ).should.be.instanceOf( TypeEnum ).which.has.property( "index" ).which.is.equal( 0x02030405 );
		Decoders[AttributeType.enum]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 0, 4 ).should.be.instanceOf( TypeEnum ).which.has.property( "index" ).which.is.equal( 0x01020304 );

		( () => Decoders[AttributeType.enum]( Buffer.alloc( 0 ), 0, 0 ) ).should.throw();
		( () => Decoders[AttributeType.enum]( Buffer.from( [0] ), 0, 1 ) ).should.throw();
		( () => Decoders[AttributeType.enum]( Buffer.from( [ 0, 0 ] ), 0, 2 ) ).should.throw();
		( () => Decoders[AttributeType.enum]( Buffer.from( [ 0, 0, 0 ] ), 0, 3 ) ).should.throw();
		( () => Decoders[AttributeType.enum]( Buffer.from( [ 0, 0, 0, 0 ] ), 0, 3 ) ).should.throw();
		( () => Decoders[AttributeType.enum]( Buffer.from( [ 0, 0, 0, 0 ] ), 1, 3 ) ).should.throw();
	} );

	it( "supports octetString-type", () => {
		Decoders[AttributeType.octetString]( Buffer.from( [ 1, 2, 3, 4 ] ), 0, 4 ).should.be.instanceOf( TypeOctetString ).which.has.property( "value" ).which.is.deepEqual( Buffer.from( [ 1, 2, 3, 4 ] ) );
		Decoders[AttributeType.octetString]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 1, 4 ).should.be.instanceOf( TypeOctetString ).which.has.property( "value" ).which.is.deepEqual( Buffer.from( [ 2, 3, 4, 5 ] ) );
		Decoders[AttributeType.octetString]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 0, 4 ).should.be.instanceOf( TypeOctetString ).which.has.property( "value" ).which.is.deepEqual( Buffer.from( [ 1, 2, 3, 4 ] ) );

		Decoders[AttributeType.octetString]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 1, 3 ).should.be.instanceOf( TypeOctetString ).which.has.property( "value" ).which.is.deepEqual( Buffer.from( [ 2, 3, 4 ] ) );
		Decoders[AttributeType.octetString]( Buffer.from( [ 1, 2, 3, 4, 5 ] ), 1, 0 ).should.be.instanceOf( TypeOctetString ).which.has.property( "value" ).which.is.deepEqual( Buffer.alloc( 0 ) );
	} );

	it( "supports dateTime-type", () => {
		const minimal = Buffer.from( [ 0, 0, 0, 0, 0, 0, 0, 0, "+".charCodeAt( 0 ), 0, 0 ] );
		const alt = Buffer.from( [ 1, 0, 0, 0, 0, 0, 0, 0, 0, "-".charCodeAt( 0 ), 0, 0 ] );
		const extra = Buffer.from( [ 0, 0, 0, 0, 0, 0, 0, 0, "+".charCodeAt( 0 ), 0, 0, 1 ] );

		Decoders[AttributeType.dateTime]( minimal, 0, 11 ).should.be.instanceOf( TypeDateTime );
		Decoders[AttributeType.dateTime]( alt, 1, 11 ).should.be.instanceOf( TypeDateTime );
		Decoders[AttributeType.dateTime]( extra, 0, 11 ).should.be.instanceOf( TypeDateTime );

		for ( let i = 0; i < 11; i++ ) {
			( () => Decoders[AttributeType.dateTime]( minimal, 0, i ) ).should.throw();
			( () => Decoders[AttributeType.dateTime]( alt, 1, i ) ).should.throw();
		}

		( () => Decoders[AttributeType.dateTime]( extra, 0, 11 ) ).should.not.throw();

		for ( let i = 0; i < 255; i++ ) {
			extra[8] = i;

			switch ( String.fromCharCode( i ) ) {
				case "+" :
				case "-" :
					( () => Decoders[AttributeType.dateTime]( extra, 0, 11 ) ).should.not.throw();
					break;

				default :
					( () => Decoders[AttributeType.dateTime]( extra, 0, 11 ) ).should.throw();
			}
		}
	} );

	it( "supports resolution-type", () => {
		const minimal = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8, 3 ] );
		const alt = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 4 ] );
		const extra = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8, 3, 9 ] );

		Decoders[AttributeType.resolution]( minimal, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "x" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.resolution]( minimal, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "y" ).which.is.equal( 0x5060708 );
		Decoders[AttributeType.resolution]( minimal, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "unit" ).which.is.equal( TypeResolution.PerInch );
		Decoders[AttributeType.resolution]( alt, 1, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "x" ).which.is.equal( 0x2030405 );
		Decoders[AttributeType.resolution]( alt, 1, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "y" ).which.is.equal( 0x6070809 );
		Decoders[AttributeType.resolution]( alt, 1, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "unit" ).which.is.equal( TypeResolution.PerCm );
		Decoders[AttributeType.resolution]( extra, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "x" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.resolution]( extra, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "y" ).which.is.equal( 0x5060708 );
		Decoders[AttributeType.resolution]( extra, 0, 9 ).should.be.instanceOf( TypeResolution ).which.has.property( "unit" ).which.is.equal( TypeResolution.PerInch );

		for ( let i = 0; i < 9; i++ ) {
			( () => Decoders[AttributeType.resolution]( minimal, 0, i ) ).should.throw();
			( () => Decoders[AttributeType.resolution]( alt, 1, i ) ).should.throw();
		}

		( () => Decoders[AttributeType.resolution]( extra, 0, 9 ) ).should.not.throw();
		( () => Decoders[AttributeType.resolution]( extra, 0, 10 ) ).should.throw();

		( () => Decoders[AttributeType.resolution]( minimal, 0, 9 ) ).should.not.throw();

		for ( let i = 0; i < 255; i++ ) {
			extra[8] = i;

			switch ( i ) {
				case TypeResolution.PerInch :
				case TypeResolution.PerCm :
					( () => Decoders[AttributeType.resolution]( extra, 0, 9 ) ).should.not.throw();
					break;

				default :
					( () => Decoders[AttributeType.resolution]( extra, 0, 9 ) ).should.throw();
			}
		}
	} );

	it( "supports rangeOfInteger-type", () => {
		const minimal = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8 ] );
		const alt = Buffer.from( [ 1, 2, 3, 4, 5, 6, 7, 8, 9 ] );

		Decoders[AttributeType.rangeOfInteger]( minimal, 0, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "lower" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.rangeOfInteger]( minimal, 0, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "upper" ).which.is.equal( 0x5060708 );
		Decoders[AttributeType.rangeOfInteger]( alt, 1, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "lower" ).which.is.equal( 0x2030405 );
		Decoders[AttributeType.rangeOfInteger]( alt, 1, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "upper" ).which.is.equal( 0x6070809 );
		Decoders[AttributeType.rangeOfInteger]( alt, 0, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "lower" ).which.is.equal( 0x1020304 );
		Decoders[AttributeType.rangeOfInteger]( alt, 0, 8 ).should.be.instanceOf( TypeRangeOfInteger ).which.has.property( "upper" ).which.is.equal( 0x5060708 );

		for ( let i = 0; i < 8; i++ ) {
			( () => Decoders[AttributeType.rangeOfInteger]( alt, 0, i ) ).should.throw();
			( () => Decoders[AttributeType.rangeOfInteger]( alt, 1, i ) ).should.throw();
		}

		( () => Decoders[AttributeType.rangeOfInteger]( alt, 0, 8 ) ).should.not.throw();
		( () => Decoders[AttributeType.rangeOfInteger]( alt, 0, 9 ) ).should.throw();
	} );

	it( "supports textWithLanguage-type", () => {
		const minimal = Buffer.concat( [ Buffer.from( [ 0, 2 ] ), Buffer.from( "de" ), Buffer.from( [ 0, 3 ] ), Buffer.from( "foo" ) ] );
		const alt = Buffer.concat( [ Buffer.from( [1] ), minimal ] );
		const extra = Buffer.concat( [ minimal, Buffer.from( [1] ) ] );

		Decoders[AttributeType.textWithLanguage]( minimal, 0, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.textWithLanguage]( minimal, 0, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );
		Decoders[AttributeType.textWithLanguage]( alt, 1, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.textWithLanguage]( alt, 1, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );
		Decoders[AttributeType.textWithLanguage]( extra, 0, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.textWithLanguage]( extra, 0, minimal.length, AttributeType.textWithLanguage ).should.be.instanceOf( TypeTextWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );

		for ( let i = 0; i < minimal.length; i++ ) {
			( () => Decoders[AttributeType.textWithLanguage]( extra, 0, i, AttributeType.textWithLanguage ) ).should.throw();
			( () => Decoders[AttributeType.textWithLanguage]( alt, 1, i, AttributeType.textWithLanguage ) ).should.throw();
		}

		( () => Decoders[AttributeType.textWithLanguage]( extra, 0, minimal.length, AttributeType.textWithLanguage ) ).should.not.throw();
		( () => Decoders[AttributeType.textWithLanguage]( extra, 0, extra.length, AttributeType.textWithLanguage ) ).should.throw();
	} );

	it( "supports nameWithLanguage-type", () => {
		const minimal = Buffer.concat( [ Buffer.from( [ 0, 2 ] ), Buffer.from( "de" ), Buffer.from( [ 0, 3 ] ), Buffer.from( "foo" ) ] );
		const alt = Buffer.concat( [ Buffer.from( [1] ), minimal ] );
		const extra = Buffer.concat( [ minimal, Buffer.from( [1] ) ] );

		Decoders[AttributeType.nameWithLanguage]( minimal, 0, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.nameWithLanguage]( minimal, 0, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );
		Decoders[AttributeType.nameWithLanguage]( alt, 1, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.nameWithLanguage]( alt, 1, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );
		Decoders[AttributeType.nameWithLanguage]( extra, 0, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.nameWithLanguage]( extra, 0, minimal.length, AttributeType.nameWithLanguage ).should.be.instanceOf( TypeNameWithLanguage ).which.has.property( "language" ).which.is.equal( "de" );

		for ( let i = 0; i < minimal.length; i++ ) {
			( () => Decoders[AttributeType.nameWithLanguage]( extra, 0, i, AttributeType.nameWithLanguage ) ).should.throw();
			( () => Decoders[AttributeType.nameWithLanguage]( alt, 1, i, AttributeType.nameWithLanguage ) ).should.throw();
		}

		( () => Decoders[AttributeType.nameWithLanguage]( extra, 0, minimal.length, AttributeType.nameWithLanguage ) ).should.not.throw();
		( () => Decoders[AttributeType.nameWithLanguage]( extra, 0, extra.length, AttributeType.nameWithLanguage ) ).should.throw();
	} );

	it( "supports string-types", () => {
		Decoders[AttributeType.textWithoutLanguage]( Buffer.from( "foobar" ), 0, 3 ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.textWithoutLanguage]( Buffer.from( "foobar" ), 1, 3 ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "oob" );
		Decoders[AttributeType.nameWithoutLanguage]( Buffer.from( "foobar" ), 0, 3 ).should.be.instanceOf( TypeNameWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.nameWithoutLanguage]( Buffer.from( "foobar" ), 1, 3 ).should.be.instanceOf( TypeNameWithoutLanguage ).which.has.property( "value" ).which.is.equal( "oob" );
		Decoders[AttributeType.keyword]( Buffer.from( "foobar" ), 0, 3 ).should.be.instanceOf( TypeKeyword ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.keyword]( Buffer.from( "foobar" ), 1, 3 ).should.be.instanceOf( TypeKeyword ).which.has.property( "value" ).which.is.equal( "oob" );
		Decoders[AttributeType.charset]( Buffer.from( "foobar" ), 0, 3 ).should.be.instanceOf( TypeCharset ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.charset]( Buffer.from( "foobar" ), 1, 3 ).should.be.instanceOf( TypeCharset ).which.has.property( "value" ).which.is.equal( "oob" );
		Decoders[AttributeType.naturalLanguage]( Buffer.from( "foobar" ), 0, 3 ).should.be.instanceOf( TypeNaturalLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
		Decoders[AttributeType.naturalLanguage]( Buffer.from( "foobar" ), 1, 3 ).should.be.instanceOf( TypeNaturalLanguage ).which.has.property( "value" ).which.is.equal( "oob" );
		Decoders[AttributeType.mimeMediaType]( Buffer.from( "text/html" ), 0, 9 ).should.be.instanceOf( TypeMimeMediaType ).which.has.property( "value" ).which.is.equal( "text/html" );
		Decoders[AttributeType.mimeMediaType]( Buffer.from( "text/html" ), 1, 7 ).should.be.instanceOf( TypeMimeMediaType ).which.has.property( "value" ).which.is.equal( "ext/htm" );
		Decoders[AttributeType.uri]( Buffer.from( "urn:1" ), 0, 5 ).should.be.instanceOf( TypeUri ).which.has.property( "value" ).which.is.equal( "urn:1" );
		Decoders[AttributeType.uri]( Buffer.from( "urn:1" ), 1, 4 ).should.be.instanceOf( TypeUri ).which.has.property( "value" ).which.is.equal( "rn:1" );
		Decoders[AttributeType.uriScheme]( Buffer.from( "urn" ), 0, 3 ).should.be.instanceOf( TypeUriScheme ).which.has.property( "value" ).which.is.equal( "urn" );
		Decoders[AttributeType.uriScheme]( Buffer.from( "urn" ), 1, 2 ).should.be.instanceOf( TypeUriScheme ).which.has.property( "value" ).which.is.equal( "rn" );
	} );
} );
