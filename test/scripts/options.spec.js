/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const {
	setGlobalOption,
	getGlobalOption,
} = require( "../../" );

describe( "global options", () => {
	describe( "can be fetched with `getGlobalOption()` which", () => {
		it( "is a function", () => {
			getGlobalOption.should.be.Function().which.has.length( 1 );
		} );

		it( "accepts name of option in first argument", () => {
			getGlobalOption( "relaxKeywordSyntax" ).should.be.false();
		} );

		it( "rejects to fetch option selected by invalid name", () => {
			( () => getGlobalOption( "RelaxKeywordSyntax" ) ).should.throw();
			( () => getGlobalOption( "relaxKeywordSynta" ) ).should.throw();
			( () => getGlobalOption( "" ) ).should.throw();
			( () => getGlobalOption( true ) ).should.throw();
			( () => getGlobalOption( false ) ).should.throw();
		} );
	} );

	describe( "can be fetched with `setGlobalOption()` which", () => {
		it( "is a function", () => {
			setGlobalOption.should.be.Function().which.has.length( 2 );
		} );

		it( "accepts name of supported option in first argument and some value to apply in second argument", () => {
			getGlobalOption( "relaxKeywordSyntax" ).should.be.false();
			setGlobalOption( "relaxKeywordSyntax", true );
			getGlobalOption( "relaxKeywordSyntax" ).should.be.true();
			setGlobalOption( "relaxKeywordSyntax", false );
			getGlobalOption( "relaxKeywordSyntax" ).should.be.false();
		} );

		it( "normalizes value provided in second argument according to selected option", () => {
			getGlobalOption( "relaxKeywordSyntax" ).should.be.false();
			setGlobalOption( "relaxKeywordSyntax", 1 );
			getGlobalOption( "relaxKeywordSyntax" ).should.be.true();
			setGlobalOption( "relaxKeywordSyntax", "" );
			getGlobalOption( "relaxKeywordSyntax" ).should.be.false();
		} );

		it( "throws on trying to adjust unsupported options", () => {
			( () => { setGlobalOption( "RelaxKeywordSyntax", true ); } ).should.throw();
			( () => { setGlobalOption( "relaxKeywordSynta", true ); } ).should.throw();
			( () => { setGlobalOption( "", true ); } ).should.throw();
			( () => { setGlobalOption( true, true ); } ).should.throw();
			( () => { setGlobalOption( false, true ); } ).should.throw();
		} );
	} );
} );
