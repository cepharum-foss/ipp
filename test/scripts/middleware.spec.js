/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { Readable } = require( "stream" );

const { describe, it } = require( "mocha" );
require( "should" );

const { emitBuffer, consume } = require( "../helper" );
const Library = require( "../../" );
const { IPPMessage, middleware: MiddlewareGenerator } = Library;


describe( "express-compatible middleware", () => {
	it( "can generated", () => {
		MiddlewareGenerator.should.be.Function();
		MiddlewareGenerator().should.be.Function().which.has.length( 3 );
	} );

	it( "is passing if request does not provide IPP message", done => {
		MiddlewareGenerator()( { is: n => n !== "application/ipp" }, {}, done );
	} );

	it( "is processing stream if request provides IPP message", done => {
		const source = IPPMessage.createRequest( "CreateJob" );
		source.data = Buffer.from( [ 1, 2, 3, 4, 5 ] );

		const request = emitBuffer( source.toBuffer() );
		request.is = n => n === "application/ipp";

		MiddlewareGenerator()( request, {}, () => {
			request.body.should.be.Object().which.has.properties( "message", "data" ).and.has.size( 2 );
			const { message, data } = request.body;

			message.should.not.be.equal( source );
			message.should.be.instanceOf( IPPMessage );
			message.data.should.not.be.equal( source.data );
			message.data.should.be.empty();

			data.should.be.instanceOf( Readable );
			consume( data )
				.then( payload => {
					payload.should.be.instanceOf( Buffer ).which.is.not.equal( source.data ).and.is.deepEqual( source.data );

					done();
				} )
				.catch( done );
		} );
	} );

	it( "uses different spot for exposing received IPP message when used in context of Hitchy server-side framework", done => {
		const source = IPPMessage.createRequest( "CreateJob" );
		source.data = Buffer.from( [ 1, 2, 3, 4, 5 ] );

		const request = emitBuffer( source.toBuffer() );
		request.is = n => n === "application/ipp";
		request.context = { local: {} };
		request.hitchy = true;

		MiddlewareGenerator()( request, {}, () => {
			request.context.local.ipp.should.be.Object().which.has.properties( "message", "data" ).and.has.size( 2 );
			const { message, data } = request.context.local.ipp;

			message.should.not.be.equal( source );
			message.should.be.instanceOf( IPPMessage );
			message.data.should.not.be.equal( source.data );
			message.data.should.be.empty();

			data.should.be.instanceOf( Readable );
			consume( data )
				.then( payload => {
					payload.should.be.instanceOf( Buffer ).which.is.not.equal( source.data ).and.is.deepEqual( source.data );

					done();
				} )
				.catch( done );
		} );
	} );
} );
