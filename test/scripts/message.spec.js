/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const EventEmitter = require( "events" );

const { describe, it } = require( "mocha" );
require( "should" );

const {
	Operation, Status, IPPMessage,
	TypeNoValue, TypeDefault,
	TypeInteger, TypeBoolean,
	TypeTextWithoutLanguage,
	TypeOctetString, TypeDateTime,
} = require( "../../" );
const Data = require( "../../lib/data" );


describe( "IPP message", () => {
	it( "can be created", () => {
		new IPPMessage();
	} );

	it( "is an EventEmitter", () => {
		new IPPMessage().should.be.instanceOf( EventEmitter );
	} );

	it( "exposes IPP version in property `version`", () => {
		const message = new IPPMessage();

		message.should.have.property( "version" ).which.is.a.String().and.is.equal( "1.1" );
	} );

	it( "accepts IPP version adjusted", () => {
		const message = new IPPMessage();

		message.version = "1.0";
		message.version.should.be.a.String().which.is.equal( "1.0" );
	} );

	it( "rejects to assign invalid IPP version", () => {
		const message = new IPPMessage();

		( () => { message.version = null; } ).should.throw();
		( () => { message.version = undefined; } ).should.throw();
		( () => { message.version = 0; } ).should.throw();
		( () => { message.version = 1; } ).should.throw();
		( () => { message.version = ""; } ).should.throw();
		( () => { message.version = "1"; } ).should.throw();
		( () => { message.version = "a"; } ).should.throw();
		( () => { message.version = ".0"; } ).should.throw();
		( () => { message.version = "foo"; } ).should.throw();
		( () => { message.version = []; } ).should.throw();
		( () => { message.version = [1]; } ).should.throw();
		( () => { message.version = [ "1", "1" ]; } ).should.throw();
		( () => { message.version = { version: "1.1" }; } ).should.throw();
		( () => { message.version = { value: "1.1" }; } ).should.throw();
		( () => { message.version = () => "1.1"; } ).should.throw();

		( () => { message.version = ["1.1"]; } ).should.not.throw();
		( () => { message.version = { toString: () => "1.1" }; } ).should.not.throw();
	} );

	it( "provides property `code` representing a request's operation-id or a response's status-code", () => {
		const message = new IPPMessage();

		message.should.have.property( "code" ).which.is.undefined();

		message.code = Operation.CreateJob;
		message.code.should.be.equal( Operation.CreateJob );

		message.code = "SendDocument";
		message.code.should.be.equal( Operation.SendDocument );

		message.code = Status.clientErrorForbidden;
		message.code.should.be.equal( Status.clientErrorForbidden );

		message.code = "clientErrorGone";
		message.code.should.be.equal( Status.clientErrorGone );

		( () => { message.code = undefined; } ).should.throw();
		( () => { message.code = null; } ).should.throw();
		( () => { message.code = ""; } ).should.throw();
		( () => { message.code = { toString: () => "CreateJob" }; } ).should.throw();
		( () => { message.code = 12345678; } ).should.throw();
	} );

	it( "provides property `id` representing request-id for associating a response with its related request", () => {
		const message = new IPPMessage();

		message.should.have.property( "id" );
		message.id = 1;
		message.id.should.be.equal( 1 );
	} );

	it( "automatically assigns `id` on first fetch without prior assignment", () => {
		const message = new IPPMessage();

		message.id.should.be.greaterThanOrEqual( 1 );
	} );

	it( "ensures next `id` assigned automatically is succeeding previous one", () => {
		const a = new IPPMessage().id;
		const b = new IPPMessage().id;

		a.should.be.lessThan( b );
	} );

	it( "adopts explicitly provided `id` for automatically assigning successors to that provided ID in upcoming cases", () => {
		const a = new IPPMessage();
		const b = new IPPMessage();
		const c = new IPPMessage();

		a.id.should.be.lessThan( 10000 );
		b.id = 10000;
		c.id.should.be.greaterThan( 10000 );
	} );

	it( "restarts with ID 1 on running beyond maximum ID value on automatically assigning `id`", () => {
		const a = new IPPMessage();
		const b = new IPPMessage();
		const c = new IPPMessage();
		const d = new IPPMessage();

		a.id = Data.MaxInteger - 1;
		b.id.should.be.equal( Data.MaxInteger );
		c.id.should.be.equal( 1 );

		a.id = Data.MaxInteger;
		d.id.should.be.equal( 1 );
	} );

	it( "rejects assignment of invalid request-id", () => {
		const a = new IPPMessage();

		( () => { a.id = null; } ).should.throw();
		( () => { a.id = undefined; } ).should.throw();
		( () => { a.id = ""; } ).should.throw();
		( () => { a.id = true; } ).should.throw();
		( () => { a.id = false; } ).should.throw();
		( () => { a.id = []; } ).should.throw();
		( () => { a.id = {}; } ).should.throw();
		( () => { a.id = { id: 1 }; } ).should.throw();
		( () => { a.id = () => 1; } ).should.throw();
	} );

	it( "exposes message payload in property `data`", () => {
		new IPPMessage().should.have.property( "data" ).which.is.instanceOf( Buffer ).and.is.empty();
	} );

	it( "accepts Buffer assigned to be message payload", () => {
		const a = new IPPMessage();
		const chunk = Buffer.from( [ 1, 2, 3, 4 ] );

		a.data = chunk;
		a.data.should.be.instanceOf( Buffer ).which.is.deepEqual( chunk );
	} );

	it( "requires assigned message payload to be Buffer", () => {
		( () => { new IPPMessage().data = Buffer.alloc( 0 ); } ).should.not.throw();
		( () => { new IPPMessage().data = Buffer.from( [0] ); } ).should.not.throw();

		( () => { new IPPMessage().data = undefined; } ).should.throw();
		( () => { new IPPMessage().data = null; } ).should.throw();
		( () => { new IPPMessage().data = ""; } ).should.throw();
		( () => { new IPPMessage().data = " "; } ).should.throw();
		( () => { new IPPMessage().data = "0"; } ).should.throw();
		( () => { new IPPMessage().data = "foo"; } ).should.throw();
		( () => { new IPPMessage().data = 1; } ).should.throw();
		( () => { new IPPMessage().data = true; } ).should.throw();
		( () => { new IPPMessage().data = false; } ).should.throw();
		( () => { new IPPMessage().data = []; } ).should.throw();
		( () => { new IPPMessage().data = [ 1, 2, 3, 4 ]; } ).should.throw();
		( () => { new IPPMessage().data = [Buffer.alloc( 0 )]; } ).should.throw();
		( () => { new IPPMessage().data = {}; } ).should.throw();
		( () => { new IPPMessage().data = { data: Buffer.alloc( 0 ) }; } ).should.throw();
		( () => { new IPPMessage().data = { value: Buffer.alloc( 0 ) }; } ).should.throw();
		( () => { new IPPMessage().data = () => Buffer.alloc( 0 ); } ).should.throw();
	} );

	describe( "provides static method `createRequest()` for creating request messages which", () => {
		it( "is a function", () => {
			IPPMessage.createRequest.should.be.a.Function();
		} );

		it( "accepts name of operation", () => {
			const a = IPPMessage.createRequest( "CreateJob" );
			a.code = Operation.CreateJob;
		} );

		it( "accepts code of operation", () => {
			const a = IPPMessage.createRequest( Operation.CreateJob );
			a.code = Operation.CreateJob;
		} );

		it( "fails when invoked without any argument", () => {
			( () => IPPMessage.createRequest() ).should.throw();
		} );

		it( "fails when invoked with invalid argument", () => {
			( () => IPPMessage.createRequest( undefined ) ).should.throw();
			( () => IPPMessage.createRequest( null ) ).should.throw();
			( () => IPPMessage.createRequest( "" ) ).should.throw();
			( () => IPPMessage.createRequest( " " ) ).should.throw();
			( () => IPPMessage.createRequest( "." ) ).should.throw();
			( () => IPPMessage.createRequest( "1" ) ).should.throw();
			( () => IPPMessage.createRequest( [] ) ).should.throw();
			( () => IPPMessage.createRequest( ["1"] ) ).should.throw();
			( () => IPPMessage.createRequest( { value: "CreateJob" } ) ).should.throw();
			( () => IPPMessage.createRequest( { toString: () => "CreateJob" } ) ).should.throw();
			( () => IPPMessage.createRequest( 1000000 ) ).should.throw();
			( () => IPPMessage.createRequest( -1 ) ).should.throw();
			( () => IPPMessage.createRequest( 0 ) ).should.throw();
		} );

		it( "automatically assigns request ID", () => {
			IPPMessage.createRequest( "CreateJob" ).id = 10000;
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( 10001 );

			IPPMessage.createRequest( "CreateJob" ).id = Data.MaxInteger - 1;
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( Data.MaxInteger );
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( 1 );
		} );
	} );

	describe( "provides static method `getOperationName()` for looking up name of operation selected by its ID which", () => {
		it( "is a function", () => {
			IPPMessage.getOperationName.should.be.a.Function();
		} );

		it( "returns name of provided valid operation ID", () => {
			Object.keys( Operation )
				.forEach( name => {
					const code = Operation[name];

					IPPMessage.getOperationName( code ).should.be.String().which.is.equal( name );
				} );
		} );

		it( "accepts code of operation", () => {
			const a = IPPMessage.createRequest( Operation.CreateJob );
			a.code = Operation.CreateJob;
		} );

		it( "fails when invoked without any argument", () => {
			( () => IPPMessage.createRequest() ).should.throw();
		} );

		it( "fails when invoked with invalid argument", () => {
			( () => IPPMessage.createRequest( undefined ) ).should.throw();
			( () => IPPMessage.createRequest( null ) ).should.throw();
			( () => IPPMessage.createRequest( "" ) ).should.throw();
			( () => IPPMessage.createRequest( " " ) ).should.throw();
			( () => IPPMessage.createRequest( "." ) ).should.throw();
			( () => IPPMessage.createRequest( "1" ) ).should.throw();
			( () => IPPMessage.createRequest( [] ) ).should.throw();
			( () => IPPMessage.createRequest( ["1"] ) ).should.throw();
			( () => IPPMessage.createRequest( { value: "CreateJob" } ) ).should.throw();
			( () => IPPMessage.createRequest( { toString: () => "CreateJob" } ) ).should.throw();
			( () => IPPMessage.createRequest( 1000000 ) ).should.throw();
			( () => IPPMessage.createRequest( -1 ) ).should.throw();
			( () => IPPMessage.createRequest( 0 ) ).should.throw();
		} );

		it( "automatically assigns request ID", () => {
			IPPMessage.createRequest( "CreateJob" ).id = 10000;
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( 10001 );

			IPPMessage.createRequest( "CreateJob" ).id = Data.MaxInteger - 1;
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( Data.MaxInteger );
			IPPMessage.createRequest( "CreateJob" ).id.should.be.equal( 1 );
		} );
	} );

	describe( "provides method `deriveResponse()` for deriving response related to given message which", () => {
		it( "is a function", () => {
			new IPPMessage().deriveResponse.should.be.a.Function();
		} );

		it( "works on sufficiently prepared request", () => {
			const request = IPPMessage.createRequest( "CreateJob" )
				.setOperationAttribute( "op-default", new TypeDefault() );

			request.version = "1.0";

			request.deriveResponse.should.be.Function();

			const response = request.deriveResponse();

			response.should.be.instanceOf( IPPMessage );
			response.code.should.be.equal( Status.successfulOk );
			response.id.should.be.equal( request.id );
			response.version.should.be.equal( request.version );
		} );

		it( "fails on providing invalid status-code for response", () => {
			const request = IPPMessage.createRequest( "CreateJob" );

			( () => request.deriveResponse( "" ) ).should.throw();
			( () => request.deriveResponse( " " ) ).should.throw();
			( () => request.deriveResponse( null ) ).should.throw();
			( () => request.deriveResponse( true ) ).should.throw();
			( () => request.deriveResponse( false ) ).should.throw();
			( () => request.deriveResponse( [] ) ).should.throw();
			( () => request.deriveResponse( {} ) ).should.throw();
			( () => request.deriveResponse( { value: 1 } ) ).should.throw();
		} );

		it( "accepts status message in second argument", () => {
			const request = IPPMessage.createRequest( "CreateJob" );

			request.deriveResponse( Status.successfulOk ).attributes.operation.has( "status-message" ).should.be.false();
			request.deriveResponse( Status.successfulOk, "test" ).attributes.operation.has( "status-message" ).should.be.true();
		} );
	} );

	describe( "provides method `toBuffer()` for encoding IPP message which", () => {
		it( "fails on encoding w/o assigning operation-id or status-code first", () => {
			const a = new IPPMessage();

			( () => a.toBuffer() ).should.throw();

			a.code = "CreateJob";

			( () => a.toBuffer() ).should.not.throw();
		} );

		it( "fails on encoding message using version out of bounds", () => {
			const a = new IPPMessage();
			a.code = "CreateJob";

			a.version = "1.128";
			( () => a.toBuffer() ).should.throw();

			a.version = "1.127";
			( () => a.toBuffer() ).should.not.throw();

			a.version = "128.1";
			( () => a.toBuffer() ).should.throw();

			a.version = "127.1";
			( () => a.toBuffer() ).should.not.throw();
		} );

		it( "includes message payload", () => {
			const a = IPPMessage.createRequest( "CreateJob" );
			const sizeWithout = a.toBuffer().length;

			const data = Buffer.from( [ 1, 2, 3, 4 ] );
			a.data = data;

			a.toBuffer().length.should.be.equal( sizeWithout + data.length );
		} );
	} );

	describe( "supports parsing encoded IPP messages for it", () => {
		it( "is accepting encoded message on construction", () => {
			new IPPMessage( IPPMessage.createRequest( "CreateJob" ).toBuffer() );
		} );

		it( "is rejecting too small buffers provided on construction", () => {
			( () => new IPPMessage( Buffer.alloc( 7 ) ) ).should.throw( /too small/ );
			( () => new IPPMessage( Buffer.alloc( 8 ) ) ).should.not.throw( /too small/ );
		} );
	} );

	describe( "supports function for setting attributes which", () => {
		it( "is named `set*Attribute()` for setting attribute of either group of attributes", () => {
			new IPPMessage().setOperationAttribute.should.be.a.Function();
			new IPPMessage().setJobAttribute.should.be.a.Function();
			new IPPMessage().setPrinterAttribute.should.be.a.Function();
			new IPPMessage().setUnsupportedAttribute.should.be.a.Function();
		} );

		it( "returns message for daisy-chaining calls", () => {
			const message = new IPPMessage();

			message.setOperationAttribute( "a" ).should.be.equal( message );
			message.setJobAttribute( "a" ).should.be.equal( message );
			message.setPrinterAttribute( "a" ).should.be.equal( message );
			message.setUnsupportedAttribute( "a" ).should.be.equal( message );
		} );

		it( "requires name in first argument", () => {
			( () => new IPPMessage().setOperationAttribute() ).should.throw();
			( () => new IPPMessage().setJobAttribute() ).should.throw();
			( () => new IPPMessage().setPrinterAttribute() ).should.throw();
			( () => new IPPMessage().setUnsupportedAttribute() ).should.throw();

			( () => new IPPMessage().setOperationAttribute( "a" ) ).should.not.throw();
			( () => new IPPMessage().setJobAttribute( "a" ) ).should.not.throw();
			( () => new IPPMessage().setPrinterAttribute( "a" ) ).should.not.throw();
			( () => new IPPMessage().setUnsupportedAttribute( "a" ) ).should.not.throw();
		} );

		it( "causes set attribute instantly exposed in related group of attributes", () => {
			new IPPMessage().setOperationAttribute( "a" ).attributes.operation.has( "a" ).should.be.true();
			new IPPMessage().setOperationAttribute( "a" ).attributes.printer.has( "a" ).should.be.false();
			new IPPMessage().setOperationAttribute( "a" ).attributes.job.has( "a" ).should.be.false();
			new IPPMessage().setOperationAttribute( "a" ).attributes.unsupported.has( "a" ).should.be.false();

			new IPPMessage().setPrinterAttribute( "a" ).attributes.operation.has( "a" ).should.be.false();
			new IPPMessage().setPrinterAttribute( "a" ).attributes.printer.has( "a" ).should.be.true();
			new IPPMessage().setPrinterAttribute( "a" ).attributes.job.has( "a" ).should.be.false();
			new IPPMessage().setPrinterAttribute( "a" ).attributes.unsupported.has( "a" ).should.be.false();

			new IPPMessage().setJobAttribute( "a" ).attributes.operation.has( "a" ).should.be.false();
			new IPPMessage().setJobAttribute( "a" ).attributes.printer.has( "a" ).should.be.false();
			new IPPMessage().setJobAttribute( "a" ).attributes.job.has( "a" ).should.be.true();
			new IPPMessage().setJobAttribute( "a" ).attributes.unsupported.has( "a" ).should.be.false();

			new IPPMessage().setUnsupportedAttribute( "a" ).attributes.operation.has( "a" ).should.be.false();
			new IPPMessage().setUnsupportedAttribute( "a" ).attributes.printer.has( "a" ).should.be.false();
			new IPPMessage().setUnsupportedAttribute( "a" ).attributes.job.has( "a" ).should.be.false();
			new IPPMessage().setUnsupportedAttribute( "a" ).attributes.unsupported.has( "a" ).should.be.true();
		} );

		it( "implicitly wraps native values in instances of `Type`", () => {
			new IPPMessage().setOperationAttribute( "a" ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setOperationAttribute( "a", null ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setOperationAttribute( "a", 1 ).attributes.operation.get( "a" ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 1 );
			new IPPMessage().setOperationAttribute( "a", "foo" ).attributes.operation.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
			new IPPMessage().setOperationAttribute( "a", new Date() ).attributes.operation.get( "a" ).should.be.instanceOf( TypeDateTime );
			new IPPMessage().setOperationAttribute( "a", Buffer.from( [ 1, 2 ] ) ).attributes.operation.get( "a" ).should.be.instanceOf( TypeOctetString );
			new IPPMessage().setOperationAttribute( "a", true ).attributes.operation.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setOperationAttribute( "a", false ).attributes.operation.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setOperationAttribute( "a", { toString: () => "converted" } ).attributes.operation.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "converted" );

			new IPPMessage().setPrinterAttribute( "a" ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setPrinterAttribute( "a", null ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setPrinterAttribute( "a", 1 ).attributes.printer.get( "a" ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 1 );
			new IPPMessage().setPrinterAttribute( "a", "foo" ).attributes.printer.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
			new IPPMessage().setPrinterAttribute( "a", new Date() ).attributes.printer.get( "a" ).should.be.instanceOf( TypeDateTime );
			new IPPMessage().setPrinterAttribute( "a", Buffer.from( [ 1, 2 ] ) ).attributes.printer.get( "a" ).should.be.instanceOf( TypeOctetString );
			new IPPMessage().setPrinterAttribute( "a", true ).attributes.printer.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setPrinterAttribute( "a", false ).attributes.printer.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setPrinterAttribute( "a", { toString: () => "converted" } ).attributes.printer.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "converted" );

			new IPPMessage().setJobAttribute( "a" ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setJobAttribute( "a", null ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setJobAttribute( "a", 1 ).attributes.job.get( "a" ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 1 );
			new IPPMessage().setJobAttribute( "a", "foo" ).attributes.job.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
			new IPPMessage().setJobAttribute( "a", new Date() ).attributes.job.get( "a" ).should.be.instanceOf( TypeDateTime );
			new IPPMessage().setJobAttribute( "a", Buffer.from( [ 1, 2 ] ) ).attributes.job.get( "a" ).should.be.instanceOf( TypeOctetString );
			new IPPMessage().setJobAttribute( "a", true ).attributes.job.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setJobAttribute( "a", false ).attributes.job.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setJobAttribute( "a", { toString: () => "converted" } ).attributes.job.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "converted" );

			new IPPMessage().setUnsupportedAttribute( "a" ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setUnsupportedAttribute( "a", null ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );
			new IPPMessage().setUnsupportedAttribute( "a", 1 ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeInteger ).which.has.property( "value" ).which.is.equal( 1 );
			new IPPMessage().setUnsupportedAttribute( "a", "foo" ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "foo" );
			new IPPMessage().setUnsupportedAttribute( "a", new Date() ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeDateTime );
			new IPPMessage().setUnsupportedAttribute( "a", Buffer.from( [ 1, 2 ] ) ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeOctetString );
			new IPPMessage().setUnsupportedAttribute( "a", true ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setUnsupportedAttribute( "a", false ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeBoolean );
			new IPPMessage().setUnsupportedAttribute( "a", { toString: () => "converted" } ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeTextWithoutLanguage ).which.has.property( "value" ).which.is.equal( "converted" );
		} );

		it( "accepts third argument controlling whether provided value should replace or extend selected attribute", () => {
			const a = new IPPMessage();

			a.setOperationAttribute( "a", null ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setPrinterAttribute( "a", null ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setJobAttribute( "a", null ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setUnsupportedAttribute( "a", null ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );

			a.setOperationAttribute( "a", null ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setPrinterAttribute( "a", null ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setJobAttribute( "a", null ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setUnsupportedAttribute( "a", null ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );

			a.setOperationAttribute( "a", null, true ).attributes.operation.get( "a" ).should.be.Array().which.is.deepEqual( [ new TypeNoValue(), new TypeNoValue() ] );
			a.setPrinterAttribute( "a", null, true ).attributes.printer.get( "a" ).should.be.Array().which.is.deepEqual( [ new TypeNoValue(), new TypeNoValue() ] );
			a.setJobAttribute( "a", null, true ).attributes.job.get( "a" ).should.be.Array().which.is.deepEqual( [ new TypeNoValue(), new TypeNoValue() ] );
			a.setUnsupportedAttribute( "a", null, true ).attributes.unsupported.get( "a" ).should.be.Array().which.is.deepEqual( [ new TypeNoValue(), new TypeNoValue() ] );

			a.setOperationAttribute( "a", null ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setPrinterAttribute( "a", null ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setJobAttribute( "a", null ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			a.setUnsupportedAttribute( "a", null ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );

			const b = new IPPMessage();

			b.setOperationAttribute( "a", null, true ).attributes.operation.get( "a" ).should.be.instanceOf( TypeNoValue );
			b.setPrinterAttribute( "a", null, true ).attributes.printer.get( "a" ).should.be.instanceOf( TypeNoValue );
			b.setJobAttribute( "a", null, true ).attributes.job.get( "a" ).should.be.instanceOf( TypeNoValue );
			b.setUnsupportedAttribute( "a", null, true ).attributes.unsupported.get( "a" ).should.be.instanceOf( TypeNoValue );
		} );
	} );
} );
