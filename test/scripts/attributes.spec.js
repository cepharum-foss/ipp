/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const {
	GuidedAttributes,
	TypeUnknown, TypeNoValue, TypeDefault,
	TypeInteger, TypeBoolean, TypeEnum,
	TypeTextWithoutLanguage, TypeTextWithLanguage,
	TypeNameWithoutLanguage, TypeNameWithLanguage,
	TypeKeyword, TypeCharset, TypeNaturalLanguage, TypeMimeMediaType,
	TypeUriScheme, TypeUri,
	TypeOctetString, TypeResolution, TypeRangeOfInteger, TypeDateTime,
} = require( "../../" );

describe( "GuidedAttributes collection", () => {
	it( "can be created", () => {
		new GuidedAttributes();
	} );

	it( "ignores any data provided for initialization", () => {
		new GuidedAttributes( { value: true } ).should.be.empty();
		new GuidedAttributes( [[ "value", true ]] ).should.be.empty();
	} );

	it( "is basically a Map", () => {
		new GuidedAttributes().should.be.instanceOf( Map );
	} );

	describe( "overrides `set()` which", () => {
		it( "is a method for setting/replacing key-based value(s)", () => {
			new GuidedAttributes().should.have.property( "set" ).which.is.a.Function();
		} );

		it( "accepts keys complying with keyword syntax", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.set( "a", new TypeNoValue() );
			map.set( "a-a", new TypeNoValue() );
			map.set( "a1", new TypeNoValue() );
			map.set( "a_1", new TypeNoValue() );
			map.set( "a.1", new TypeNoValue() );

			map.should.have.size( 5 );
		} );

		it( "returns map for daisy-chaining calls", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.set( "a", new TypeNoValue() ).set( "a-a", new TypeNoValue() );

			map.should.have.size( 2 );
		} );

		it( "rejects keys not complying with keyword syntax", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "", new TypeNoValue() ) ).should.throw();
			( () => map.set( " a", new TypeNoValue() ) ).should.throw();
			( () => map.set( " a ", new TypeNoValue() ) ).should.throw();
			( () => map.set( "a ", new TypeNoValue() ) ).should.throw();
			( () => map.set( "-a", new TypeNoValue() ) ).should.throw();
			( () => map.set( "1", new TypeNoValue() ) ).should.throw();
			( () => map.set( "_1", new TypeNoValue() ) ).should.throw();
			( () => map.set( ".1", new TypeNoValue() ) ).should.throw();
			( () => map.set( 1, new TypeNoValue() ) ).should.throw();
			( () => map.set( true, new TypeNoValue() ) ).should.throw();
			( () => map.set( false, new TypeNoValue() ) ).should.throw();
			( () => map.set( null, new TypeNoValue() ) ).should.throw();
			( () => map.set( undefined, new TypeNoValue() ) ).should.throw();
			( () => map.set( [], new TypeNoValue() ) ).should.throw();
			( () => map.set( ["a"], new TypeNoValue() ) ).should.throw();
			( () => map.set( {}, new TypeNoValue() ) ).should.throw();
			( () => map.set( { key: "a" }, new TypeNoValue() ) ).should.throw();
			( () => map.set( { toString: () => "a" }, new TypeNoValue() ) ).should.throw();
			( () => map.set( () => "a", new TypeNoValue() ) ).should.throw();

			map.should.be.empty();
		} );

		it( "accepts values derived from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "a", new TypeNoValue() ) ).should.not.throw();
			( () => map.set( "a", new TypeUnknown() ) ).should.not.throw();
			( () => map.set( "a", new TypeDefault() ) ).should.not.throw();
			( () => map.set( "a", new TypeInteger( 1 ) ) ).should.not.throw();
			( () => map.set( "a", new TypeBoolean( true ) ) ).should.not.throw();
			( () => map.set( "a", new TypeEnum( 1 ) ) ).should.not.throw();
			( () => map.set( "a", new TypeTextWithoutLanguage( "" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeTextWithLanguage( "", "de" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeNameWithoutLanguage( "a" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeNameWithLanguage( "a", "de" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeKeyword( "a" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeCharset( "utf" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeNaturalLanguage( "de" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeUri( "urn:1" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeUriScheme( "urn" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeOctetString( "" ) ) ).should.not.throw();
			( () => map.set( "a", new TypeRangeOfInteger( 1, 1 ) ) ).should.not.throw();
			( () => map.set( "a", new TypeResolution( 1 ) ) ).should.not.throw();
			( () => map.set( "a", new TypeDateTime() ) ).should.not.throw();

			map.should.have.size( 1 );
		} );

		it( "accepts list consisting of values derived from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "a", [new TypeNoValue()] ) ).should.not.throw();
			( () => map.set( "a", [new TypeUnknown()] ) ).should.not.throw();
			( () => map.set( "a", [new TypeDefault()] ) ).should.not.throw();
			( () => map.set( "a", [new TypeInteger( 1 )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeBoolean( true )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeEnum( 1 )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeTextWithoutLanguage( "" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeTextWithLanguage( "", "de" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeNameWithoutLanguage( "a" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeNameWithLanguage( "a", "de" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeKeyword( "a" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeCharset( "utf" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeNaturalLanguage( "de" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeUri( "urn:1" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeUriScheme( "urn" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeOctetString( "" )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeRangeOfInteger( 1, 1 )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeResolution( 1 )] ) ).should.not.throw();
			( () => map.set( "a", [new TypeDateTime()] ) ).should.not.throw();

			( () => map.set( "a", [ new TypeNoValue(), new TypeNoValue() ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeUnknown(), new TypeUnknown() ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeDefault(), new TypeDefault() ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeInteger( 1 ), new TypeInteger( 1 ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeBoolean( true ), new TypeBoolean( true ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeEnum( 1 ), new TypeEnum( 1 ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeTextWithoutLanguage( "" ), new TypeTextWithoutLanguage( "" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeTextWithLanguage( "", "de" ), new TypeTextWithLanguage( "", "de" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeNameWithoutLanguage( "a" ), new TypeNameWithoutLanguage( "a" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeNameWithLanguage( "a", "de" ), new TypeNameWithLanguage( "a", "de" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeKeyword( "a" ), new TypeKeyword( "a" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeCharset( "utf" ), new TypeCharset( "utf" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeNaturalLanguage( "de" ), new TypeNaturalLanguage( "de" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeUri( "urn:1" ), new TypeUri( "urn:1" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeUriScheme( "urn" ), new TypeUriScheme( "urn" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeOctetString( "" ), new TypeOctetString( "" ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeRangeOfInteger( 1, 1 ), new TypeRangeOfInteger( 1, 1 ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeResolution( 1 ), new TypeResolution( 1 ) ] ) ).should.not.throw();
			( () => map.set( "a", [ new TypeDateTime(), new TypeDateTime() ] ) ).should.not.throw();

			map.should.have.size( 1 );
		} );

		it( "rejects values not derived from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "a", undefined ) ).should.throw();
			( () => map.set( "a", null ) ).should.throw();
			( () => map.set( "a", false ) ).should.throw();
			( () => map.set( "a", true ) ).should.throw();
			( () => map.set( "a", "" ) ).should.throw();
			( () => map.set( "a", "1" ) ).should.throw();
			( () => map.set( "a", "a" ) ).should.throw();
			( () => map.set( "a", 0 ) ).should.throw();
			( () => map.set( "a", 1 ) ).should.throw();
			( () => map.set( "a", -2.5 ) ).should.throw();
			( () => map.set( "a", NaN ) ).should.throw();
			( () => map.set( "a", Infinity ) ).should.throw();
			( () => map.set( "a", [] ) ).should.throw();
			( () => map.set( "a", [undefined] ) ).should.throw();
			( () => map.set( "a", [null] ) ).should.throw();
			( () => map.set( "a", [false] ) ).should.throw();
			( () => map.set( "a", [true] ) ).should.throw();
			( () => map.set( "a", [""] ) ).should.throw();
			( () => map.set( "a", ["1"] ) ).should.throw();
			( () => map.set( "a", ["a"] ) ).should.throw();
			( () => map.set( "a", [0] ) ).should.throw();
			( () => map.set( "a", [1] ) ).should.throw();
			( () => map.set( "a", [-2.5] ) ).should.throw();
			( () => map.set( "a", [NaN] ) ).should.throw();
			( () => map.set( "a", [Infinity] ) ).should.throw();
			( () => map.set( "a", [[]] ) ).should.throw();
			( () => map.set( "a", [{}] ) ).should.throw();
			( () => map.set( "a", {} ) ).should.throw();
			( () => map.set( "a", { value: new TypeUnknown() } ) ).should.throw();
			( () => map.set( "a", () => new TypeUnknown() ) ).should.throw();

			map.should.be.empty();
		} );

		it( "rejects list mixing values deriving from `Type` with values not deriving from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "a", [ new TypeUnknown(), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeDefault(), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeInteger( 1 ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeBoolean( true ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeEnum( 1 ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeTextWithoutLanguage( "" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeTextWithLanguage( "", "de" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNameWithoutLanguage( "a" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNameWithLanguage( "a", "de" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeKeyword( "a" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeCharset( "utf" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNaturalLanguage( "de" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeUri( "urn:1" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeUriScheme( "urn" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeOctetString( "" ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeRangeOfInteger( 1, 1 ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeResolution( 1 ), 1 ] ) ).should.throw();
			( () => map.set( "a", [ new TypeDateTime(), 1 ] ) ).should.throw();

			map.should.be.empty();
		} );

		it( "rejects list consisting of values of different types deriving from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.set( "a", [ new TypeNoValue(), new TypeUnknown() ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeDefault() ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeInteger( 1 ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeBoolean( true ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeEnum( 1 ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeTextWithoutLanguage( "" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeTextWithLanguage( "", "de" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeNameWithoutLanguage( "a" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeNameWithLanguage( "a", "de" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeKeyword( "a" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeCharset( "utf" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeNaturalLanguage( "de" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeUri( "urn:1" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeUriScheme( "urn" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeOctetString( "" ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeRangeOfInteger( 1, 1 ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeResolution( 1 ) ] ) ).should.throw();
			( () => map.set( "a", [ new TypeNoValue(), new TypeDateTime() ] ) ).should.throw();

			map.should.be.empty();
		} );

		it( "replaces existing value", () => {
			const map = new GuidedAttributes();

			map.set( "a", new TypeNoValue() );
			map.set( "a", new TypeNoValue() );

			map.get( "a" ).should.be.instanceOf( TypeNoValue );

			map.set( "a", [new TypeNoValue()] );
			map.set( "a", [new TypeNoValue()] );

			map.get( "a" ).should.be.instanceOf( TypeNoValue );
		} );
	} );

	describe( "introduces `add()` which", () => {
		it( "is a method for adding another key-based value(s)", () => {
			new GuidedAttributes().should.have.property( "add" ).which.is.a.Function();
		} );

		it( "accepts keys complying with keyword syntax", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.add( "a", new TypeNoValue() );
			map.add( "a-a", new TypeNoValue() );
			map.add( "a1", new TypeNoValue() );
			map.add( "a_1", new TypeNoValue() );
			map.add( "a.1", new TypeNoValue() );

			map.should.have.size( 5 );
		} );

		it( "returns map for daisy-chaining calls", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.add( "a", new TypeNoValue() ).add( "a-a", new TypeNoValue() );

			map.should.have.size( 2 );
		} );

		it( "rejects keys not complying with keyword syntax when adding to initially missing attribute", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "", new TypeNoValue() ) ).should.throw();
			( () => map.add( "A", new TypeNoValue() ) ).should.throw();
			( () => map.add( " a", new TypeNoValue() ) ).should.throw();
			( () => map.add( " a ", new TypeNoValue() ) ).should.throw();
			( () => map.add( "a ", new TypeNoValue() ) ).should.throw();
			( () => map.add( "-a", new TypeNoValue() ) ).should.throw();
			( () => map.add( "1", new TypeNoValue() ) ).should.throw();
			( () => map.add( "_1", new TypeNoValue() ) ).should.throw();
			( () => map.add( ".1", new TypeNoValue() ) ).should.throw();
			( () => map.add( 1, new TypeNoValue() ) ).should.throw();
			( () => map.add( true, new TypeNoValue() ) ).should.throw();
			( () => map.add( false, new TypeNoValue() ) ).should.throw();
			( () => map.add( null, new TypeNoValue() ) ).should.throw();
			( () => map.add( undefined, new TypeNoValue() ) ).should.throw();
			( () => map.add( [], new TypeNoValue() ) ).should.throw();
			( () => map.add( ["a"], new TypeNoValue() ) ).should.throw();
			( () => map.add( {}, new TypeNoValue() ) ).should.throw();
			( () => map.add( { key: "a" }, new TypeNoValue() ) ).should.throw();
			( () => map.add( { toString: () => "a" }, new TypeNoValue() ) ).should.throw();
			( () => map.add( () => "a", new TypeNoValue() ) ).should.throw();

			map.should.be.empty();
		} );

		it( "accepts values derived from `Type` though constraining to have homogenic lists per attribute", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "a", new TypeNoValue() ) ).should.not.throw();
			( () => map.add( "a", new TypeUnknown() ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeUnknown() ) ).should.not.throw();
			( () => map.add( "a", new TypeDefault() ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeDefault() ) ).should.not.throw();
			( () => map.add( "a", new TypeInteger( 1 ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeInteger( 1 ) ) ).should.not.throw();
			( () => map.add( "a", new TypeBoolean( true ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeBoolean( true ) ) ).should.not.throw();
			( () => map.add( "a", new TypeEnum( 1 ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeEnum( 1 ) ) ).should.not.throw();
			( () => map.add( "a", new TypeTextWithoutLanguage( "" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeTextWithoutLanguage( "" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeTextWithLanguage( "", "de" ) ) ).should.not.throw(); // these two types are considered combinable
			map.clear();
			( () => map.add( "a", new TypeTextWithLanguage( "", "de" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeNameWithoutLanguage( "a" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeNameWithoutLanguage( "a" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeNameWithLanguage( "a", "de" ) ) ).should.not.throw(); // these two types are considered combinable
			map.clear();
			( () => map.add( "a", new TypeNameWithLanguage( "a", "de" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeKeyword( "a" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeKeyword( "a" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeCharset( "utf" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeCharset( "utf" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeNaturalLanguage( "de" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeNaturalLanguage( "de" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeUri( "urn:1" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeUri( "urn:1" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeUriScheme( "urn" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeUriScheme( "urn" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeOctetString( "" ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeOctetString( "" ) ) ).should.not.throw();
			( () => map.add( "a", new TypeRangeOfInteger( 1, 1 ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeRangeOfInteger( 1, 1 ) ) ).should.not.throw();
			( () => map.add( "a", new TypeResolution( 1 ) ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeResolution( 1 ) ) ).should.not.throw();
			( () => map.add( "a", new TypeDateTime() ) ).should.throw();
			map.clear();
			( () => map.add( "a", new TypeDateTime() ) ).should.not.throw();

			map.should.have.size( 1 );
		} );

		it( "rejects list consisting of values derived from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "a", [new TypeNoValue()] ) ).should.throw();
			( () => map.add( "a", [new TypeUnknown()] ) ).should.throw();
			( () => map.add( "a", [new TypeDefault()] ) ).should.throw();
			( () => map.add( "a", [new TypeInteger( 1 )] ) ).should.throw();
			( () => map.add( "a", [new TypeBoolean( true )] ) ).should.throw();
			( () => map.add( "a", [new TypeEnum( 1 )] ) ).should.throw();
			( () => map.add( "a", [new TypeTextWithoutLanguage( "" )] ) ).should.throw();
			( () => map.add( "a", [new TypeTextWithLanguage( "", "de" )] ) ).should.throw();
			( () => map.add( "a", [new TypeNameWithoutLanguage( "a" )] ) ).should.throw();
			( () => map.add( "a", [new TypeNameWithLanguage( "a", "de" )] ) ).should.throw();
			( () => map.add( "a", [new TypeKeyword( "a" )] ) ).should.throw();
			( () => map.add( "a", [new TypeCharset( "utf" )] ) ).should.throw();
			( () => map.add( "a", [new TypeNaturalLanguage( "de" )] ) ).should.throw();
			( () => map.add( "a", [new TypeUri( "urn:1" )] ) ).should.throw();
			( () => map.add( "a", [new TypeUriScheme( "urn" )] ) ).should.throw();
			( () => map.add( "a", [new TypeOctetString( "" )] ) ).should.throw();
			( () => map.add( "a", [new TypeRangeOfInteger( 1, 1 )] ) ).should.throw();
			( () => map.add( "a", [new TypeResolution( 1 )] ) ).should.throw();
			( () => map.add( "a", [new TypeDateTime()] ) ).should.throw();

			( () => map.add( "a", [ new TypeNoValue(), new TypeNoValue() ] ) ).should.throw();
			( () => map.add( "a", [ new TypeUnknown(), new TypeUnknown() ] ) ).should.throw();
			( () => map.add( "a", [ new TypeDefault(), new TypeDefault() ] ) ).should.throw();
			( () => map.add( "a", [ new TypeInteger( 1 ), new TypeInteger( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeBoolean( true ), new TypeBoolean( true ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeEnum( 1 ), new TypeEnum( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeTextWithoutLanguage( "" ), new TypeTextWithoutLanguage( "" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeTextWithLanguage( "", "de" ), new TypeTextWithLanguage( "", "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNameWithoutLanguage( "a" ), new TypeNameWithoutLanguage( "a" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNameWithLanguage( "a", "de" ), new TypeNameWithLanguage( "a", "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeKeyword( "a" ), new TypeKeyword( "a" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeCharset( "utf" ), new TypeCharset( "utf" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNaturalLanguage( "de" ), new TypeNaturalLanguage( "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeUri( "urn:1" ), new TypeUri( "urn:1" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeUriScheme( "urn" ), new TypeUriScheme( "urn" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeOctetString( "" ), new TypeOctetString( "" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeRangeOfInteger( 1, 1 ), new TypeRangeOfInteger( 1, 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeResolution( 1 ), new TypeResolution( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeDateTime(), new TypeDateTime() ] ) ).should.throw();

			map.should.be.empty();
		} );

		it( "rejects values not derived from `Type` on adding to initially missing attribute", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "a", undefined ) ).should.throw();
			( () => map.add( "a", null ) ).should.throw();
			( () => map.add( "a", false ) ).should.throw();
			( () => map.add( "a", true ) ).should.throw();
			( () => map.add( "a", "" ) ).should.throw();
			( () => map.add( "a", "1" ) ).should.throw();
			( () => map.add( "a", "a" ) ).should.throw();
			( () => map.add( "a", 0 ) ).should.throw();
			( () => map.add( "a", 1 ) ).should.throw();
			( () => map.add( "a", -2.5 ) ).should.throw();
			( () => map.add( "a", NaN ) ).should.throw();
			( () => map.add( "a", Infinity ) ).should.throw();
			( () => map.add( "a", [] ) ).should.throw();
			( () => map.add( "a", [undefined] ) ).should.throw();
			( () => map.add( "a", [null] ) ).should.throw();
			( () => map.add( "a", [false] ) ).should.throw();
			( () => map.add( "a", [true] ) ).should.throw();
			( () => map.add( "a", [""] ) ).should.throw();
			( () => map.add( "a", ["1"] ) ).should.throw();
			( () => map.add( "a", ["a"] ) ).should.throw();
			( () => map.add( "a", [0] ) ).should.throw();
			( () => map.add( "a", [1] ) ).should.throw();
			( () => map.add( "a", [-2.5] ) ).should.throw();
			( () => map.add( "a", [NaN] ) ).should.throw();
			( () => map.add( "a", [Infinity] ) ).should.throw();
			( () => map.add( "a", [[]] ) ).should.throw();
			( () => map.add( "a", [{}] ) ).should.throw();
			( () => map.add( "a", {} ) ).should.throw();
			( () => map.add( "a", { value: new TypeUnknown() } ) ).should.throw();
			( () => map.add( "a", () => new TypeUnknown() ) ).should.throw();

			map.should.be.empty();
		} );

		it( "rejects values not derived from `Type` on adding to initially existing attribute", () => {
			const map = new GuidedAttributes();
			map.set( "a", new TypeUnknown() );

			map.should.have.size( 1 );

			( () => map.add( "a", undefined ) ).should.throw();
			( () => map.add( "a", null ) ).should.throw();
			( () => map.add( "a", false ) ).should.throw();
			( () => map.add( "a", true ) ).should.throw();
			( () => map.add( "a", "" ) ).should.throw();
			( () => map.add( "a", "1" ) ).should.throw();
			( () => map.add( "a", "a" ) ).should.throw();
			( () => map.add( "a", 0 ) ).should.throw();
			( () => map.add( "a", 1 ) ).should.throw();
			( () => map.add( "a", -2.5 ) ).should.throw();
			( () => map.add( "a", NaN ) ).should.throw();
			( () => map.add( "a", Infinity ) ).should.throw();
			( () => map.add( "a", [] ) ).should.throw();
			( () => map.add( "a", [undefined] ) ).should.throw();
			( () => map.add( "a", [null] ) ).should.throw();
			( () => map.add( "a", [false] ) ).should.throw();
			( () => map.add( "a", [true] ) ).should.throw();
			( () => map.add( "a", [""] ) ).should.throw();
			( () => map.add( "a", ["1"] ) ).should.throw();
			( () => map.add( "a", ["a"] ) ).should.throw();
			( () => map.add( "a", [0] ) ).should.throw();
			( () => map.add( "a", [1] ) ).should.throw();
			( () => map.add( "a", [-2.5] ) ).should.throw();
			( () => map.add( "a", [NaN] ) ).should.throw();
			( () => map.add( "a", [Infinity] ) ).should.throw();
			( () => map.add( "a", [[]] ) ).should.throw();
			( () => map.add( "a", [{}] ) ).should.throw();
			( () => map.add( "a", {} ) ).should.throw();
			( () => map.add( "a", { value: new TypeUnknown() } ) ).should.throw();
			( () => map.add( "a", () => new TypeUnknown() ) ).should.throw();

			map.should.have.size( 1 );
		} );

		it( "rejects list mixing values deriving from `Type` with values not deriving from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "a", [ new TypeUnknown(), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeDefault(), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeInteger( 1 ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeBoolean( true ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeEnum( 1 ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeTextWithoutLanguage( "" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeTextWithLanguage( "", "de" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNameWithoutLanguage( "a" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNameWithLanguage( "a", "de" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeKeyword( "a" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeCharset( "utf" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNaturalLanguage( "de" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeUri( "urn:1" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeUriScheme( "urn" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeOctetString( "" ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeRangeOfInteger( 1, 1 ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeResolution( 1 ), 1 ] ) ).should.throw();
			( () => map.add( "a", [ new TypeDateTime(), 1 ] ) ).should.throw();

			map.should.be.empty();
		} );

		it( "rejects list consisting of values of different types deriving from `Type`", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.add( "a", [ new TypeNoValue(), new TypeUnknown() ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeDefault() ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeInteger( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeBoolean( true ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeEnum( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeTextWithoutLanguage( "" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeTextWithLanguage( "", "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeNameWithoutLanguage( "a" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeNameWithLanguage( "a", "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeKeyword( "a" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeCharset( "utf" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeNaturalLanguage( "de" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeUri( "urn:1" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeUriScheme( "urn" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeOctetString( "" ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeRangeOfInteger( 1, 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeResolution( 1 ) ] ) ).should.throw();
			( () => map.add( "a", [ new TypeNoValue(), new TypeDateTime() ] ) ).should.throw();

			map.should.be.empty();
		} );

		it( "extends existing value", () => {
			const map = new GuidedAttributes();

			map.add( "a", new TypeNoValue() );
			map.add( "a", new TypeNoValue() );

			map.get( "a" ).should.be.Array().which.is.deepEqual( [ new TypeNoValue(), new TypeNoValue() ] );
		} );
	} );

	describe( "introduces `remove()` which", () => {
		it( "is a method for removing single value of attribute selected by its key", () => {
			new GuidedAttributes().should.have.property( "remove" ).which.is.a.Function();
		} );

		it( "accepts to be called with instance of `Type` in second argument", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.remove( "a", new TypeUnknown() ) ).should.not.throw();
			( () => map.remove( "a", new TypeNoValue() ) ).should.not.throw();
			( () => map.remove( "a", new TypeDefault() ) ).should.not.throw();
			( () => map.remove( "a", new TypeInteger( 1 ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeBoolean( true ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeEnum( 5 ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeTextWithoutLanguage( "" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeTextWithLanguage( "", "de" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeNameWithoutLanguage( "a" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeNameWithLanguage( "a", "de" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeKeyword( "a" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeCharset( "utf" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeNaturalLanguage( "de" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeMimeMediaType( "test/html" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeUri( "urn:1" ) ) ).should.not.throw();
			( () => map.remove( "a", new TypeUriScheme( "urn" ) ) ).should.not.throw();

			map.should.be.empty();
		} );

		it( "rejects to be called without instance of `Type` in second argument", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			( () => map.remove( "a", null ) ).should.throw();
			( () => map.remove( "a", undefined ) ).should.throw();
			( () => map.remove( "a", 0 ) ).should.throw();
			( () => map.remove( "a", 1 ) ).should.throw();
			( () => map.remove( "a", -23.54 ) ).should.throw();
			( () => map.remove( "a", "0" ) ).should.throw();
			( () => map.remove( "a", "" ) ).should.throw();
			( () => map.remove( "a", true ) ).should.throw();
			( () => map.remove( "a", false ) ).should.throw();
			( () => map.remove( "a", [0] ) ).should.throw();
			( () => map.remove( "a", { value: 0 } ) ).should.throw();
			( () => map.remove( "a", () => 0 ) ).should.throw();
			( () => map.remove( "a", [] ) ).should.throw();
			( () => map.remove( "a", {} ) ).should.throw();

			map.should.be.empty();
		} );

		it( "returns map for daisy-chaining calls", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.remove( "a", new TypeUnknown() ).remove( "a-a", new TypeUnknown() );

			map.should.be.empty();
		} );

		it( "doesn't change map on request for deleting some value that does not exist", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.set( "a", [ new TypeInteger( 3 ), new TypeInteger( 5 ) ] );

			map.should.have.size( 1 );
			map.get( "a" ).should.be.Array().which.has.length( 2 );

			map.remove( "a", new TypeInteger( 4 ) );

			map.should.have.size( 1 );
			map.get( "a" ).should.be.Array().which.has.length( 2 );
		} );

		it( "deletes matching value from list of values of attributes selected by its key", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.set( "a", [ new TypeInteger( 3 ), new TypeInteger( 5 ) ] );

			map.should.have.size( 1 );
			map.get( "a" ).should.be.Array().which.has.length( 2 );

			map.remove( "a", new TypeInteger( 5 ) );

			map.should.have.size( 1 );
			map.get( "a" ).should.not.be.Array().which.is.instanceOf( TypeInteger ).and.has.property( "value" ).which.is.equal( 3 );
		} );

		it( "drops attribute on deleting its last existing value", () => {
			const map = new GuidedAttributes();
			map.should.be.empty();

			map.set( "a", [ new TypeInteger( 3 ), new TypeInteger( 5 ) ] );

			map.should.have.size( 1 );
			map.get( "a" ).should.be.Array().which.has.length( 2 );

			map.remove( "a", new TypeInteger( 5 ) ).remove( "a", new TypeInteger( 3 ) );

			map.should.be.empty();
		} );
	} );

	describe( "overrides `get()` which", () => {
		it( "is a method for reading key-based value(s)", () => {
			new GuidedAttributes().should.have.property( "get" ).which.is.a.Function();
		} );

		it( "throws on trying to fetch missing attribute", () => {
			( () => new GuidedAttributes().get( "missing" ) ).should.throw();
		} );

		it( "accepts fallback in second argument to be returned instead of throwing on trying to fetch missing attribute", () => {
			( () => new GuidedAttributes().get( "missing", new TypeInteger( 1 ) ) ).should.not.throw();
		} );

		it( "rejects fallback in second argument unless its derived from `Type`", () => {
			( () => new GuidedAttributes().get( "missing", 1 ) ).should.throw();
			( () => new GuidedAttributes().get( "missing", "" ) ).should.throw();
			( () => new GuidedAttributes().get( "missing", "1" ) ).should.throw();
			( () => new GuidedAttributes().get( "missing", true ) ).should.throw();
			( () => new GuidedAttributes().get( "missing", false ) ).should.throw();
			( () => new GuidedAttributes().get( "missing", new Date() ) ).should.throw();

			( () => new GuidedAttributes().get( "missing", new TypeInteger( 1 ) ) ).should.not.throw();
			( () => new GuidedAttributes().get( "missing", new TypeTextWithoutLanguage( "" ) ) ).should.not.throw();
			( () => new GuidedAttributes().get( "missing", new TypeNameWithoutLanguage( "1" ) ) ).should.not.throw();
			( () => new GuidedAttributes().get( "missing", new TypeBoolean( true ) ) ).should.not.throw();
			( () => new GuidedAttributes().get( "missing", new TypeBoolean( false ) ) ).should.not.throw();
			( () => new GuidedAttributes().get( "missing", new TypeDateTime( new Date() ) ) ).should.not.throw();
		} );
	} );
} );
