/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { Readable, Writable } = require( "stream" );
const EventEmitter = require( "events" );

const { describe, it } = require( "mocha" );
require( "should" );

const { consume, emitBuffer } = require( "../helper" );
const {
	Operation, IPPMessage, IPPMessageStreamParser,
	TypeUnknown, getParsingStream,
} = require( "../../" );


describe( "Stream parser for IPP messages", () => {
	it( "can be created via constructor", () => {
		new IPPMessageStreamParser();
	} );

	it( "can be created via exposed helper", () => {
		getParsingStream.should.be.Function();
		getParsingStream().should.be.instanceOf( IPPMessageStreamParser );
	} );

	it( "is a Readable", () => {
		new IPPMessageStreamParser().should.be.instanceOf( Readable );
	} );

	it( "is a Writable", () => {
		new IPPMessageStreamParser().should.be.instanceOf( Writable );
	} );

	it( "is an EventEmitter", () => {
		new IPPMessageStreamParser().should.be.instanceOf( EventEmitter );
	} );

	describe( "accepts options provided on constructor which", () => {
		it( "may be empty object", () => {
			new IPPMessageStreamParser( {} );
		} );

		it( "may enable object-mode to be ignored", () => {
			const stream = new IPPMessageStreamParser( { objectMode: true } );

			if ( "readableObjectMode" in stream ) {
				stream.readableObjectMode.should.be.false();
			}

			if ( "writableObjectMode" in stream ) {
				stream.writableObjectMode.should.be.false();
			}
		} );
	} );

	it( "recovers message feed octet by octet", () => {
		const input = IPPMessage.createRequest( Operation.CreateJob )
			.setOperationAttribute( "op-unknown", new TypeUnknown() );

		/** @type IPPMessage */
		let output;

		const tx = emitBuffer( input.toBuffer(), 1 );
		const rx = new IPPMessageStreamParser();

		rx.once( "message", message => { output = message; } );

		tx.pipe( rx );

		return consume( rx )
			.then( payload => {
				payload.should.be.instanceOf( Buffer ).which.is.empty();

				output.should.be.instanceOf( IPPMessage );
				output.should.not.be.equal( input );
				output.code.should.be.equal( Operation.CreateJob );
				output.attributes.operation.get( "op-unknown" ).should.be.instanceOf( TypeUnknown );
			} );
	} );

	it( "recovers message feed as instantly as possible", () => {
		const input = IPPMessage.createRequest( Operation.CreateJob )
			.setOperationAttribute( "op-unknown", new TypeUnknown() );

		/** @type IPPMessage */
		let output;

		const tx = emitBuffer( input.toBuffer(), 4096 );
		const rx = new IPPMessageStreamParser();

		rx.once( "message", message => { output = message; } );

		tx.pipe( rx );

		return consume( rx )
			.then( payload => {
				payload.should.be.instanceOf( Buffer ).which.is.empty();

				output.should.be.instanceOf( IPPMessage );
				output.should.not.be.equal( input );
				output.code.should.be.equal( Operation.CreateJob );
				output.attributes.operation.get( "op-unknown" ).should.be.instanceOf( TypeUnknown );
			} );
	} );

	it( "forwards any payload following parsed message when fed octet by octet", () => {
		const input = IPPMessage.createRequest( Operation.CreateJob )
			.setOperationAttribute( "op-unknown", new TypeUnknown() );
		const data = Buffer.from( new Array( 256 ).fill( 0 ).map( ( _, i ) => i ) );

		/** @type IPPMessage */
		let output;

		const tx = emitBuffer( input.toBuffer(), 1, data );
		const rx = new IPPMessageStreamParser();

		rx.once( "message", message => { output = message; } );

		tx.pipe( rx );

		return consume( rx )
			.then( payload => {
				payload.should.be.instanceOf( Buffer ).which.has.length( 256 );
				payload.equals( data ).should.be.true();

				output.should.be.instanceOf( IPPMessage );
				output.should.not.be.equal( input );
				output.code.should.be.equal( Operation.CreateJob );
				output.attributes.operation.get( "op-unknown" ).should.be.instanceOf( TypeUnknown );
			} );
	} );
} );
