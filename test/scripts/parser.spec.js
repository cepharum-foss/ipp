/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Library = require( "../../" );
const { IPPMessage, Operation, TypeResolution } = Library;

const Groups = [ "printer", "operation", "job", "unsupported" ];
const Enum = [ "one", "two", "three", "four", "five" ];
const TaggedValues = {
	a: () => new Library.TypeDefault(),
	b: () => new Library.TypeUnknown(),
	c: () => new Library.TypeNoValue(),
	d: i => new Library.TypeInteger( i ),
	e: i => new Library.TypeBoolean( i ),
	f: i => new Library.TypeEnum( i + 1, Enum ),
	g: i => new Library.TypeEnum( Enum[i], Enum ),
	h: i => new Library.TypeOctetString( Buffer.from( new Array( i ).fill( 0 ).map( ( _, j ) => j ) ) ),
	i: i => new Library.TypeDateTime( new Date( `2020-03-03T${( "0" + i ).slice( -2 )}:00:00Z` ) ),
	j: i => new Library.TypeResolution( 50 + ( i * 50 ), 1000 - ( i * 50 ), TypeResolution.PerCm ),
	k: i => new Library.TypeRangeOfInteger( i * -10000, i * 500000 ),
	l: i => new Library.TypeTextWithLanguage( Groups[i % Groups.length], "en-us", 7 ),
	m: i => new Library.TypeNameWithLanguage( Groups[i % Groups.length], "fr-ca", 5 ),
	n: i => new Library.TypeTextWithoutLanguage( Groups[i % Groups.length], 3 ),
	o: i => new Library.TypeNameWithoutLanguage( Groups[i % Groups.length], 2 ),
	p: i => new Library.TypeKeyword( Groups[i % Groups.length], 63 ),
	q: i => new Library.TypeUri( "urn:" + Groups[i % Groups.length], 63 ),
	r: i => new Library.TypeUriScheme( Groups[i % Groups.length] ),
	s: i => new Library.TypeCharset( [ "utf8", "iso-8859-1", "cp-1252", "ucs2", "ascii" ][i] ),
	t: i => new Library.TypeNaturalLanguage( [ "de-de", "en", "en-us", "ja-jp", "fr-ca", "es-es" ][i] ),
	u: i => new Library.TypeMimeMediaType( [ "text/plain", "application/pdf", "application/postscript", "application/pcl", "application/octet-stream" ][i] ),
};

describe( "IPPMessage parser", () => {
	it( "can read a message it has compiled before", () => {
		const source = new IPPMessage();

		Groups.forEach( ( group, groupIndex ) => {
			for ( const [ tag, value ] of Object.entries( TaggedValues ) ) {
				source.setAttribute( group, `${group}-${tag}`, value( groupIndex ) );
			}
		} );

		source.code = Operation.SendDocument;

		const target = new IPPMessage( source.toBuffer() );

		Groups.forEach( ( group, groupIndex ) => {
			for ( const [ tag, value ] of Object.entries( TaggedValues ) ) {
				const compareWith = value( groupIndex );

				const reparsed = target.attributes[group].get( `${group}-${tag}` );

				if ( reparsed instanceof Library.TypeEnum ) {
					reparsed.enumeration = Enum;
				}

				reparsed.should.be.instanceOf( compareWith.constructor ).which.is.deepEqual( compareWith );
			}
		} );

		const alt = Library.parse( source.toBuffer() );

		Groups.forEach( ( group, groupIndex ) => {
			for ( const [ tag, value ] of Object.entries( TaggedValues ) ) {
				const compareWith = value( groupIndex );

				const reparsed = alt.attributes[group].get( `${group}-${tag}` );

				if ( reparsed instanceof Library.TypeEnum ) {
					reparsed.enumeration = Enum;
				}

				reparsed.should.be.instanceOf( compareWith.constructor ).which.is.deepEqual( compareWith );
			}
		} );
	} );
} );
