/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const { TypeBoolean, TypeInteger, TypeKeyword, TypeTextWithoutLanguage, AttributeType: { boolean } } = require( "../../../../" );


describe( "A boolean-type value", () => {
	it( "can be instantiated", () => {
		new TypeBoolean( false ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( true ).should.have.property( "value" ).which.is.equal( true );

		new TypeBoolean( 0 ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( -3.6 ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( 7654321 ).should.have.property( "value" ).which.is.equal( true );

		new TypeBoolean( "" ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( "0" ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( "1" ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( "yes" ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( "NO" ).should.have.property( "value" ).which.is.equal( false );

		( () => new TypeBoolean( "hello" ) ).should.throw();
	} );

	it( "extends abstract Type", () => {
		new TypeBoolean( false ).should.be.instanceOf( Type );
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeBoolean( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( true );
	} );

	it( "can be instantiated with keyword-type value", () => {
		new TypeBoolean( new TypeKeyword( "n" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeKeyword( "no" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeKeyword( "off" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeKeyword( "false" ) ).should.have.property( "value" ).which.is.equal( false );

		new TypeBoolean( new TypeKeyword( "y" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeKeyword( "yes" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeKeyword( "on" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeKeyword( "true" ) ).should.have.property( "value" ).which.is.equal( true );
	} );

	it( "can be instantiated with textWithoutLanguage-type value", () => {
		new TypeBoolean( new TypeTextWithoutLanguage( "" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeTextWithoutLanguage( "n" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeTextWithoutLanguage( "No" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeTextWithoutLanguage( "ofF" ) ).should.have.property( "value" ).which.is.equal( false );
		new TypeBoolean( new TypeTextWithoutLanguage( "faLSe" ) ).should.have.property( "value" ).which.is.equal( false );

		new TypeBoolean( new TypeTextWithoutLanguage( "1" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeTextWithoutLanguage( "y" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeTextWithoutLanguage( "YES" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeTextWithoutLanguage( "on" ) ).should.have.property( "value" ).which.is.equal( true );
		new TypeBoolean( new TypeTextWithoutLanguage( "tRUe" ) ).should.have.property( "value" ).which.is.equal( true );
	} );

	it( "rejects to be instantiated with textWithoutLanguage-type value not providing string that can't be cast to boolean", () => {
		( () => new TypeBoolean( new TypeTextWithoutLanguage( "foo" ) ) ).should.throw();
		( () => new TypeBoolean( new TypeTextWithoutLanguage( "BAR" ) ) ).should.throw();
	} );

	it( "exposes type identifier", () => {
		new TypeBoolean( false ).should.have.property( "type" ).which.is.equal( boolean );
	} );

	it( "can be adjusted", () => {
		const value = new TypeBoolean( false );
		value.value.should.be.equal( false );

		value.value = true;
		value.value.should.be.equal( true );

		value.value = false;
		value.value.should.be.equal( false );
	} );

	it( "rejects incompatible data on adjustment", () => {
		const value = new TypeBoolean( false );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
		( () => { value.value = "some string"; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
		( () => { value.value = [123]; } ).should.throw();
		( () => { value.value = {}; } ).should.throw();
		( () => { value.value = { value: 123 }; } ).should.throw();
		( () => { value.value = function() {}; } ).should.throw();
	} );

	it( "casts type on adjustment", () => {
		const value = new TypeBoolean( "false" );
		value.value.should.be.equal( false );

		value.value = "true";
		value.value.should.be.equal( true );

		value.value = "";
		value.value.should.be.equal( false );

		value.value = "1";
		value.value.should.be.equal( true );

		value.value = "0";
		value.value.should.be.equal( false );

		value.value = "-200";
		value.value.should.be.equal( true );

		value.value = "off";
		value.value.should.be.equal( false );

		value.value = "on";
		value.value.should.be.equal( true );

		value.value = "no";
		value.value.should.be.equal( false );

		value.value = "yes";
		value.value.should.be.equal( true );

		value.value = "n";
		value.value.should.be.equal( false );

		value.value = "y";
		value.value.should.be.equal( true );

		value.value = "clear";
		value.value.should.be.equal( false );

		value.value = "set";
		value.value.should.be.equal( true );
	} );

	it( "ignores case on casting string on adjustment", () => {
		const value = new TypeBoolean( "FALSE" );
		value.value.should.be.equal( false );

		value.value = "tRuE";
		value.value.should.be.equal( true );

		value.value = "OfF";
		value.value.should.be.equal( false );

		value.value = "on";
		value.value.should.be.equal( true );

		value.value = "No";
		value.value.should.be.equal( false );

		value.value = "YeS";
		value.value.should.be.equal( true );

		value.value = "N";
		value.value.should.be.equal( false );

		value.value = "Y";
		value.value.should.be.equal( true );

		value.value = "clEAr";
		value.value.should.be.equal( false );

		value.value = "SEt";
		value.value.should.be.equal( true );
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when type and value are matching", () => {
			new TypeBoolean( false ).equals( new TypeBoolean( false ) ).should.be.true();
			new TypeBoolean( true ).equals( new TypeBoolean( true ) ).should.be.true();
		} );

		it( "is considered different when values aren't matching", () => {
			new TypeBoolean( false ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeBoolean( true ).equals( new TypeBoolean( false ) ).should.be.false();
		} );

		it( "is considered different when types aren't matching", () => {
			new TypeBoolean( false ).equals( new TypeInteger( 0 ) ).should.be.false();
			new TypeBoolean( true ).equals( new TypeInteger( 1 ) ).should.be.false();
		} );

		it( "supports comparing with native values", () => {
			new TypeBoolean( false ).equals( false ).should.be.true();
			new TypeBoolean( false ).equals( true ).should.be.false();
			new TypeBoolean( false ).equals( "false" ).should.be.true();
			new TypeBoolean( false ).equals( "" ).should.be.true();
			new TypeBoolean( false ).equals( "OFF" ).should.be.true();

			new TypeBoolean( true ).equals( true ).should.be.true();
			new TypeBoolean( true ).equals( false ).should.be.false();
			new TypeBoolean( true ).equals( "1" ).should.be.true();
			new TypeBoolean( true ).equals( "true" ).should.be.true();

			new TypeBoolean( true ).equals( "foo" ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeBoolean( false ).toString.should.be.a.Function();

		new TypeBoolean( false ).toString().should.be.String().which.is.equal( "[boolean false]" );
		new TypeBoolean( true ).toString().should.be.String().which.is.equal( "[boolean true]" );
	} );
} );
