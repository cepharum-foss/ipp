/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const { TypeInteger, TypeTextWithoutLanguage, AttributeType: { integer } } = require( "../../../../" );


describe( "An integer-type value", () => {
	it( "can be instantiated", () => {
		new TypeInteger( 0 ).should.have.property( "value" ).which.is.equal( 0 );
		new TypeInteger( -3 ).should.have.property( "value" ).which.is.equal( -3 );
		new TypeInteger( 7654321 ).should.have.property( "value" ).which.is.equal( 7654321 );
		new TypeInteger( "7654321" ).should.have.property( "value" ).which.is.equal( 7654321 );
		new TypeInteger( " 200px " ).should.have.property( "value" ).which.is.equal( 200 );
	} );

	it( "extends abstract Type", () => {
		new TypeInteger( 1 ).should.be.instanceOf( Type );
	} );

	it( "can be instantiated with textWithoutLanguage-value providing integer value as string", () => {
		new TypeInteger( new TypeTextWithoutLanguage( "0" ) ).should.have.property( "value" ).which.is.equal( 0 );
		new TypeInteger( new TypeTextWithoutLanguage( "-3" ) ).should.have.property( "value" ).which.is.equal( -3 );
		new TypeInteger( new TypeTextWithoutLanguage( "7654321" ) ).should.have.property( "value" ).which.is.equal( 7654321 );
	} );

	it( "exposes type identifier", () => {
		new TypeInteger( 0 ).should.have.property( "type" ).which.is.equal( integer );
	} );

	it( "ignores decimal part of provided numbers", () => {
		new TypeInteger( 0.6 ).should.have.property( "value" ).which.is.equal( 0 );
		new TypeInteger( -3.6 ).should.have.property( "value" ).which.is.equal( -3 );
		new TypeInteger( "-3.6" ).should.have.property( "value" ).which.is.equal( -3 );
		new TypeInteger( 7654321.812334 ).should.have.property( "value" ).which.is.equal( 7654321 );
		new TypeInteger( "7654321.812334" ).should.have.property( "value" ).which.is.equal( 7654321 );
	} );

	it( "can be adjusted", () => {
		const value = new TypeInteger( 0 );
		value.value.should.be.equal( 0 );

		value.value = -1;
		value.value.should.be.equal( -1 );

		value.value = " 200px ";
		value.value.should.be.equal( 200 );
	} );

	it( "ignores decimal part on adjustment", () => {
		const value = new TypeInteger( 0 );
		value.value.should.be.equal( 0 );

		value.value = -345.6;
		value.value.should.be.equal( -345 );
	} );

	it( "rejects incompatible data on adjustment", () => {
		const value = new TypeInteger( 0 );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
		( () => { value.value = true; } ).should.throw();
		( () => { value.value = false; } ).should.throw();
		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = "some string"; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
		( () => { value.value = [123]; } ).should.throw();
		( () => { value.value = {}; } ).should.throw();
		( () => { value.value = { value: 123 }; } ).should.throw();
		( () => { value.value = function() {}; } ).should.throw();
	} );

	it( "casts type on adjustment", () => {
		const value = new TypeInteger( "0" );
		value.value.should.be.equal( 0 );

		value.value = "-1";
		value.value.should.be.equal( -1 );

		value.value = "200px";
		value.value.should.be.equal( 200 );

		value.value = "-345.6";
		value.value.should.be.equal( -345 );
	} );

	it( "must not exceed range of 32-bit signed integers", () => {
		( () => new TypeInteger( 0x7fffffff ) ).should.not.throw();
		( () => new TypeInteger( 0x7fffffff + 1 ) ).should.throw();
		( () => new TypeInteger( -0x80000000 ) ).should.not.throw();
		( () => new TypeInteger( -0x80000000 - 1 ) ).should.throw();

		const value = new TypeInteger( 0 );

		( () => { value.value = 0x7fffffff; } ).should.not.throw();
		( () => { value.value = 0x7fffffff + 1; } ).should.throw();
		( () => { value.value = -0x80000000; } ).should.not.throw();
		( () => { value.value = -0x80000000 - 1; } ).should.throw();
	} );

	describe( "can be compared with another value so that it", () => {
		it( "is considered equal when type and value are matching", () => {
			new TypeInteger( 0 ).equals( new TypeInteger( 0 ) ).should.be.true();
			new TypeInteger( -1434234 ).equals( new TypeInteger( -1434234 ) ).should.be.true();
			new TypeInteger( 1434234 ).equals( new TypeInteger( 1434234 ) ).should.be.true();
		} );

		it( "is considered different when value is different, only", () => {
			new TypeInteger( 0 ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeInteger( -1434234 ).equals( new TypeInteger( 1434234 ) ).should.be.false();
			new TypeInteger( 1434234 ).equals( new TypeInteger( -1434234 ) ).should.be.false();
		} );

		it( "is considered different when type is different", () => {
			new TypeInteger( 0 ).equals( new TypeTextWithoutLanguage( "1" ) ).should.be.false();
			new TypeInteger( -1434234 ).equals( new TypeTextWithoutLanguage( "1434234" ) ).should.be.false();
			new TypeInteger( 1434234 ).equals( new TypeTextWithoutLanguage( "-1434234" ) ).should.be.false();
		} );

		it( "is considered equal when providing native value representing same content as current type's value", () => {
			new TypeInteger( 2 ).equals( 2 ).should.be.true();
			new TypeInteger( 2 ).equals( "2" ).should.be.true();
			new TypeInteger( 2 ).equals( " 2 " ).should.be.true();
			new TypeInteger( 2 ).equals( " 2.0 " ).should.be.true();
			new TypeInteger( 2 ).equals( " 2px " ).should.be.true();
			new TypeInteger( 2 ).equals( 3 ).should.be.false();
			new TypeInteger( 2 ).equals( "3" ).should.be.false();
			new TypeInteger( 2 ).equals( "" ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeInteger( 1 ).toString.should.be.a.Function();

		new TypeInteger( 1 ).toString().should.be.String().which.is.equal( "[integer 1]" );
		new TypeInteger( 500 ).toString().should.be.String().which.is.equal( "[integer 500]" );
	} );
} );
