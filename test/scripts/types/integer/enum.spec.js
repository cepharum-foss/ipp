/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const { MinInteger, MaxInteger } = require( "../../../../lib/data" );
const { TypeEnum, TypeTextWithoutLanguage, TypeInteger, TypeKeyword, AttributeType: { enum: typeEnum } } = require( "../../../../" );


describe( "An enumeration-type value", () => {
	it( "can be instantiated with index", () => {
		new TypeEnum( 1 ).should.have.property( "index" ).which.is.equal( 1 );
	} );

	it( "extends abstract Type", () => {
		new TypeEnum( 1 ).should.be.instanceOf( Type );
	} );

	it( "rejects index provided in construction being adjusted afterwards", () => {
		( () => { new TypeEnum( 1 ).value = 2; } ).should.throw();
	} );

	it( "exposes type identifier", () => {
		new TypeEnum( 1 ).should.have.property( "type" ).which.is.equal( typeEnum );
	} );

	it( "can be instantiated with value", () => {
		new TypeEnum( "two", [ "one", "two", "three" ] ).should.have.property( "index" ).which.is.equal( 2 );
		new TypeEnum( "two", [ "one", "two", "three" ] ).should.have.property( "value" ).which.is.equal( "two" );
	} );

	it( "requires provision of enumeration when instantiating with value", () => {
		( () => new TypeEnum( "two" ) ).should.throw();
	} );

	it( "rejects value not listed in provided enumeration", () => {
		( () => new TypeEnum( "two", [ "one", "two", "three" ] ) ).should.not.throw();
		( () => new TypeEnum( "four", [ "one", "two", "three" ] ) ).should.throw();
	} );

	it( "accepts positive indices w/o enumeration provided when instantiating with index instead of value", () => {
		for ( let i = 1; i < 2048; i++ ) {
			( () => new TypeEnum( i ) ).should.not.throw();
		}
	} );

	it( "rejects non-positive indices", () => {
		for ( let i = 0; i > -2048; i-- ) {
			( () => new TypeEnum( i ) ).should.throw();
		}
	} );

	it( "accepts deferred provision of enumeration", () => {
		const deferred = new TypeEnum( 1 );
		deferred.enumeration = ["one"];
	} );

	it( "rejects non-array provided as enumeration", () => {
		( () => { new TypeEnum( 1, {} ); } ).should.throw();
		( () => { new TypeEnum( 1, () => {} ); } ).should.throw();
		( () => { new TypeEnum( 1, "one,two" ); } ).should.throw();
		( () => { new TypeEnum( 1, "one" ); } ).should.throw();
		( () => { new TypeEnum( 1, 1 ); } ).should.throw();

		const deferred = new TypeEnum( 1 );
		( () => { deferred.enumeration = {}; } ).should.throw();
		( () => { deferred.enumeration = () => {}; } ).should.throw();
		( () => { deferred.enumeration = null; } ).should.throw();
		( () => { deferred.enumeration = "one,two"; } ).should.throw();
		( () => { deferred.enumeration = "one"; } ).should.throw();
		( () => { deferred.enumeration = 1; } ).should.throw();
	} );

	it( "rejects empty array provided as enumeration", () => {
		( () => { new TypeEnum( 1, [] ); } ).should.throw();

		const deferred = new TypeEnum( 1 );
		( () => { deferred.enumeration = []; } ).should.throw();
	} );

	// this case is untestable now due to requiring non-sparse array of 2^32-1 keywords
	it.skip( "rejects too huge array provided as enumeration", () => {
		( () => { new TypeEnum( 1, new Array( MaxInteger ).fill( "a" ) ); } ).should.not.throw();
		( () => { new TypeEnum( 1, new Array( MaxInteger + 1 ).fill( "a" ) ); } ).should.throw();

		const deferred = new TypeEnum( 1 );
		( () => { deferred.enumeration = new Array( MaxInteger ).fill( "a" ); } ).should.not.throw();
		( () => { deferred.enumeration = new Array( MaxInteger + 1 ).fill( "a" ); } ).should.throw();
	} );

	it( "rejects multiple provision of enumeration", () => {
		const instant = new TypeEnum( 1, ["one"] );
		( () => { instant.enumeration = ["one"]; } ).should.throw();

		const deferred = new TypeEnum( 1 );
		( () => { deferred.enumeration = ["one"]; } ).should.not.throw();
		( () => { deferred.enumeration = ["one"]; } ).should.throw();
	} );

	it( "throws when deferred assignment of enumeration isn't covering index set on constructing", () => {
		const bad = new TypeEnum( 2 );
		( () => { bad.enumeration = ["one"]; } ).should.throw();

		const good = new TypeEnum( 2 );
		( () => { good.enumeration = [ "one", "two" ]; } ).should.not.throw();
	} );

	it( "rejects to fetch value prior to assigning enumeration deferredly", () => {
		const value = new TypeEnum( 1 );

		( () => value.value ).should.throw();

		value.enumeration = ["one"];

		( () => value.value ).should.not.throw();
	} );

	it( "exposes enumeration", () => {
		const value = new TypeEnum( 1, ["one"] );
		value.enumeration.should.be.Array().which.is.deepEqual( ["one"] );
	} );

	it( "exposes null if enumeration hasn't been provided yet", () => {
		const value = new TypeEnum( 1 );
		( value.enumeration == null ).should.be.true();
	} );

	describe( "can be compared with value which", () => {
		it( "is considered equal when exposing same value", () => {
			const a = new TypeEnum( 1, ["one"] );
			const b = new TypeEnum( 1, ["one"] );

			a.equals( b ).should.be.true();
		} );

		it( "is considered equal when exposing same value of different enumerations", () => {
			const a = new TypeEnum( 1, ["one"] );
			const b = new TypeEnum( 2, [ "two", "one" ] );

			a.equals( b ).should.be.true();
		} );

		it( "is not considered equal when indices of exposed values in either enumeration are equal, only", () => {
			const a = new TypeEnum( 1, ["one"] );
			const b = new TypeEnum( 1, ["two"] );

			a.equals( b ).should.be.false();
		} );

		it( "is not considered equal when provided value is not an enumeration though exposing same value", () => {
			const a = new TypeEnum( 1, ["one"] );
			const b = new TypeTextWithoutLanguage( "one" );

			a.equals( b ).should.be.false();
		} );

		it( "is not considered equal when provided value is not an enumeration though exposing same index", () => {
			const a = new TypeEnum( 1, ["one"] );
			const b = new TypeInteger( 1 );

			a.equals( b ).should.be.false();
		} );

		it( "is considered equal when providing native value representing same content as current type's value", () => {
			new TypeEnum( "two", [ "one", "two", "three" ] ).equals( "two" ).should.be.true();
			new TypeEnum( "two", [ "one", "two", "three" ] ).equals( "one" ).should.be.false();
			new TypeEnum( "two", [ "one", "two", "three" ] ).equals( 2 ).should.be.false();
			new TypeEnum( "two", [ "one", "two", "three" ] ).equals( "TWO" ).should.be.false();
		} );
	} );

	describe( "includes static method for statically checking enumerations which", () => {
		it( "fails on providing anything but array", () => {
			( () => { TypeEnum.checkEnumeration( null ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( {} ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( () => {} ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( "one,two" ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( "one" ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( 1 ); } ).should.throw();
		} );

		it( "fails on providing empty array", () => {
			( () => { TypeEnum.checkEnumeration( [] ); } ).should.throw();
		} );

		it( "fails on providing too huge array", () => {
			( () => { TypeEnum.checkEnumeration( new Array( MaxInteger ) ); } ).should.not.throw();
			( () => { TypeEnum.checkEnumeration( new Array( MaxInteger + 1 ) ); } ).should.throw();
		} );

		it( "ignores listed values of provided array by default", () => {
			( () => TypeEnum.checkEnumeration( [
				null, undefined, false, true, {}, [],
				{ value: "one" }, ["one"], () => "one", "", " ", "1"
			] ) ).should.not.throw();
		} );

		it( "fails on invalid values in provided array when checking thoroughly", () => {
			( () => { TypeEnum.checkEnumeration( [null], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [undefined], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [false], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [true], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [{}], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [[]], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [{ value: "one" }], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [["one"]], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [() => "one"], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [""], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( [" "], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( ["1"], true ); } ).should.throw();
			( () => { TypeEnum.checkEnumeration( ["UPPERCASE-NOT-ALLOWED-KEYWORDS"], true ); } ).should.throw();
		} );

		it( "accepts strings conforming to IPP keyword syntax rules when checking enumeration items thoroughly", () => {
			( () => { TypeEnum.checkEnumeration( ["a"], true ); } ).should.not.throw();
			( () => { TypeEnum.checkEnumeration( ["lowercase-is-fine_keyw0rds"], true ); } ).should.not.throw();
		} );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ) ).value.should.be.equal( "one" );
		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ) ).index.should.be.equal( 1 );
		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ) ).enumeration.should.be.deepEqual( [ "one", "two" ] );

		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ) ).value.should.be.equal( "one" );
		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ) ).index.should.be.equal( 1 );
		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ) ).enumeration.should.be.deepEqual( [ "one", "two" ] );

		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ), [ "two", "one" ] ).value.should.be.equal( "one" );
		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ), [ "two", "one" ] ).index.should.be.equal( 2 );
		new TypeEnum( new TypeEnum( "one", [ "one", "two" ] ), [ "two", "one" ] ).enumeration.should.be.deepEqual( [ "two", "one" ] );

		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ), [ "two", "one" ] ).value.should.be.equal( "one" );
		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ), [ "two", "one" ] ).index.should.be.equal( 2 );
		new TypeEnum( new TypeEnum( 1, [ "one", "two" ] ), [ "two", "one" ] ).enumeration.should.be.deepEqual( [ "two", "one" ] );
	} );

	it( "rejects to be instantiated with enumeration-type value lacking enumeration itself", () => {
		( () => new TypeEnum( new TypeEnum( 1 ) ) ).should.throw();
		( () => new TypeEnum( new TypeEnum( 1 ), ["two"] ) ).should.throw();
	} );

	it( "rejects to be instantiated with integer-type value", () => {
		( () => new TypeEnum( new TypeInteger( 1 ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeInteger( 1 ) ) ).should.throw();

		( () => new TypeEnum( new TypeInteger( 0 ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeInteger( -1 ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeInteger( MinInteger ), [ "two", "one" ] ) ).should.throw();
	} );

	it( "can be instantiated with keyword-type value", () => {
		new TypeEnum( new TypeKeyword( "one" ), [ "two", "one" ] ).value.should.be.equal( "one" );
		new TypeEnum( new TypeKeyword( "one" ), [ "two", "one" ] ).index.should.be.equal( 2 );
		new TypeEnum( new TypeKeyword( "one" ), [ "two", "one" ] ).enumeration.should.be.deepEqual( [ "two", "one" ] );
	} );

	it( "can be instantiated with textWithoutLanguage-type value complying with keyword syntax rules", () => {
		new TypeEnum( new TypeTextWithoutLanguage( "one" ), [ "two", "one" ] ).value.should.be.equal( "one" );
		new TypeEnum( new TypeTextWithoutLanguage( "one" ), [ "two", "one" ] ).index.should.be.equal( 2 );
		new TypeEnum( new TypeTextWithoutLanguage( "one" ), [ "two", "one" ] ).enumeration.should.be.deepEqual( [ "two", "one" ] );
	} );

	it( "rejects to be instantiated with textWithoutLanguage-type value not complying with keyword syntax rules", () => {
		( () => new TypeEnum( new TypeTextWithoutLanguage( "" ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeTextWithoutLanguage( " " ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeTextWithoutLanguage( "1" ), [ "two", "one" ] ) ).should.throw();
		( () => new TypeEnum( new TypeTextWithoutLanguage( "_" ), [ "two", "one" ] ) ).should.throw();
	} );

	it( "renders value as string", () => {
		new TypeEnum( 1 ).toString.should.be.a.Function();

		new TypeEnum( 1 ).toString().should.be.String().which.is.equal( "[enum #1]" );
		new TypeEnum( 6 ).toString().should.be.String().which.is.equal( "[enum #6]" );
		new TypeEnum( 1, ["one"] ).toString().should.be.String().which.is.equal( "[enum one]" );
		new TypeEnum( 2, [ "one", "zwei" ] ).toString().should.be.String().which.is.equal( "[enum zwei]" );
	} );
} );
