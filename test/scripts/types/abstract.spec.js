/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../lib/types/abstract" );
const { AttributeType } = require( "../../../" );
const { AttributeTypeLimit } = require( "../../../lib/data" );


describe( "Abstract type implementation `Type`", () => {
	it( "can be instantiated", () => {
		new Type();
	} );

	it( "exposes type identifier provided on construction", () => {
		new Type().should.have.property( "type" ).which.is.undefined();

		Object.keys( AttributeType ).forEach( key => {
			new Type( AttributeType[key] ).should.have.property( "type" ).which.is.equal( AttributeType[key] );
		} );
	} );

	it( "exposes limit on number of encoded octets", () => {
		new Type().should.have.property( "octetLimit" ).which.is.Infinity();

		Object.keys( AttributeType ).forEach( key => {
			const type = AttributeType[key];
			const limit = AttributeTypeLimit[type] || Infinity;

			new Type( type ).should.have.property( "octetLimit" ).which.is.equal( limit );
		} );
	} );

	it( "accepts limit to be reduced", () => {
		const value = new Type();

		( () => { value.octetLimit = 10; } ).should.not.throw();
		( () => { value.octetLimit = 10; } ).should.not.throw();
		( () => { value.octetLimit = 9; } ).should.not.throw();
		( () => { value.octetLimit = 8; } ).should.not.throw();
	} );

	it( "exposes method for adjusting limit", () => {
		const value = new Type();

		value.octetLimit.should.be.Infinity();
		value.limit( 50 );
		value.octetLimit.should.be.equal( 50 );
		value.limit( 50 ).should.be.equal( value );
		value.limit( 50 ).limit( 49 ).limit( 48 );
		value.octetLimit.should.be.equal( 48 );
	} );

	it( "rejects limit to be 0", () => {
		const value = new Type();

		( () => { value.octetLimit = 0; } ).should.throw();
		( () => value.limit( 0 ) ).should.throw();

		( () => { value.octetLimit = 10; } ).should.not.throw();
		( () => value.limit( 10 ) ).should.not.throw();
		( () => { value.octetLimit = 0; } ).should.throw();
		( () => value.limit( 0 ) ).should.throw();

		( () => { value.octetLimit = 9; } ).should.not.throw();
		( () => value.limit( 9 ) ).should.not.throw();
		( () => { value.octetLimit = 0; } ).should.throw();
		( () => value.limit( 0 ) ).should.throw();
	} );

	it( "rejects limit to be increased", () => {
		const value = new Type();
		value.octetLimit = 50;

		for ( let i = 51; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
			( () => value.limit( i ) ).should.throw();
		}
	} );

	it( "exposes abstract method for encoding value as buffer", () => {
		Object.keys( AttributeType ).forEach( key => {
			new Type( AttributeType[key] ).should.have.property( "toBuffer" ).which.is.a.Function();
		} );
	} );

	it( "rejects invocation of abstract method for encoding value as buffer", () => {
		Object.keys( AttributeType ).forEach( key => {
			( () => new Type( AttributeType[key] ).toBuffer() ).should.throw();
		} );
	} );

	it( "rejects invocation of abstract method for rendering value as string", () => {
		Object.keys( AttributeType ).forEach( key => {
			( () => new Type( AttributeType[key] ).toString() ).should.throw();
		} );
	} );

	it( "exposes basic method for comparing types checking type identifiers, only", () => {
		Object.keys( AttributeType ).forEach( key => {
			new Type( AttributeType[key] ).should.have.property( "equals" ).which.is.a.Function();
			new Type( AttributeType[key] ).equals( null ).should.be.false();
			new Type( AttributeType[key] ).equals( undefined ).should.be.false();
			new Type( AttributeType[key] ).equals( false ).should.be.false();
			new Type( AttributeType[key] ).equals( true ).should.be.false();
			new Type( AttributeType[key] ).equals( "" ).should.be.false();
			new Type( AttributeType[key] ).equals( " " ).should.be.false();
			new Type( AttributeType[key] ).equals( "." ).should.be.false();
			new Type( AttributeType[key] ).equals( "0" ).should.be.false();
			new Type( AttributeType[key] ).equals( 0 ).should.be.false();
			new Type( AttributeType[key] ).equals( 1.5 ).should.be.false();
			new Type( AttributeType[key] ).equals( [] ).should.be.false();
			new Type( AttributeType[key] ).equals( [1] ).should.be.false();
			new Type( AttributeType[key] ).equals( [""] ).should.be.false();
			new Type( AttributeType[key] ).equals( {} ).should.be.false();
			new Type( AttributeType[key] ).equals( { toString: () => "a" } ).should.be.false();
			new Type( AttributeType[key] ).equals( { value: "a" } ).should.be.false();

			new Type( AttributeType[key] ).equals( new Type( AttributeType[key] ) ).should.be.true();
			new Type( AttributeType[key] ).equals( new Type( 0 ) ).should.be.false();
		} );
	} );

	it( "ignores context additionally provided on comparing types", () => {
		Object.keys( AttributeType ).forEach( key => {
			new Type( AttributeType[key] ).equals( null, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( undefined, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( false, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( true, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( "", { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( " ", { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( ".", { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( "0", { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( 0, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( 1.5, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( [], { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( [1], { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( [""], { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( {}, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( { toString: () => "a" }, { valid: false } ).should.be.false();
			new Type( AttributeType[key] ).equals( { value: "a" }, { valid: false } ).should.be.false();

			new Type( AttributeType[key] ).equals( new Type( AttributeType[key] ), { valid: false } ).should.be.true();
			new Type( AttributeType[key] ).equals( new Type( 0 ), { valid: false } ).should.be.false();
		} );
	} );
} );
