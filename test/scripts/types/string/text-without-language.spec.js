/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const {
	TypeTextWithoutLanguage, TypeTextWithLanguage, TypeNameWithLanguage,
	TypeBoolean, TypeInteger, TypeEnum, AttributeType
} = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A textWithoutLanguage-type value", () => {
	it( "can be instantiated", () => {
		new TypeTextWithoutLanguage( "" ).should.have.property( "value" ).which.is.equal( "" );
		new TypeTextWithoutLanguage( "hello" ).should.have.property( "value" ).which.is.equal( "hello" );
		new TypeTextWithoutLanguage( "FooBar" ).should.have.property( "value" ).which.is.equal( "FooBar" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeTextWithoutLanguage( "" ).should.have.property( "type" ).which.is.equal( AttributeType.textWithoutLanguage );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeTextWithoutLanguage( "" ).should.be.instanceOf( TypeString );
	} );

	it( "can be instantiated with value cast to string", () => {
		new TypeTextWithoutLanguage( 1 ).should.have.property( "value" ).which.is.equal( "1" );
		new TypeTextWithoutLanguage( -256 ).should.have.property( "value" ).which.is.equal( "-256" );
		new TypeTextWithoutLanguage( true ).should.have.property( "value" ).which.is.equal( "true" );
		new TypeTextWithoutLanguage( false ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeTextWithoutLanguage( {} ).should.have.property( "value" ).which.is.equal( "[object Object]" );
		new TypeTextWithoutLanguage( { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
		new TypeTextWithoutLanguage( [] ).should.have.property( "value" ).which.is.equal( "" );
		new TypeTextWithoutLanguage( [ "foo", "bar" ] ).should.have.property( "value" ).which.is.equal( "foo,bar" );
	} );

	it( "can be instantiated with any string-type value", () => {
		Types.forEach( from => {
			new TypeTextWithoutLanguage( new TypeString( from, "" ) ).should.have.property( "value" ).which.is.equal( "" );
		} );
	} );

	it( "can be instantiated with integer-type value", () => {
		new TypeTextWithoutLanguage( new TypeInteger( 0 ) ).should.have.property( "value" ).which.is.equal( "0" );
		new TypeTextWithoutLanguage( new TypeInteger( -1234 ) ).should.have.property( "value" ).which.is.equal( "-1234" );
		new TypeTextWithoutLanguage( new TypeInteger( 5678 ) ).should.have.property( "value" ).which.is.equal( "5678" );
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeTextWithoutLanguage( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeTextWithoutLanguage( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeTextWithoutLanguage( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
	} );

	it( "obeys defined limit of octets required for encoding some value", () => {
		new TypeTextWithoutLanguage( "hello", 10 ).value.should.be.equal( "hello" );
		new TypeTextWithoutLanguage( "extraordinary", 10 ).value.should.be.equal( "extraordin" );
		new TypeTextWithoutLanguage( "überhöht", 9 ).value.should.be.equal( "überhöh" );
		new TypeTextWithoutLanguage( "überhöht", 8 ).value.should.be.equal( "überhö" );
		new TypeTextWithoutLanguage( "überhöht", 7 ).value.should.be.equal( "überh" );
		new TypeTextWithoutLanguage( "überhöht", 6 ).value.should.be.equal( "überh" );
		new TypeTextWithoutLanguage( "überhöht", 5 ).value.should.be.equal( "über" );

		new TypeTextWithoutLanguage( "hello", 1 ).value.should.be.equal( "h" );
		new TypeTextWithoutLanguage( "überhöht", 1 ).value.should.be.equal( "" );
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeTextWithoutLanguage( "hello", 0 ) ).should.throw();
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeTextWithoutLanguage( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "ignores limits on number of encoded octets exceeding some internally defined one", () => {
		new TypeTextWithoutLanguage( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.textWithoutLanguage] );
	} );

	it( "accepts limit being reduced", () => {
		const value = new TypeTextWithoutLanguage( "überhöht", 9 );
		value.octetLimit.should.be.equal( 9 );
		value.value.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 9; } ).should.not.throw();
		value.value.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.value.should.be.equal( "überhö" );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.value.should.be.equal( "überh" );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.value.should.be.equal( "überh" );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeTextWithoutLanguage( "überhöht", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted", () => {
		const value = new TypeTextWithoutLanguage( "" );

		value.value = 1;
		value.value.should.be.equal( "1" );
		value.value = -256;
		value.value.should.be.equal( "-256" );
		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = {};
		value.value.should.be.equal( "[object Object]" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );
		value.value = [];
		value.value.should.be.equal( "" );
		value.value = [ "foo", "bar" ];
		value.value.should.be.equal( "foo,bar" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.equal( "foo" );
		} );

		value.value = new TypeInteger( 12345 );
		value.value.should.be.equal( "12345" );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeTextWithoutLanguage( "" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-sensitively", () => {
			const value = new TypeTextWithoutLanguage( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.equal( from === textWithoutLanguage );
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.false();
			} );
		} );

		it( "is considered different when type of provided value isn't matching", () => {
			new TypeTextWithoutLanguage( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeTextWithoutLanguage( "1" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeTextWithoutLanguage( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeTextWithoutLanguage( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeTextWithoutLanguage( "1" ).equals( new TypeInteger( 1 ).value ).should.be.true();
			new TypeTextWithoutLanguage( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );

		it( "is considered equal when comparing with textWithLanguage with its language matching provided default language", () => {
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.true();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "FOO", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeTextWithoutLanguage( "FOO" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "en" } ).should.be.false();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ) ).should.be.false();
		} );

		it( "is considered different when comparing with nameWithLanguage", () => {
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "FOO", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeTextWithoutLanguage( "FOO" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "en" } ).should.be.false();
			new TypeTextWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ) ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeTextWithoutLanguage( "" ).toString.should.be.a.Function();

		new TypeTextWithoutLanguage( "" ).toString().should.be.String().which.is.equal( "[textWithoutLanguage(0) ]" );
		new TypeTextWithoutLanguage( "foo" ).toString().should.be.String().which.is.equal( "[textWithoutLanguage(3) foo]" );
		new TypeTextWithoutLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[textWithoutLanguage(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeTextWithoutLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[textWithoutLanguage(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
