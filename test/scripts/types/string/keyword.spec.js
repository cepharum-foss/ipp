/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { setGlobalOption, TypeKeyword, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A keyword-type value", () => {
	it( "can be instantiated", () => {
		new TypeKeyword( "a" ).should.have.property( "value" ).which.is.equal( "a" );
		new TypeKeyword( "hello" ).should.have.property( "value" ).which.is.equal( "hello" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeKeyword( "a" ).should.have.property( "type" ).which.is.equal( AttributeType.keyword );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeKeyword( "a" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeKeyword( "" ) ).should.throw();
	} );

	it( "rejects values containing uppercase letters", () => {
		( () => new TypeKeyword( "FooBar" ) ).should.throw();
		( () => new TypeKeyword( "foobar" ) ).should.not.throw();
	} );

	it( "accepts values containing uppercase letters when relaxing rules", () => {
		setGlobalOption( "relaxKeywordSyntax", true );

		( () => new TypeKeyword( "FooBar" ) ).should.not.throw();
		( () => new TypeKeyword( "foobar" ) ).should.not.throw();

		setGlobalOption( "relaxKeywordSyntax", false );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeKeyword( "überhöht" ) ).should.throw();
		( () => new TypeKeyword( "ü" ) ).should.throw();
		( () => new TypeKeyword( "fóôbÂr" ) ).should.throw();
	} );

	it( "rejects values containing inner whitespace", () => {
		( () => new TypeKeyword( "foo bar" ) ).should.throw();
		( () => new TypeKeyword( "foobar" ) ).should.not.throw();
	} );

	it( "accepts values containing leading/trailing whitespace due to trimming", () => {
		( () => new TypeKeyword( " foobar " ) ).should.not.throw();
	} );

	it( "rejects values not starting with latin lowercase letter", () => {
		( () => new TypeKeyword( "1" ) ).should.throw();
		( () => new TypeKeyword( "1a" ) ).should.throw();
		( () => new TypeKeyword( "-" ) ).should.throw();
		( () => new TypeKeyword( "-a" ) ).should.throw();
		( () => new TypeKeyword( "_" ) ).should.throw();
		( () => new TypeKeyword( "_a" ) ).should.throw();
		( () => new TypeKeyword( "." ) ).should.throw();
		( () => new TypeKeyword( ".a" ) ).should.throw();

		( () => new TypeKeyword( "a1" ) ).should.not.throw();
		( () => new TypeKeyword( "a1a" ) ).should.not.throw();
		( () => new TypeKeyword( "a-" ) ).should.not.throw();
		( () => new TypeKeyword( "a-a" ) ).should.not.throw();
		( () => new TypeKeyword( "a_" ) ).should.not.throw();
		( () => new TypeKeyword( "a_a" ) ).should.not.throw();
		( () => new TypeKeyword( "a." ) ).should.not.throw();
		( () => new TypeKeyword( "a.a" ) ).should.not.throw();
	} );

	it( "accepts values starting with digit when relaxing rules", () => {
		setGlobalOption( "relaxKeywordSyntax", true );

		( () => new TypeKeyword( "1" ) ).should.not.throw();
		( () => new TypeKeyword( "1a" ) ).should.not.throw();
		( () => new TypeKeyword( "-" ) ).should.throw();
		( () => new TypeKeyword( "-a" ) ).should.throw();
		( () => new TypeKeyword( "_" ) ).should.throw();
		( () => new TypeKeyword( "_a" ) ).should.throw();
		( () => new TypeKeyword( "." ) ).should.throw();
		( () => new TypeKeyword( ".a" ) ).should.throw();

		( () => new TypeKeyword( "a1" ) ).should.not.throw();
		( () => new TypeKeyword( "a1a" ) ).should.not.throw();
		( () => new TypeKeyword( "a-" ) ).should.not.throw();
		( () => new TypeKeyword( "a-a" ) ).should.not.throw();
		( () => new TypeKeyword( "a_" ) ).should.not.throw();
		( () => new TypeKeyword( "a_a" ) ).should.not.throw();
		( () => new TypeKeyword( "a." ) ).should.not.throw();
		( () => new TypeKeyword( "a.a" ) ).should.not.throw();

		setGlobalOption( "relaxKeywordSyntax", false );
	} );

	it( "can be instantiated with value cast to string", () => {
		new TypeKeyword( true ).should.have.property( "value" ).which.is.equal( "true" );
		new TypeKeyword( false ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeKeyword( { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
	} );

	it( "can be instantiated with any string-type value complying with syntax rules", () => {
		Types.forEach( from => {
			new TypeKeyword( new TypeString( from, "a" ) ).should.have.property( "value" ).which.is.equal( "a" );
		} );
	} );

	it( "rejects to be instantiated with any string-type value not complying with syntax rules", () => {
		Types.forEach( from => {
			( () => new TypeKeyword( new TypeString( from, "" ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "-" ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "A" ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "." ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "1" ) ) ).should.throw();
		} );
	} );

	it( "accepts to be instantiated with some string-type values not complying with syntax rules when relaxing rules", () => {
		setGlobalOption( "relaxKeywordSyntax", true );

		Types.forEach( from => {
			( () => new TypeKeyword( new TypeString( from, "" ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "-" ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "A" ) ) ).should.not.throw();
			( () => new TypeKeyword( new TypeString( from, "." ) ) ).should.throw();
			( () => new TypeKeyword( new TypeString( from, "1" ) ) ).should.not.throw();
		} );

		setGlobalOption( "relaxKeywordSyntax", false );
	} );

	it( "cannot be instantiated with integer-type value due to failing keyword syntax rules", () => {
		( () => new TypeKeyword( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeKeyword( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeKeyword( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeKeyword( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeKeyword( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeKeyword( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeKeyword( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeKeyword( "extraordinary", 10 ) ).should.throw();
		( () => new TypeKeyword( "extraordinary", 11 ) ).should.throw();
		( () => new TypeKeyword( "extraordinary", 12 ) ).should.throw();
		( () => new TypeKeyword( "extraordinary", 13 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeKeyword( "hello", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeKeyword( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.keyword] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeKeyword( "foobar", 8 );
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.octetLimit.should.be.equal( 7 );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.octetLimit.should.be.equal( 6 );

		( () => { value.octetLimit = 5; } ).should.throw();
		value.octetLimit.should.be.equal( 6 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeKeyword( "foobar", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted", () => {
		const value = new TypeKeyword( "a" );

		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );
		value.value = ["bar"];
		value.value.should.be.equal( "bar" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.equal( "foo" );
		} );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeKeyword( "a" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeKeyword( "a" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeKeyword( "foobar" );

		( () => { value.value = "überhöht"; } ).should.throw();
		( () => { value.value = "ü"; } ).should.throw();
		( () => { value.value = "Vâlúê"; } ).should.throw();
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeKeyword( "foobar", 7 );

		( () => { value.value = "barfoo"; } ).should.not.throw();
		( () => { value.value = "foobarb"; } ).should.not.throw();
		( () => { value.value = "foobarba"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-sensitively", () => {
			const value = new TypeKeyword( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.equal( from === keyword );
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.false();
				value.equals( new TypeString( from, "FOO" ).value ).should.be.false();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeKeyword( "true" ).equals( "true" ).should.be.true();
			new TypeKeyword( "bar" ).equals( "bar" ).should.be.true();
			new TypeKeyword( "bar" ).equals( ["bar"] ).should.be.true();
		} );

		it( "is considered different when type of provided value isn't string-type", () => {
			new TypeKeyword( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeKeyword( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeKeyword( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeKeyword( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );
	} );

	it( "renders value as string", () => {
		new TypeKeyword( "f" ).toString.should.be.a.Function();

		new TypeKeyword( "f" ).toString().should.be.String().which.is.equal( "[keyword(1) f]" );
		new TypeKeyword( "foo" ).toString().should.be.String().which.is.equal( "[keyword(3) foo]" );
		new TypeKeyword( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[keyword(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeKeyword( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[keyword(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
