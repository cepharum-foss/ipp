/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { TypeUriScheme, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A uriScheme-type value", () => {
	it( "can be instantiated", () => {
		new TypeUriScheme( "a" ).should.have.property( "value" ).which.is.equal( "a" );
		new TypeUriScheme( "hello" ).should.have.property( "value" ).which.is.equal( "hello" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeUriScheme( "a" ).should.have.property( "type" ).which.is.equal( AttributeType.uriScheme );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeUriScheme( "a" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeUriScheme( "" ) ).should.throw();
	} );

	it( "rejects to be instantiated with string not complying with syntax rules", () => {
		( () => new TypeUriScheme( "." ) ).should.throw();
		( () => new TypeUriScheme( "1" ) ).should.throw();
		( () => new TypeUriScheme( "-1" ) ).should.throw();
		( () => new TypeUriScheme( "-" ) ).should.throw();
		( () => new TypeUriScheme( "_" ) ).should.throw();
		( () => new TypeUriScheme( "foo bar" ) ).should.throw();
	} );

	it( "always converts provided strings to lower-case", () => {
		new TypeUriScheme( "FooBar" ).should.have.property( "value" ).which.is.equal( "foobar" );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeUriScheme( "überhöht" ) ).should.throw();
		( () => new TypeUriScheme( "ü" ) ).should.throw();
		( () => new TypeUriScheme( "fóôbÂr" ) ).should.throw();
	} );

	it( "can be instantiated with value cast to string complying with syntax rules", () => {
		new TypeUriScheme( true ).should.have.property( "value" ).which.is.equal( "true" );
		new TypeUriScheme( false ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeUriScheme( { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
	} );

	it( "rejects to be instantiated with value cast to string not complying with syntax rules", () => {
		( () => new TypeUriScheme( 1 ) ).should.throw();
		( () => new TypeUriScheme( -256 ) ).should.throw();
		( () => new TypeUriScheme( {} ) ).should.throw();
		( () => new TypeUriScheme( [ "foo", "bar" ] ) ).should.throw();

		( () => new TypeUriScheme( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value complying with syntax rules", () => {
		Types.forEach( from => {
			new TypeUriScheme( new TypeString( from, "a" ) ).should.have.property( "value" ).which.is.equal( "a" );
			new TypeUriScheme( new TypeString( from, "B" ) ).should.have.property( "value" ).which.is.equal( "b" );
		} );
	} );

	it( "rejects to be instantiated with any string-type value not complying with syntax rules", () => {
		Types.forEach( from => {
			( () => new TypeUriScheme( new TypeString( from, "" ) ) ).should.throw();
			( () => new TypeUriScheme( new TypeString( from, "-" ) ) ).should.throw();
			( () => new TypeUriScheme( new TypeString( from, "1" ) ) ).should.throw();
		} );
	} );

	it( "rejects to be instantiated with integer-type value for not complying with syntax rules", () => {
		( () => new TypeUriScheme( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeUriScheme( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeUriScheme( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeUriScheme( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeUriScheme( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeUriScheme( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeUriScheme( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeUriScheme( "extraordinary", 10 ) ).should.throw();
		( () => new TypeUriScheme( "extraordinary", 11 ) ).should.throw();
		( () => new TypeUriScheme( "extraordinary", 12 ) ).should.throw();
		( () => new TypeUriScheme( "extraordinary", 13 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeUriScheme( "hello", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeUriScheme( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.uriScheme] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeUriScheme( "foobar", 8 );
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.octetLimit.should.be.equal( 7 );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.octetLimit.should.be.equal( 6 );

		( () => { value.octetLimit = 5; } ).should.throw();
		value.octetLimit.should.be.equal( 6 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeUriScheme( "foobar", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted using value complying with syntax rules when cast to string", () => {
		const value = new TypeUriScheme( "a" );

		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.equal( "foo" );
			value.value = new TypeString( from, "FOO" );
			value.value.should.be.equal( "foo" );
		} );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects to be adjusted using value not complying with syntax rules when cast to string", () => {
		const value = new TypeUriScheme( "a" );

		( () => { value.value = 1; } ).should.throw();
		value.value.should.be.equal( "a" );
		( () => { value.value = -256; } ).should.throw();
		value.value.should.be.equal( "a" );
		( () => { value.value = {}; } ).should.throw();
		value.value.should.be.equal( "a" );
		( () => { value.value = [ "foo", "bar" ]; } ).should.throw();
		value.value.should.be.equal( "a" );

		Types.forEach( from => {
			( () => { value.value = new TypeString( from, "-" ); } ).should.throw();
			value.value.should.be.equal( "a" );
		} );

		( () => { value.value = new TypeInteger( 12345 ); } ).should.throw();
		value.value.should.be.equal( "a" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeUriScheme( "a" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeUriScheme( "a" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeUriScheme( "foobar" );

		( () => { value.value = "überhöht"; } ).should.throw();
		( () => { value.value = "ü"; } ).should.throw();
		( () => { value.value = "Vâlúê"; } ).should.throw();
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeUriScheme( "foobar", 7 );

		( () => { value.value = "barfoo"; } ).should.not.throw();
		( () => { value.value = "foobarb"; } ).should.not.throw();
		( () => { value.value = "foobarba"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeUriScheme( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.equal( from === uriScheme );
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.equal( from === uriScheme );
				value.equals( new TypeString( from, "FOO" ).value ).should.be.true();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeUriScheme( "true" ).equals( "true" ).should.be.true();
			new TypeUriScheme( "bar" ).equals( "bar" ).should.be.true();
			new TypeUriScheme( "bar" ).equals( ["bar"] ).should.be.true();
		} );

		it( "is considered different when type of provided value isn't string-type", () => {
			new TypeUriScheme( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeUriScheme( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeUriScheme( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeUriScheme( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );
	} );

	it( "renders value as string", () => {
		new TypeUriScheme( "f" ).toString.should.be.a.Function();

		new TypeUriScheme( "f" ).toString().should.be.String().which.is.equal( "[uriScheme(1) f]" );
		new TypeUriScheme( "foo" ).toString().should.be.String().which.is.equal( "[uriScheme(3) foo]" );
		new TypeUriScheme( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[uriScheme(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeUriScheme( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[uriScheme(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
