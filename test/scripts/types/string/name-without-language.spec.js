/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const {
	TypeNameWithoutLanguage, TypeNameWithLanguage, TypeTextWithLanguage,
	TypeBoolean, TypeInteger, TypeEnum, AttributeType
} = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A nameWithoutLanguage-type value", () => {
	it( "can be instantiated", () => {
		new TypeNameWithoutLanguage( "a" ).should.have.property( "value" ).which.is.equal( "a" );
		new TypeNameWithoutLanguage( "hello" ).should.have.property( "value" ).which.is.equal( "hello" );
		new TypeNameWithoutLanguage( "FooBar" ).should.have.property( "value" ).which.is.equal( "FooBar" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeNameWithoutLanguage( "a" ).should.have.property( "type" ).which.is.equal( AttributeType.nameWithoutLanguage );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeNameWithoutLanguage( "a" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeNameWithoutLanguage( "" ) ).should.throw();
	} );

	it( "can be instantiated with value cast to string", () => {
		new TypeNameWithoutLanguage( 1 ).should.have.property( "value" ).which.is.equal( "1" );
		new TypeNameWithoutLanguage( -256 ).should.have.property( "value" ).which.is.equal( "-256" );
		new TypeNameWithoutLanguage( true ).should.have.property( "value" ).which.is.equal( "true" );
		new TypeNameWithoutLanguage( false ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeNameWithoutLanguage( {} ).should.have.property( "value" ).which.is.equal( "[object Object]" );
		new TypeNameWithoutLanguage( { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
		new TypeNameWithoutLanguage( [ "foo", "bar" ] ).should.have.property( "value" ).which.is.equal( "foo,bar" );

		( () => new TypeNameWithoutLanguage( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value", () => {
		Types.forEach( from => {
			new TypeNameWithoutLanguage( new TypeString( from, "a" ) ).should.have.property( "value" ).which.is.equal( "a" );
		} );
	} );

	it( "can be instantiated with integer-type value", () => {
		new TypeNameWithoutLanguage( new TypeInteger( 0 ) ).should.have.property( "value" ).which.is.equal( "0" );
		new TypeNameWithoutLanguage( new TypeInteger( -1234 ) ).should.have.property( "value" ).which.is.equal( "-1234" );
		new TypeNameWithoutLanguage( new TypeInteger( 5678 ) ).should.have.property( "value" ).which.is.equal( "5678" );
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeNameWithoutLanguage( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeNameWithoutLanguage( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeNameWithoutLanguage( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
	} );

	it( "obeys defined limit of octets required for encoding some value", () => {
		new TypeNameWithoutLanguage( "hello", 10 ).value.should.be.equal( "hello" );
		new TypeNameWithoutLanguage( "extraordinary", 10 ).value.should.be.equal( "extraordin" );
		new TypeNameWithoutLanguage( "überhöht", 9 ).value.should.be.equal( "überhöh" );
		new TypeNameWithoutLanguage( "überhöht", 8 ).value.should.be.equal( "überhö" );
		new TypeNameWithoutLanguage( "überhöht", 7 ).value.should.be.equal( "überh" );
		new TypeNameWithoutLanguage( "überhöht", 6 ).value.should.be.equal( "überh" );
		new TypeNameWithoutLanguage( "überhöht", 5 ).value.should.be.equal( "über" );

		new TypeNameWithoutLanguage( "hello", 1 ).value.should.be.equal( "h" );
		( () => new TypeNameWithoutLanguage( "überhöht", 1 ) ).should.throw(); // because of resulting in empty name
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeNameWithoutLanguage( "hello", 0 ) ).should.throw();
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeNameWithoutLanguage( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "ignores limits on number of encoded octets exceeding some internally defined one", () => {
		new TypeNameWithoutLanguage( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.nameWithoutLanguage] );
	} );

	it( "accepts limit being reduced", () => {
		const value = new TypeNameWithoutLanguage( "überhöht", 9 );
		value.octetLimit.should.be.equal( 9 );
		value.value.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 9; } ).should.not.throw();
		value.value.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.value.should.be.equal( "überhö" );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.value.should.be.equal( "überh" );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.value.should.be.equal( "überh" );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeNameWithoutLanguage( "überhöht", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted", () => {
		const value = new TypeNameWithoutLanguage( "a" );

		value.value = 1;
		value.value.should.be.equal( "1" );
		value.value = -256;
		value.value.should.be.equal( "-256" );
		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = {};
		value.value.should.be.equal( "[object Object]" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );
		// value.value = [];
		// value.value.should.be.equal( "" );
		value.value = [ "foo", "bar" ];
		value.value.should.be.equal( "foo,bar" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.equal( "foo" );
		} );

		value.value = new TypeInteger( 12345 );
		value.value.should.be.equal( "12345" );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeNameWithoutLanguage( "a" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
		( () => { value.value = ""; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeNameWithoutLanguage( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.equal( from === nameWithoutLanguage );
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.equal( from === nameWithoutLanguage );
				value.equals( new TypeString( from, "FOO" ).value ).should.be.true();
			} );
		} );

		it( "is considered different when type of provided value isn't matching", () => {
			new TypeNameWithoutLanguage( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeNameWithoutLanguage( "1" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeNameWithoutLanguage( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeNameWithoutLanguage( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeNameWithoutLanguage( "1" ).equals( new TypeInteger( 1 ).value ).should.be.true();
			new TypeNameWithoutLanguage( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );

		it( "is considered equal when comparing with nameWithLanguage with its language matching provided default language", () => {
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.true();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "FOO", "de" ), { defaultLanguage: "de" } ).should.be.true();
			new TypeNameWithoutLanguage( "FOO" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.true();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ), { defaultLanguage: "en" } ).should.be.false();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeNameWithLanguage( "foo", "de" ) ).should.be.false();
		} );

		it( "is considered different when comparing with textWithLanguage", () => {
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "FOO", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeNameWithoutLanguage( "FOO" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "de" } ).should.be.false();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ), { defaultLanguage: "en" } ).should.be.false();
			new TypeNameWithoutLanguage( "foo" ).equals( new TypeTextWithLanguage( "foo", "de" ) ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeNameWithoutLanguage( "f" ).toString.should.be.a.Function();

		new TypeNameWithoutLanguage( "f" ).toString().should.be.String().which.is.equal( "[nameWithoutLanguage(1) f]" );
		new TypeNameWithoutLanguage( "foo" ).toString().should.be.String().which.is.equal( "[nameWithoutLanguage(3) foo]" );
		new TypeNameWithoutLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[nameWithoutLanguage(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeNameWithoutLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[nameWithoutLanguage(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
