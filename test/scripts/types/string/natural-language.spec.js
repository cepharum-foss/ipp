/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { TypeNaturalLanguage, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A naturalLanguage-type value", () => {
	it( "can be instantiated using RFC 5646-compliant language tag", () => {
		new TypeNaturalLanguage( " en-us " ).should.have.property( "value" ).which.is.equal( "en-us" );
		new TypeNaturalLanguage( " x-cepharum " ).should.have.property( "value" ).which.is.equal( "x-cepharum" );
	} );

	it( "rejects to be instantiated without proper language tag", () => {
		( () => new TypeNaturalLanguage( "" ) ).should.throw();
		( () => new TypeNaturalLanguage( "t" ) ).should.throw();
		( () => new TypeNaturalLanguage( "1" ) ).should.throw();
		( () => new TypeNaturalLanguage( "de-" ) ).should.throw();
		( () => new TypeNaturalLanguage( "-us" ) ).should.throw();
	} );

	it( "exposes proper type identifier", () => {
		new TypeNaturalLanguage( "en" ).should.have.property( "type" ).which.is.equal( AttributeType.naturalLanguage );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeNaturalLanguage( "de" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeNaturalLanguage( "" ) ).should.throw();
	} );

	it( "converts provided strings to lowercase", () => {
		new TypeNaturalLanguage( "En-US-x-CEPHarum" ).should.have.property( "value" ).which.is.equal( "en-us-x-cepharum" );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeNaturalLanguage( "x-höher" ) ).should.throw();
		( () => new TypeNaturalLanguage( "x-válûè" ) ).should.throw();
	} );

	it( "can be instantiated with value cast to properly formatted string", () => {
		new TypeNaturalLanguage( { toString: () => "x-FOO" } ).should.have.property( "value" ).which.is.equal( "x-foo" );
		new TypeNaturalLanguage( true ).value.should.be.equal( "true" );
		new TypeNaturalLanguage( false ).value.should.be.equal( "false" );
	} );

	it( "rejects to be instantiated with value cast to improperly formatted string", () => {
		( () => new TypeNaturalLanguage( 1 ) ).should.throw();
		( () => new TypeNaturalLanguage( -256 ) ).should.throw();
		( () => new TypeNaturalLanguage( {} ) ).should.throw();
		( () => new TypeNaturalLanguage( { toString: () => "f" } ) ).should.throw();
		( () => new TypeNaturalLanguage( [ "foo", "bar" ] ) ).should.throw();
		( () => new TypeNaturalLanguage( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value complying with syntax rules", () => {
		Types.forEach( from => {
			new TypeNaturalLanguage( new TypeString( from, "x-foo-test" ) ).should.have.property( "value" ).which.is.equal( "x-foo-test" );
			new TypeNaturalLanguage( new TypeString( from, "X-FOO-TEST" ) ).should.have.property( "value" ).which.is.equal( "x-foo-test" );
		} );
	} );

	it( "rejects to be instantiated with any string-type value not complying with syntax rules", () => {
		Types.forEach( from => {
			( () => new TypeNaturalLanguage( new TypeString( from, "f" ) ) ).should.throw();
			( () => new TypeNaturalLanguage( new TypeString( from, "f-" ) ) ).should.throw();
			( () => new TypeNaturalLanguage( new TypeString( from, "en-" ) ) ).should.throw();
			( () => new TypeNaturalLanguage( new TypeString( from, "-US" ) ) ).should.throw();
		} );
	} );

	it( "rejects to be instantiated with integer-type value due to failing syntax rules", () => {
		( () => new TypeNaturalLanguage( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeNaturalLanguage( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeNaturalLanguage( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "can be instantiated with boolean-type value for matching syntax rules when cast to string", () => {
		new TypeNaturalLanguage( new TypeBoolean( false ) ).value.should.be.equal( "false" );
		new TypeNaturalLanguage( new TypeBoolean( true ) ).value.should.be.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value complying with syntax rules", () => {
		new TypeNaturalLanguage( new TypeEnum( 2, [ "foo", "en-us" ] ) ).should.have.property( "value" ).which.is.equal( "en-us" );
	} );

	it( "rejects to be instantiated with enumeration-type value complying with syntax rules", () => {
		( () => new TypeNaturalLanguage( new TypeEnum( 2, [ "foo", "en-u" ] ) ) ).should.throw();
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeNaturalLanguage( "x-cepharum", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "instantly applies limit on number of encoded octets to provided value", () => {
		new TypeNaturalLanguage( "de-deu-de", 5 ).value.should.be.equal( "de" );
		new TypeNaturalLanguage( "de-deu-de", 6 ).value.should.be.equal( "de-deu" );
		new TypeNaturalLanguage( "de-deu-de", 7 ).value.should.be.equal( "de-deu" );
		new TypeNaturalLanguage( "de-deu-de", 8 ).value.should.be.equal( "de-deu" );
		new TypeNaturalLanguage( "de-deu-de", 9 ).value.should.be.equal( "de-deu-de" );
		new TypeNaturalLanguage( "de-x-cepharum", 9 ).value.should.be.equal( "de" );
	} );

	it( "rejects limit on number of encoded octets too little for taking first provided subtag", () => {
		( () => new TypeNaturalLanguage( "de-de", 1 ) ).should.throw();
		( () => new TypeNaturalLanguage( "de-de", 2 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeNaturalLanguage( "de-de", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeNaturalLanguage( "de-de", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.naturalLanguage] );
	} );

	it( "accepts limit being reduced instantly trimming existing value", () => {
		const value = new TypeNaturalLanguage( "de-deu-de", 10 );
		value.octetLimit.should.be.equal( 10 );
		value.value.should.be.equal( "de-deu-de" );

		( () => { value.octetLimit = 10; } ).should.not.throw();
		value.octetLimit.should.be.equal( 10 );
		value.value.should.be.equal( "de-deu-de" );

		( () => { value.octetLimit = 9; } ).should.not.throw();
		value.octetLimit.should.be.equal( 9 );
		value.value.should.be.equal( "de-deu-de" );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );
		value.value.should.be.equal( "de-deu" );

		( () => { value.octetLimit = 4; } ).should.not.throw();
		value.octetLimit.should.be.equal( 4 );
		value.value.should.be.equal( "de" );

		( () => { value.octetLimit = 1; } ).should.throw();
		value.octetLimit.should.be.equal( 4 );
		value.value.should.be.equal( "de" );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeNaturalLanguage( "de-deu-de", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted using values complying with syntax rules when cast to string", () => {
		const value = new TypeNaturalLanguage( "de-de" );

		value.value = { toString: () => " EN-US " };
		value.value.should.be.equal( "en-us" );

		Types.forEach( ( from, index ) => {
			value.value = new TypeString( from, "de-" + "ABCDEFGHIJKLMNOPQRSTUVWXYZ".substr( index, 2 ) );
			value.value.should.be.equal( "de-" + "abcdefghijklmnopqrstuvwxyz".substr( index, 2 ) );
		} );

		value.value = new TypeEnum( 2, [ "foo", "de" ] );
		value.value.should.be.equal( "de" );

		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );
	} );

	it( "rejects to be adjusted using values not complying with syntax rules when cast to string", () => {
		const value = new TypeNaturalLanguage( "de-de" );

		( () => { value.value = 1; } ).should.throw();
		value.value.should.be.equal( "de-de" );
		( () => { value.value = -256; } ).should.throw();
		value.value.should.be.equal( "de-de" );
		( () => { value.value = {}; } ).should.throw();
		value.value.should.be.equal( "de-de" );
		( () => { value.value = { toString: () => "-U" }; } ).should.throw();
		value.value.should.be.equal( "de-de" );
		( () => { value.value = [ "foo", "bar" ]; } ).should.throw();
		value.value.should.be.equal( "de-de" );

		Types.forEach( from => {
			( () => { value.value = new TypeString( from, "en-" ); } ).should.throw();
			value.value.should.be.equal( "de-de" );
		} );

		( () => { value.value = new TypeInteger( 12345 ); } ).should.throw();
		value.value.should.be.equal( "de-de" );

		( () => { value.value = new TypeEnum( 2, [ "foo", "-US" ] ); } ).should.throw();
		value.value.should.be.equal( "de-de" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeNaturalLanguage( "de-de" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeNaturalLanguage( "de-de" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeNaturalLanguage( "de" );

		( () => { value.value = "über"; } ).should.throw();
		( () => { value.value = "Vâlúê"; } ).should.throw();
	} );

	it( "trims strings exceeding number of encoded octets on adjustment", () => {
		const value = new TypeNaturalLanguage( "en", 8 );
		value.value.should.be.equal( "en" );

		value.value = "de-de";
		value.value.should.be.equal( "de-de" );
		value.value = "en-us";
		value.value.should.be.equal( "en-us" );
		value.value = "de-ext-de";
		value.value.should.be.equal( "de-ext" );
		value.value = "de-x-cepharum";
		value.value.should.be.equal( "de" );
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeNaturalLanguage( "de-de" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "de-de" ) ).should.be.equal( from === naturalLanguage );
				value.equals( new TypeString( from, "de-de" ).value ).should.be.true();
				value.equals( new TypeString( from, "de-de" ) ).should.be.equal( from === naturalLanguage );
				value.equals( new TypeString( from, "de-de" ).value ).should.be.true();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeNaturalLanguage( "de-de" ).equals( "de-de" ).should.be.true();
			new TypeNaturalLanguage( "de-de" ).equals( ["de-de"] ).should.be.true();
		} );

		it( "is considered equal when as many subtags ara matching as are available in both values", () => {
			new TypeNaturalLanguage( "de-deu-de" ).equals( "DE-DEU" ).should.be.true();
			new TypeNaturalLanguage( "de-deu-de" ).equals( "DE-DE" ).should.be.true();
			new TypeNaturalLanguage( "de-deu-de" ).equals( "DE" ).should.be.true();
			new TypeNaturalLanguage( "de-deu" ).equals( "de-DEU-de" ).should.be.true();
			new TypeNaturalLanguage( "de-de" ).equals( "de-DEU-DE" ).should.be.true();
			new TypeNaturalLanguage( "de" ).equals( "de-DEU-DE" ).should.be.true();

			new TypeNaturalLanguage( "de-deu-de" ).equals( ["DE-deu"] ).should.be.true();
			new TypeNaturalLanguage( "de-deu-de" ).equals( ["DE-de"] ).should.be.true();
			new TypeNaturalLanguage( "de-deu-de" ).equals( ["de"] ).should.be.true();
			new TypeNaturalLanguage( "de-deu" ).equals( ["de-DEU-DE"] ).should.be.true();
			new TypeNaturalLanguage( "de-de" ).equals( ["de-DEU-DE"] ).should.be.true();
			new TypeNaturalLanguage( "de" ).equals( ["dE-DEU-de"] ).should.be.true();

			new TypeNaturalLanguage( "de-sui-de" ).equals( "DE-DEU" ).should.be.false();
			new TypeNaturalLanguage( "de-sui-de" ).equals( "DE-DE" ).should.be.false();
			new TypeNaturalLanguage( "de-sui" ).equals( "de-DEU-de" ).should.be.false();
			new TypeNaturalLanguage( "de-ch" ).equals( "de-DEU-DE" ).should.be.false();

			new TypeNaturalLanguage( "de-deu-de" ).equals( ["DE-sui"] ).should.be.false();
			new TypeNaturalLanguage( "de-deu-de" ).equals( ["DE-ch"] ).should.be.false();
			new TypeNaturalLanguage( "de-deu" ).equals( ["de-SUI-DE"] ).should.be.false();
			new TypeNaturalLanguage( "de-de" ).equals( ["de-SUI-DE"] ).should.be.false();
		} );

		it( "is considered different when type of provided value isn't string-type", () => {
			new TypeNaturalLanguage( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeNaturalLanguage( "de" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeNaturalLanguage( "de-de" ).equals( new TypeEnum( 2, [ "foo", "de-de" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeNaturalLanguage( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeNaturalLanguage( "de-de" ).equals( new TypeEnum( 2, [ "foo", "de-de" ] ).value ).should.be.true();
		} );
	} );

	it( "exposes current language subtag in separate property which is read-only", () => {
		const value = new TypeNaturalLanguage( "de-deu-de" );

		value.should.have.property( "language" ).which.is.equal( "de-deu" );
		( () => { value.language = "de"; } ).should.throw();
	} );

	it( "exposes current script in separate property which is read-only", () => {
		const value = new TypeNaturalLanguage( "de-deu-info" );

		value.should.have.property( "script" ).which.is.equal( "info" );
		( () => { value.script = "some"; } ).should.throw();

		value.value = "de-info";
		value.should.have.property( "script" ).which.is.equal( "info" );
	} );

	it( "exposes current region in separate property which is read-only", () => {
		const value = new TypeNaturalLanguage( "de-de" );

		value.should.have.property( "region" ).which.is.equal( "de" );
		( () => { value.region = "at"; } ).should.throw();

		value.value = "de-deu-info-at";
		value.should.have.property( "region" ).which.is.equal( "at" );
	} );

	it( "exposes current variant in separate property which is read-only", () => {
		const value = new TypeNaturalLanguage( "de-nodead" );

		value.should.have.property( "variant" ).which.is.equal( "nodead" );
		( () => { value.region = "altkeys"; } ).should.throw();

		value.value = "de-deu-info-at-altkeys";
		value.should.have.property( "variant" ).which.is.equal( "altkeys" );
	} );

	it( "exposes current extension in separate property which is read-only", () => {
		const value = new TypeNaturalLanguage( "de-a-alt" );

		value.should.have.property( "extension" ).which.is.equal( "a-alt" );
		( () => { value.region = "b-alt"; } ).should.throw();

		value.value = "de-deu-info-at-b-alt";
		value.should.have.property( "extension" ).which.is.equal( "b-alt" );
	} );

	it( "renders value as string", () => {
		new TypeNaturalLanguage( "fo" ).toString.should.be.a.Function();

		new TypeNaturalLanguage( "fo" ).toString().should.be.String().which.is.equal( "[naturalLanguage(2) fo]" );
		new TypeNaturalLanguage( "foo" ).toString().should.be.String().which.is.equal( "[naturalLanguage(3) foo]" );
	} );
} );
