/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { TypeUri, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A uri-type value", () => {
	it( "can be instantiated", () => {
		new TypeUri( "urn:1" ).should.have.property( "value" ).which.is.equal( "urn:1" );
		new TypeUri( "ipp://john.doe@127.0.0.1:631/printer/default?mode=custom#top" ).should.have.property( "value" ).which.is.equal( "ipp://john.doe@127.0.0.1:631/printer/default?mode=custom#top" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeUri( "urn:1" ).should.have.property( "type" ).which.is.equal( AttributeType.uri );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeUri( "urn:1" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeUri( "" ) ).should.throw();
	} );

	it( "rejects to be instantiated with string not complying with syntax rules", () => {
		( () => new TypeUri( "foo" ) ).should.throw();
		( () => new TypeUri( "a" ) ).should.throw();
		( () => new TypeUri( "1:a" ) ).should.throw();
		( () => new TypeUri( ":a" ) ).should.throw();
		( () => new TypeUri( "foo :a" ) ).should.throw();
	} );

	it( "converts scheme and host components to lowercase on normalizing URL in compliance with RFC 3986", () => {
		new TypeUri( "HTTP://JOHN@test.CEPHARUM.de/sOME/Path?QUerY#FRAgMENT" ).value
			.should.be.equal( "http://JOHN@test.cepharum.de/sOME/Path?QUerY#FRAgMENT" );
	} );

	it( "drops empty userinfo component on normalizing URL in compliance with RFC 3986", () => {
		new TypeUri( "http://@test.cepharum.de" ).value.should.be.equal( "http://test.cepharum.de" );
	} );

	it( "drops empty port component on normalizing URL in compliance with RFC 3986", () => {
		new TypeUri( "http://test.cepharum.de:" ).value.should.be.equal( "http://test.cepharum.de" );
		new TypeUri( "http://test.cepharum.de:/" ).value.should.be.equal( "http://test.cepharum.de/" );
		new TypeUri( "http://test.cepharum.de:?a" ).value.should.be.equal( "http://test.cepharum.de?a" );
		new TypeUri( "http://test.cepharum.de:#a" ).value.should.be.equal( "http://test.cepharum.de#a" );
	} );

	it( "converts all percent encodings to uppercase on normalizing URL in compliance with RFC 3986", () => {
		new TypeUri( "http://je%fff@TR%ace.CEpharum.de/L%ead?R%each#T%eacH" ).value
			.should.be.equal( "http://je%FFf@tr%ACe.cepharum.de/L%EAd?R%EAch#T%EAcH" );
	} );

	it( "exposes normalized scheme of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).scheme.should.be.equal( "http" );
		new TypeUri( "URN:L%ead?R%each#T%eacH" ).scheme.should.be.equal( "urn" );
	} );

	it( "exposes normalized authority of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).authority.should.be.equal( "je%FFf@tr%ACe.cepharum.de:1234" );
		( new TypeUri( "URN:L%ead?R%each#T%eacH" ).authority == null ).should.be.true();
	} );

	it( "exposes normalized userinfo of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).userinfo.should.be.equal( "je%FFf" );
		( new TypeUri( "URN:L%ead?R%each#T%eacH" ).userinfo == null ).should.be.true();
	} );

	it( "exposes normalized host of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).host.should.be.equal( "tr%ACe.cepharum.de" );
		( new TypeUri( "URN:L%ead?R%each#T%eacH" ).host == null ).should.be.true();
	} );

	it( "exposes normalized port of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).port.should.be.equal( "1234" );
		( new TypeUri( "URN:L%ead?R%each#T%eacH" ).port == null ).should.be.true();
	} );

	it( "exposes normalized path of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).path.should.be.equal( "/L%EAd" );
		new TypeUri( "URN:L%ead?R%each#T%eacH" ).path.should.be.equal( "L%EAd" );
	} );

	it( "exposes normalized query of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).query.should.be.equal( "R%EAch" );
		new TypeUri( "URN:L%ead?R%each#T%eacH" ).query.should.be.equal( "R%EAch" );
	} );

	it( "exposes normalized fragment of URI", () => {
		new TypeUri( "HTTP://je%fff@TR%ace.CEpharum.de:1234/L%ead?R%each#T%eacH" ).fragment.should.be.equal( "T%EAcH" );
		new TypeUri( "URN:L%ead?R%each#T%eacH" ).fragment.should.be.equal( "T%EAcH" );
	} );

	it( "drops percent encodings of unreserved characters on normalizing URL in compliance with RFC 3986", () => {
		new TypeUri( "urn:%41%2f" ).value.should.be.equal( "urn:A%2F" );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeUri( "http://schön.cepharum.de" ) ).should.throw();
		( () => new TypeUri( "http://schoen.cepharum.de" ) ).should.not.throw();
		( () => new TypeUri( "ipp://válue" ) ).should.throw();
		( () => new TypeUri( "ipp://value" ) ).should.not.throw();
	} );

	it( "can be instantiated with value cast to string complying with syntax rules", () => {
		new TypeUri( { toString: () => "URN:A" } ).should.have.property( "value" ).which.is.equal( "urn:A" );
	} );

	it( "rejects to be instantiated with value cast to string not complying with syntax rules", () => {
		( () => new TypeUri( true ) ).should.throw();
		( () => new TypeUri( false ) ).should.throw();
		( () => new TypeUri( 1 ) ).should.throw();
		( () => new TypeUri( -256 ) ).should.throw();
		( () => new TypeUri( {} ) ).should.throw();
		( () => new TypeUri( [ "foo", "bar" ] ) ).should.throw();

		( () => new TypeUri( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value complying with syntax rules", () => {
		Types.forEach( from => {
			new TypeUri( new TypeString( from, "HTTP://TEST" ) ).should.have.property( "value" ).which.is.equal( "http://test" );
		} );
	} );

	it( "rejects to be instantiated with any string-type value not complying with syntax rules", () => {
		Types.forEach( from => {
			( () => new TypeUri( new TypeString( from, "" ) ) ).should.throw();
			( () => new TypeUri( new TypeString( from, "B" ) ) ).should.throw();
			( () => new TypeUri( new TypeString( from, "http" ) ) ).should.throw();
		} );
	} );

	it( "rejects to be instantiated with integer-type value for not complying with syntax rules", () => {
		( () => new TypeUri( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeUri( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeUri( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "rejects to be instantiated with boolean-type value for not complying with syntax rules when cast to string", () => {
		( () => new TypeUri( new TypeBoolean( false ) ) ).should.throw();
		( () => new TypeUri( new TypeBoolean( true ) ) ).should.throw();
	} );

	it( "cannot be instantiated with enumeration-type value for those reject to cover values complying with syntax rules", () => {
		( () => new TypeUri( new TypeEnum( 2, [ "foo", "urn:1" ] ) ) ).should.throw();
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeUri( "urn:1", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeUri( "ftp://test.de", 10 ) ).should.throw();
		( () => new TypeUri( "ftp://test.de", 11 ) ).should.throw();
		( () => new TypeUri( "ftp://test.de", 12 ) ).should.throw();
		( () => new TypeUri( "ftp://test.de", 13 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeUri( "urn:1", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeUri( "urn:1", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.uri] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeUri( "urn:ab", 8 );
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.octetLimit.should.be.equal( 7 );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.octetLimit.should.be.equal( 6 );

		( () => { value.octetLimit = 5; } ).should.throw();
		value.octetLimit.should.be.equal( 6 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeUri( "urn:ab", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted using value complying with syntax rules when cast to string", () => {
		const value = new TypeUri( "urn:1" );

		value.value = { toString: () => "URN:B" };
		value.value.should.be.equal( "urn:B" );

		Types.forEach( from => {
			value.value = new TypeString( from, "URN:A" );
			value.value.should.be.equal( "urn:A" );
			value.value = new TypeString( from, "URN:C" );
			value.value.should.be.equal( "urn:C" );
		} );
	} );

	it( "rejects to be adjusted using value not complying with syntax rules when cast to string", () => {
		const value = new TypeUri( "urn:1" );

		( () => { value.value = 1; } ).should.throw();
		value.value.should.be.equal( "urn:1" );
		( () => { value.value = -256; } ).should.throw();
		value.value.should.be.equal( "urn:1" );
		( () => { value.value = {}; } ).should.throw();
		value.value.should.be.equal( "urn:1" );
		( () => { value.value = [ "foo", "bar" ]; } ).should.throw();
		value.value.should.be.equal( "urn:1" );

		Types.forEach( from => {
			( () => { value.value = new TypeString( from, "-" ); } ).should.throw();
			value.value.should.be.equal( "urn:1" );
		} );

		( () => { value.value = new TypeInteger( 12345 ); } ).should.throw();
		value.value.should.be.equal( "urn:1" );

		( () => { value.value = true; } ).should.throw();
		value.value.should.be.equal( "urn:1" );
		( () => { value.value = false; } ).should.throw();
		value.value.should.be.equal( "urn:1" );
		( () => { value.value = new TypeBoolean( false ); } ).should.throw();
		value.value.should.be.equal( "urn:1" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeUri( "urn:1" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeUri( "urn:1" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeUri( "urn:1" );

		( () => { value.value = "urn:ö"; } ).should.throw();
		( () => { value.value = "urn:válùê"; } ).should.throw();
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeUri( "urn:foo", 7 );

		( () => { value.value = "urn:fo"; } ).should.not.throw();
		( () => { value.value = "urn:bar"; } ).should.not.throw();
		( () => { value.value = "urn:foob"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeUri( "IPP://JOHN@T%feST:/%41?Query" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "ipp://JOHN@t%FEst/A?Query" ) ).should.be.equal( from === uri );

				value.equals( new TypeString( from, "ipp://JOHN@t%fest:/%41?Query" ) ).should.be.false(); // for missing normalization of value via TypeUri
				value.equals( new TypeString( from, "ipp://JOHN@t%fest:/%41?Query" ).value ).should.be.true();
				value.equals( new TypeString( from, "ipp://JOHN@t%fest:/A?Query" ) ).should.be.false(); // for missing normalization of value via TypeUri
				value.equals( new TypeString( from, "ipp://JOHN@t%fest:/A?Query" ).value ).should.be.true();
				value.equals( new TypeString( from, "IPP://JOHN@T%FEST:/A?Query" ) ).should.be.false(); // for missing normalization of value via TypeUri
				value.equals( new TypeString( from, "IPP://JOHN@T%FEST:/A?Query" ).value ).should.be.true();
				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/%41?Query" ) ).should.be.false(); // for missing normalization of value via TypeUri
				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/%41?Query" ).value ).should.be.true();

				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/%41?query" ) ).should.be.false();
				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/%41?query" ).value ).should.be.false();
				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/a?Query" ) ).should.be.false();
				value.equals( new TypeString( from, "IPP://JOHN@T%feST:/a?Query" ).value ).should.be.false();
				value.equals( new TypeString( from, "IPP://jOHN@T%feST:/a?Query" ) ).should.be.false();
				value.equals( new TypeString( from, "IPP://jOHN@T%feST:/a?Query" ).value ).should.be.false();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeUri( "urn:1" ).equals( "URN:1" ).should.be.true();
			new TypeUri( "URN:A" ).equals( "urn:A" ).should.be.true();
			new TypeUri( "A:B" ).equals( ["A:B"] ).should.be.true();
		} );

		it( "can't be compared with enumeration-type value for those reject to cover values complying with syntax rules", () => {
			( () => new TypeUri( "urn:a" ).equals( new TypeEnum( 2, [ "foo", "urn:a" ] ) ) ).should.throw();
			( () => new TypeUri( "urn:a" ).equals( new TypeEnum( 2, [ "foo", "urn:a" ] ).value ) ).should.throw();
		} );
	} );

	it( "renders value as string", () => {
		new TypeUri( "u:1" ).toString.should.be.a.Function();

		new TypeUri( "U:1" ).toString().should.be.String().which.is.equal( "[uri(3) u:1]" );
		new TypeUri( "fo:foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[uri(48) fo:foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeUri( "fo:foofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[uri(49) fo:foofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
