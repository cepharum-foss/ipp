/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { TypeMimeMediaType, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A mimeMediaType-type value", () => {
	it( "can be instantiated using MIME information", () => {
		new TypeMimeMediaType( " text/html " ).should.have.property( "value" ).which.is.equal( "text/html" );
		new TypeMimeMediaType( " text/html ; charset=utf-8 ; foo = \"bar with space\" " ).should.have.property( "value" )
			.which.is.equal( "text/html ; charset=utf-8 ; foo = \"bar with space\"" );
	} );

	it( "rejects to be instantiated without MIME information", () => {
		( () => new TypeMimeMediaType( "" ) ).should.throw();
		( () => new TypeMimeMediaType( "t" ) ).should.throw();
		( () => new TypeMimeMediaType( "1" ) ).should.throw();
	} );

	it( "exposes proper type identifier", () => {
		new TypeMimeMediaType( "text/html" ).should.have.property( "type" ).which.is.equal( AttributeType.mimeMediaType );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeMimeMediaType( "text/html" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeMimeMediaType( "" ) ).should.throw();
	} );

	it( "does not convert provided strings to lower-case", () => {
		new TypeMimeMediaType( "TEXT/HTML" ).should.have.property( "value" ).which.is.equal( "TEXT/HTML" );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeMimeMediaType( "x-höher/x-válûé" ) ).should.throw();
	} );

	it( "can be instantiated with value cast to properly formatted string", () => {
		new TypeMimeMediaType( { toString: () => "x-foo/bar" } ).should.have.property( "value" ).which.is.equal( "x-foo/bar" );
	} );

	it( "rejects to be instantiated with value cast to improperly formatted string", () => {
		( () => new TypeMimeMediaType( 1 ) ).should.throw();
		( () => new TypeMimeMediaType( -256 ) ).should.throw();
		( () => new TypeMimeMediaType( true ) ).should.throw();
		( () => new TypeMimeMediaType( false ) ).should.throw();
		( () => new TypeMimeMediaType( {} ) ).should.throw();
		( () => new TypeMimeMediaType( { toString: () => "foo" } ) ).should.throw();
		( () => new TypeMimeMediaType( [ "foo", "bar" ] ) ).should.throw();
		( () => new TypeMimeMediaType( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value complying with syntax rules", () => {
		Types.forEach( from => {
			new TypeMimeMediaType( new TypeString( from, "text/html" ) ).should.have.property( "value" ).which.is.equal( "text/html" );
			new TypeMimeMediaType( new TypeString( from, "TEXT/HTML" ) ).should.have.property( "value" ).which.is.equal( "TEXT/HTML" );
		} );
	} );

	it( "rejects to be instantiated with any string-type value not complying with syntax rules", () => {
		Types.forEach( from => {
			( () => new TypeMimeMediaType( new TypeString( from, "text/" ) ) ).should.throw();
			( () => new TypeMimeMediaType( new TypeString( from, "text" ) ) ).should.throw();
			( () => new TypeMimeMediaType( new TypeString( from, "TEXT" ) ) ).should.throw();
		} );
	} );

	it( "rejects to be instantiated with integer-type value due to failing syntax rules", () => {
		( () => new TypeMimeMediaType( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeMimeMediaType( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeMimeMediaType( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "rejects to be instantiated with boolean-type value due to failing syntax rules", () => {
		( () => new TypeMimeMediaType( new TypeBoolean( false ) ) ).should.throw();
		( () => new TypeMimeMediaType( new TypeBoolean( true ) ) ).should.throw();
	} );

	it( "can't be instantiated with enumeration-type value for those reject any value complying with syntax rules", () => {
		( () => new TypeMimeMediaType( new TypeEnum( 2, [ "foo", "text/html" ] ) ) ).should.throw();
		( () => new TypeMimeMediaType( new TypeEnum( 2, [ "foo", "bar" ] ) ) ).should.throw();
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeMimeMediaType( "text/html", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeMimeMediaType( "text/html", 6 ) ).should.throw();
		( () => new TypeMimeMediaType( "text/html", 7 ) ).should.throw();
		( () => new TypeMimeMediaType( "text/html", 8 ) ).should.throw();
		( () => new TypeMimeMediaType( "text/html", 9 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeMimeMediaType( "text/html", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeMimeMediaType( "text/html", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.mimeMediaType] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeMimeMediaType( "text/html", 10 );
		value.octetLimit.should.be.equal( 10 );

		( () => { value.octetLimit = 10; } ).should.not.throw();
		value.octetLimit.should.be.equal( 10 );

		( () => { value.octetLimit = 9; } ).should.not.throw();
		value.octetLimit.should.be.equal( 9 );

		( () => { value.octetLimit = 8; } ).should.throw();
		value.octetLimit.should.be.equal( 9 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeMimeMediaType( "text/html", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted using values complying with syntax rules when cast to string", () => {
		const value = new TypeMimeMediaType( "text/html" );

		value.value = { toString: () => "text/Plain" };
		value.value.should.be.equal( "text/Plain" );

		Types.forEach( ( from, index ) => {
			value.value = new TypeString( from, "text/type-" + index );
			value.value.should.be.equal( "text/type-" + index );
		} );
	} );

	it( "rejects to be adjusted using values not complying with syntax rules when cast to string", () => {
		const value = new TypeMimeMediaType( "text/html" );

		( () => { value.value = 1; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = -256; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = true; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = false; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = {}; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = { toString: () => "foo" }; } ).should.throw();
		value.value.should.be.equal( "text/html" );
		( () => { value.value = [ "foo", "bar" ]; } ).should.throw();
		value.value.should.be.equal( "text/html" );

		Types.forEach( from => {
			( () => { value.value = new TypeString( from, "foo" ); } ).should.throw();
			value.value.should.be.equal( "text/html" );
		} );

		( () => { value.value = new TypeInteger( 12345 ); } ).should.throw();
		value.value.should.be.equal( "text/html" );

		( () => { value.value = new TypeBoolean( false ); } ).should.throw();
		value.value.should.be.equal( "text/html" );

		( () => { value.value = new TypeEnum( 2, [ "foo", "bar" ] ); } ).should.throw();
		value.value.should.be.equal( "text/html" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeMimeMediaType( "text/html" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeMimeMediaType( "text/html" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeMimeMediaType( "text/html" );

		( () => { value.value = "überhöht"; } ).should.throw();
		( () => { value.value = "ü"; } ).should.throw();
		( () => { value.value = "Vâlúê"; } ).should.throw();
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeMimeMediaType( "text/html", 9 );

		( () => { value.value = "text/htm"; } ).should.not.throw();
		( () => { value.value = "x-foo/bar"; } ).should.not.throw();
		( () => { value.value = "text/plain"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeMimeMediaType( "text/html" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "text/html" ) ).should.be.equal( from === mimeMediaType );
				value.equals( new TypeString( from, "text/html" ).value ).should.be.true();
				value.equals( new TypeString( from, "text/html" ) ).should.be.equal( from === mimeMediaType );
				value.equals( new TypeString( from, "text/html" ).value ).should.be.true();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeMimeMediaType( "text/html" ).equals( "text/html" ).should.be.true();
			new TypeMimeMediaType( "text/html" ).equals( ["text/html"] ).should.be.true();
		} );

		it( "is considered different when type of provided value isn't string-type", () => {
			new TypeMimeMediaType( "text/html" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeMimeMediaType( "text/html" ).equals( new TypeInteger( 1 ) ).should.be.false();
		} );

		it( "can't be compared with enumeration-type value for those reject to cover values complying with syntax rules", () => {
			( () => new TypeMimeMediaType( "text/html" ).equals( new TypeEnum( 2, [ "foo", "text/html" ] ) ) ).should.throw();
			( () => new TypeMimeMediaType( "text/html" ).equals( new TypeEnum( 2, [ "foo", "text/html" ] ).value ) ).should.throw();
		} );
	} );

	it( "exposes current media type in separate property which is read-only", () => {
		const value = new TypeMimeMediaType( "text/html; charset=utf-8" );

		value.should.have.property( "major" ).which.is.equal( "text" );
		( () => { value.major = "image"; } ).should.throw();
	} );

	it( "exposes current media subtype in separate property which is read-only", () => {
		const value = new TypeMimeMediaType( "text/html; charset=utf-8" );

		value.should.have.property( "minor" ).which.is.equal( "html" );
		( () => { value.minor = "plain"; } ).should.throw();
	} );

	it( "exposes set of parameters additionally included with current media type", () => {
		const value = new TypeMimeMediaType( `text/html; charset = utF-8; FOO= " bar with Space "` );
		value.major.should.be.equal( "text" );
		value.minor.should.be.equal( "html" );

		value.should.have.property( "parameters" ).which.is.an.Object().which.has.properties( "charset", "foo" );
		value.parameters.charset.should.be.equal( "utF-8" );
		value.parameters.foo.should.be.equal( " bar with Space " );

		( () => { value.parameters = {}; } ).should.throw();
		( () => { value.parameters.charset = "us-ascii"; } ).should.throw();
	} );

	it( "exposes empty set of parameters by default", () => {
		new TypeMimeMediaType( "text/html" ).should.have.property( "parameters" ).which.is.empty();
	} );

	it( "renders value as string", () => {
		new TypeMimeMediaType( "f/f" ).toString.should.be.a.Function();

		new TypeMimeMediaType( "f/f" ).toString().should.be.String().which.is.equal( "[mimeMediaType(3) f/f]" );
		new TypeMimeMediaType( "text/HTML" ).toString().should.be.String().which.is.equal( "[mimeMediaType(9) text/HTML]" );
		new TypeMimeMediaType( "foo/foofoofoofoofoofoofoofoofoofoofoofoofoofoofo" ).toString().should.be.String().which.is.equal( "[mimeMediaType(48) foo/foofoofoofoofoofoofoofoofoofoofoofoofoofoofo]" );
		new TypeMimeMediaType( "foo/foofoofoofoofoofoofoofoofoofoofoofoofoofoofox" ).toString().should.be.String().which.is.equal( "[mimeMediaType(49) foo/foofoofoofoofoofoofoofoofoofoofoofoofoofo...]" );
	} );
} );
