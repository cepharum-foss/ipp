/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const TypeString = require( "../../../../lib/types/string/string" );
const { TypeCharset, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A charset-type value", () => {
	it( "can be instantiated", () => {
		new TypeCharset( "a" ).should.have.property( "value" ).which.is.equal( "a" );
		new TypeCharset( "hello" ).should.have.property( "value" ).which.is.equal( "hello" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeCharset( "a" ).should.have.property( "type" ).which.is.equal( AttributeType.charset );
	} );

	it( "is derived from common string-type implementation", () => {
		new TypeCharset( "a" ).should.be.instanceOf( TypeString );
	} );

	it( "rejects to be instantiated with empty string", () => {
		( () => new TypeCharset( "" ) ).should.throw();
	} );

	it( "always converts provided strings to lower-case", () => {
		new TypeCharset( "FooBar" ).should.have.property( "value" ).which.is.equal( "foobar" );
	} );

	it( "rejects values containing non-ASCII characters", () => {
		( () => new TypeCharset( "überhöht" ) ).should.throw();
		( () => new TypeCharset( "ü" ) ).should.throw();
		( () => new TypeCharset( "fóôbÂr" ) ).should.throw();
	} );

	it( "can be instantiated with value cast to string", () => {
		new TypeCharset( 1 ).should.have.property( "value" ).which.is.equal( "1" );
		new TypeCharset( -256 ).should.have.property( "value" ).which.is.equal( "-256" );
		new TypeCharset( true ).should.have.property( "value" ).which.is.equal( "true" );
		new TypeCharset( false ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeCharset( {} ).should.have.property( "value" ).which.is.equal( "[object object]" );
		new TypeCharset( { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
		new TypeCharset( [ "foo", "bar" ] ).should.have.property( "value" ).which.is.equal( "foo,bar" );

		( () => new TypeCharset( [] ) ).should.throw();
	} );

	it( "can be instantiated with any string-type value", () => {
		Types.forEach( from => {
			new TypeCharset( new TypeString( from, "a" ) ).should.have.property( "value" ).which.is.equal( "a" );
			new TypeCharset( new TypeString( from, "B" ) ).should.have.property( "value" ).which.is.equal( "b" );
		} );
	} );

	it( "can be instantiated with integer-type value", () => {
		new TypeCharset( new TypeInteger( 0 ) ).should.have.property( "value" ).which.is.equal( "0" );
		new TypeCharset( new TypeInteger( -1234 ) ).should.have.property( "value" ).which.is.equal( "-1234" );
		new TypeCharset( new TypeInteger( 5678 ) ).should.have.property( "value" ).which.is.equal( "5678" );
	} );

	it( "can be instantiated with boolean-type value", () => {
		new TypeCharset( new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
		new TypeCharset( new TypeBoolean( true ) ).should.have.property( "value" ).which.is.equal( "true" );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeCharset( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeCharset( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeCharset( "extraordinary", 10 ) ).should.throw();
		( () => new TypeCharset( "extraordinary", 11 ) ).should.throw();
		( () => new TypeCharset( "extraordinary", 12 ) ).should.throw();
		( () => new TypeCharset( "extraordinary", 13 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeCharset( "hello", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeCharset( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[AttributeType.charset] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeCharset( "foobar", 8 );
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.octetLimit.should.be.equal( 7 );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.octetLimit.should.be.equal( 6 );

		( () => { value.octetLimit = 5; } ).should.throw();
		value.octetLimit.should.be.equal( 6 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeCharset( "foobar", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted", () => {
		const value = new TypeCharset( "a" );

		value.value = 1;
		value.value.should.be.equal( "1" );
		value.value = -256;
		value.value.should.be.equal( "-256" );
		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = {};
		value.value.should.be.equal( "[object object]" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );
		value.value = [ "foo", "bar" ];
		value.value.should.be.equal( "foo,bar" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.equal( "foo" );
		} );

		value.value = new TypeInteger( 12345 );
		value.value.should.be.equal( "12345" );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects empty string or value cast to empty string on adjustment", () => {
		const value = new TypeCharset( "a" );

		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeCharset( "a" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "rejects strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeCharset( "foobar" );

		( () => { value.value = "überhöht"; } ).should.throw();
		( () => { value.value = "ü"; } ).should.throw();
		( () => { value.value = "Vâlúê"; } ).should.throw();
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeCharset( "foobar", 7 );

		( () => { value.value = "barfoo"; } ).should.not.throw();
		( () => { value.value = "foobarb"; } ).should.not.throw();
		( () => { value.value = "foobarba"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-insensitively", () => {
			const value = new TypeCharset( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.equal( from === charset );
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.equal( from === charset );
				value.equals( new TypeString( from, "FOO" ).value ).should.be.true();
			} );
		} );

		it( "is considered equal when provided value isn't Type-based but represents proper value of current type", () => {
			new TypeCharset( "true" ).equals( "true" ).should.be.true();
			new TypeCharset( "1" ).equals( "1" ).should.be.true();
			new TypeCharset( "bar" ).equals( "bar" ).should.be.true();
			new TypeCharset( "bar" ).equals( ["bar"] ).should.be.true();
		} );

		it( "is considered different when type of provided value isn't string-type", () => {
			new TypeCharset( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeCharset( "1" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeCharset( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type", () => {
			new TypeCharset( "true" ).equals( new TypeBoolean( true ).value ).should.be.true();
			new TypeCharset( "1" ).equals( new TypeInteger( 1 ).value ).should.be.true();
			new TypeCharset( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );
	} );

	it( "renders value as string", () => {
		new TypeCharset( "f" ).toString.should.be.a.Function();

		new TypeCharset( "f" ).toString().should.be.String().which.is.equal( "[charset(1) f]" );
		new TypeCharset( "foo" ).toString().should.be.String().which.is.equal( "[charset(3) foo]" );
		new TypeCharset( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[charset(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo]" );
		new TypeCharset( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[charset(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo...]" );
	} );
} );
