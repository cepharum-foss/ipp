/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const TypeString = require( "../../../../lib/types/string/string" );
const { TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;

const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A common string-type value", () => {
	it( "can be instantiated", () => {
		Types.forEach( to => {
			new TypeString( to, "" ).should.have.property( "value" ).which.is.equal( "" );
			new TypeString( to, "foo" ).should.have.property( "value" ).which.is.equal( "foo" );
			new TypeString( to, "BAR" ).should.have.property( "value" ).which.is.equal( "BAR" );
		} );
	} );

	it( "extends abstract Type", () => {
		Types.forEach( to => {
			new TypeString( to, "" ).should.be.instanceOf( Type );
		} );
	} );

	it( "exposes type identifier provided on construction", () => {
		Types.forEach( to => {
			new TypeString( to, "" ).should.have.property( "type" ).which.is.equal( to );
		} );
	} );

	it( "can be instantiated with value cast to string", () => {
		Types.forEach( to => {
			new TypeString( to, 1 ).should.have.property( "value" ).which.is.equal( "1" );
			new TypeString( to, -256 ).should.have.property( "value" ).which.is.equal( "-256" );
			new TypeString( to, true ).should.have.property( "value" ).which.is.equal( "true" );
			new TypeString( to, false ).should.have.property( "value" ).which.is.equal( "false" );
			new TypeString( to, {} ).should.have.property( "value" ).which.is.equal( "[object Object]" );
			new TypeString( to, { toString: () => "foo" } ).should.have.property( "value" ).which.is.equal( "foo" );
			new TypeString( to, [] ).should.have.property( "value" ).which.is.equal( "" );
			new TypeString( to, [ "foo", "bar" ] ).should.have.property( "value" ).which.is.equal( "foo,bar" );
		} );
	} );

	it( "can be instantiated with string-type value", () => {
		Types.forEach( to => {
			Types.forEach( from => {
				new TypeString( to, new TypeString( from, "" ) ).should.have.property( "value" ).which.is.equal( "" );
				new TypeString( to, new TypeString( from, "" ) ).should.have.property( "type" ).which.is.equal( to );
			} );
		} );
	} );

	it( "can be instantiated with integer-type value", () => {
		Types.forEach( to => {
			new TypeString( to, new TypeInteger( 12345 ) ).should.have.property( "value" ).which.is.equal( "12345" );
			new TypeString( to, new TypeInteger( 12345 ) ).should.have.property( "type" ).which.is.equal( to );
		} );
	} );

	it( "can be instantiated with boolean-type value", () => {
		Types.forEach( to => {
			new TypeString( to, new TypeBoolean( false ) ).should.have.property( "value" ).which.is.equal( "false" );
			new TypeString( to, new TypeBoolean( false ) ).should.have.property( "type" ).which.is.equal( to );
		} );
	} );

	it( "can be instantiated with enumeration-type value", () => {
		Types.forEach( to => {
			new TypeString( to, new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "value" ).which.is.equal( "bar" );
			new TypeString( to, new TypeEnum( 2, [ "foo", "bar" ] ) ).should.have.property( "type" ).which.is.equal( to );
		} );
	} );

	it( "rejects to be instantiated with null-ish value", () => {
		Types.forEach( to => {
			( () => new TypeString( to, undefined ) ).should.throw();
			( () => new TypeString( to, null ) ).should.throw();
		} );
	} );

	it( "can be adjusted", () => {
		Types.forEach( to => {
			const value = new TypeString( to, "" );

			value.value = 1;
			value.value.should.be.equal( "1" );
			value.value = -256;
			value.value.should.be.equal( "-256" );
			value.value = true;
			value.value.should.be.equal( "true" );
			value.value = false;
			value.value.should.be.equal( "false" );
			value.value = {};
			value.value.should.be.equal( "[object Object]" );
			value.value = { toString: () => "foo" };
			value.value.should.be.equal( "foo" );
			value.value = [];
			value.value.should.be.equal( "" );
			value.value = [ "foo", "bar" ];
			value.value.should.be.equal( "foo,bar" );

			Types.forEach( from => {
				value.value = new TypeString( from, "foo" );
				value.value.should.be.equal( "foo" );
			} );

			value.value = new TypeInteger( 12345 );
			value.value.should.be.equal( "12345" );

			value.value = new TypeBoolean( false );
			value.value.should.be.equal( "false" );

			value.value = new TypeEnum( 2, [ "foo", "bar" ] );
			value.value.should.be.equal( "bar" );
		} );
	} );

	it( "rejects null-ish value on adjustment", () => {
		Types.forEach( to => {
			const value = new TypeString( to, "" );

			( () => { value.value = undefined; } ).should.throw();
			( () => { value.value = null; } ).should.throw();
		} );
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when both values' types are string-based and strings are matching case-sensitively", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "foo" );

				Types.forEach( from => {
					value.equals( new TypeString( from, "foo" ) ).should.be.true();
					value.equals( new TypeString( from, "FOO" ) ).should.be.false();
				} );
			} );
		} );

		it( "is considered different when type of provided value isn't string-based", () => {
			Types.forEach( to => {
				new TypeString( to, "true" ).equals( new TypeBoolean( true ) ).should.be.false();
				new TypeString( to, "1" ).equals( new TypeInteger( 1 ) ).should.be.false();
				new TypeString( to, "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
			} );
		} );
	} );

	describe( "exposes static function for retrieving number of UTF-8 encoded characters fitting in defined number of octets which", () => {
		it( "requires input string and limit on number of encoded octets", () => {
			TypeString.limitUnicodeOctets.should.be.Function().which.has.length( 2 );
		} );

		it( "returns Infinity if all characters fit in provided limit", () => {
			TypeString.limitUnicodeOctets( "foobar", 10 ).should.be.equal( Infinity );
			TypeString.limitUnicodeOctets( "foobar", 6 ).should.be.equal( Infinity );
		} );

		it( "returns number of characters in provided string that will fit in defined limit after being UTF-8 encoded", () => {
			TypeString.limitUnicodeOctets( "foobar", 5 ).should.be.equal( 5 );
			TypeString.limitUnicodeOctets( "foobar", 1 ).should.be.equal( 1 );
		} );

		it( "completely drops characters consuming multiple octets in encoded form when spanning defined limit", () => {
			const data = [
				[ "höhe", 5, Infinity ],
				[ "höhe", 4, 3 ],
				[ "höhe", 3, 2 ],
				[ "höhe", 2, 1 ],
				[ "höhe", 1, 1 ],

				[ "a→b", 5, Infinity ],
				[ "a→b", 4, 2 ],
				[ "a→b", 3, 1 ],
				[ "a→b", 2, 1 ],
				[ "a→b", 1, 1 ],
			];

			data.forEach( ( [ text, limit, expected ] ) => {
				TypeString.limitUnicodeOctets( text, limit ).should.be.equal( expected );
				Buffer.from( text.substr( 0, expected ), "utf8" ).length.should.be.lessThanOrEqual( limit );
			} );
		} );

		it( "handles Javascript wrongly exposing second surrogate in representation of single supplementary code point", () => {
			const data = [
				[ "a𐄂b", 6, Infinity ],
				[ "a𐄂b", 5, 3 ], // the inner supplementary code point consumes two codepoints (surrogates) in Javascript
				[ "a𐄂b", 4, 1 ],
				[ "a𐄂b", 3, 1 ],
				[ "a𐄂b", 2, 1 ],
				[ "a𐄂b", 1, 1 ],
			];

			data.forEach( ( [ text, limit, expected ] ) => {
				TypeString.limitUnicodeOctets( text, limit ).should.be.equal( expected );
				Buffer.from( text.substr( 0, expected ), "utf8" ).length.should.be.lessThanOrEqual( limit );
			} );
		} );
	} );

	describe( "supports limit on number of encoded octets which", () => {
		it( "can be defined on construction", () => {
			Types.forEach( to => {
				new TypeString( to, "foobar", 5 ).octetLimit.should.be.equal( 5 );
			} );
		} );

		it( "is applied on construction", () => {
			Types.forEach( to => {
				new TypeString( to, "foobar", 5 ).value.should.be.equal( "fooba" );
			} );
		} );

		it( "is obeyed on adjustment", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "foobar", 5 );
				value.value = "barfoo";
				value.value.should.be.equal( "barfo" );
			} );
		} );

		it( "can be reduced", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "", 5 );
				value.octetLimit.should.be.equal( 5 );
				value.octetLimit = 4;
				value.octetLimit.should.be.equal( 4 );
			} );
		} );

		it( "can be re-assigned", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "", 5 );
				value.octetLimit.should.be.equal( 5 );
				value.octetLimit = 5;
				value.octetLimit.should.be.equal( 5 );
			} );
		} );

		it( "must not be increased", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "", 5 );
				value.octetLimit.should.be.equal( 5 );
				( () => { value.octetLimit = 6; } ).should.throw();
				value.octetLimit.should.be.equal( 5 );
			} );
		} );

		it( "is applied on reduction instantly", () => {
			Types.forEach( to => {
				const value = new TypeString( to, "foobar", 5 );
				value.value.should.be.equal( "fooba" );
				value.octetLimit = 4;
				value.value.should.be.equal( "foob" );
			} );
		} );
	} );
} );
