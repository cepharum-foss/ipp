/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const TypeString = require( "../../../../lib/types/string/string" );
const { TypeOctetString, TypeBoolean, TypeInteger, TypeEnum, AttributeType } = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );

const { octetString, textWithoutLanguage, keyword, nameWithoutLanguage, uri, uriScheme, charset, naturalLanguage, mimeMediaType } = AttributeType;
const Types = [ textWithoutLanguage, nameWithoutLanguage, keyword, uri, uriScheme, charset, naturalLanguage, mimeMediaType ];


describe( "A octetString-type value", () => {
	it( "can be instantiated", () => {
		new TypeOctetString( "a" ).should.have.property( "value" ).which.is.deepEqual( Buffer.from( "a" ) );
		new TypeOctetString( "hello" ).should.have.property( "value" ).which.is.deepEqual( Buffer.from( "hello" ) );
	} );

	it( "exposes proper type identifier", () => {
		new TypeOctetString( "a" ).should.have.property( "type" ).which.is.equal( octetString );
	} );

	it( "is derived from common type implementation", () => {
		new TypeOctetString( "a" ).should.be.instanceOf( Type );
	} );

	it( "can be instantiated with empty string", () => {
		new TypeOctetString( "" ).value.should.be.deepEqual( Buffer.alloc( 0 ) );
	} );

	it( "accepts values containing uppercase letters", () => {
		new TypeOctetString( "FooBar" ).value.should.be.deepEqual( Buffer.from( "FooBar" ) );
		new TypeOctetString( "foobar" ).value.should.be.deepEqual( Buffer.from( "foobar" ) );
	} );

	it( "accepts values containing non-ASCII characters", () => {
		new TypeOctetString( "überhöht" ).value.should.be.deepEqual( Buffer.from( "überhöht" ) );
		new TypeOctetString( "ü" ).value.should.be.deepEqual( Buffer.from( "ü" ) );
		new TypeOctetString( "fóôbÂr" ).value.should.be.deepEqual( Buffer.from( "fóôbÂr" ) );
	} );

	it( "accepts values containing inner whitespace", () => {
		new TypeOctetString( "foo bar" ).value.should.be.deepEqual( Buffer.from( "foo bar" ) );
		new TypeOctetString( "foobar" ).value.should.be.deepEqual( Buffer.from( "foobar" ) );
	} );

	it( "accepts values containing leading/trailing whitespace", () => {
		new TypeOctetString( " foobar " ).value.should.be.deepEqual( Buffer.from( " foobar " ) );
	} );

	it( "accepts values not starting with latin lowercase letter", () => {
		new TypeOctetString( "1" ).value.should.be.deepEqual( Buffer.from( "1" ) );
		new TypeOctetString( "1a" ).value.should.be.deepEqual( Buffer.from( "1a" ) );
		new TypeOctetString( "-" ).value.should.be.deepEqual( Buffer.from( "-" ) );
		new TypeOctetString( "-a" ).value.should.be.deepEqual( Buffer.from( "-a" ) );
		new TypeOctetString( "_" ).value.should.be.deepEqual( Buffer.from( "_" ) );
		new TypeOctetString( "_a" ).value.should.be.deepEqual( Buffer.from( "_a" ) );
		new TypeOctetString( "." ).value.should.be.deepEqual( Buffer.from( "." ) );
		new TypeOctetString( ".a" ).value.should.be.deepEqual( Buffer.from( ".a" ) );
	} );

	it( "rejects to be instantiated with values cast to string first", () => {
		( () => { new TypeOctetString( true ); } ).should.throw();
		( () => { new TypeOctetString( false ); } ).should.throw();
		( () => { new TypeOctetString( 0 ); } ).should.throw();
		( () => { new TypeOctetString( 1 ); } ).should.throw();
		( () => { new TypeOctetString( -500 ); } ).should.throw();
		( () => { new TypeOctetString( 23.45 ); } ).should.throw();
		( () => { new TypeOctetString( [] ); } ).should.throw();
		( () => { new TypeOctetString( ["foo"] ); } ).should.throw();
		( () => { new TypeOctetString( {} ); } ).should.throw();
		( () => { new TypeOctetString( { value: "foo" } ); } ).should.throw();
		( () => { new TypeOctetString( { toString: () => "foo" } ); } ).should.throw();
		( () => { new TypeOctetString( () => "foo" ); } ).should.throw();
	} );

	it( "can be instantiated with any string-type value", () => {
		Types.forEach( from => {
			new TypeOctetString( new TypeString( from, "a" ) ).value.should.be.deepEqual( Buffer.from( "a" ) );
		} );
	} );

	it( "rejects to be instantiated with integer-type value", () => {
		( () => new TypeOctetString( new TypeInteger( 0 ) ) ).should.throw();
		( () => new TypeOctetString( new TypeInteger( -1234 ) ) ).should.throw();
		( () => new TypeOctetString( new TypeInteger( 5678 ) ) ).should.throw();
	} );

	it( "rejects to be instantiated with boolean-type value", () => {
		( () => new TypeOctetString( new TypeBoolean( false ) ) ).should.throw();
		( () => new TypeOctetString( new TypeBoolean( true ) ) ).should.throw();
	} );

	it( "can be instantiated with enumeration-type value", () => {
		new TypeOctetString( new TypeEnum( 2, [ "foo", "bar" ] ) ).value.should.be.deepEqual( Buffer.from( "bar" ) );
	} );

	it( "accepts custom limits on number of encoded octets", () => {
		new TypeOctetString( "hello", 10 ).octetLimit.should.be.equal( 10 );
	} );

	it( "rejects any limit on number of encoded octets instantly trimming provided value", () => {
		( () => new TypeOctetString( "extraordinary", 10 ) ).should.throw();
		( () => new TypeOctetString( "extraordinary", 11 ) ).should.throw();
		( () => new TypeOctetString( "extraordinary", 12 ) ).should.throw();
		( () => new TypeOctetString( "extraordinary", 13 ) ).should.not.throw();
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeOctetString( "hello", 0 ) ).should.throw();
	} );

	it( "ignores limit on number of encoded octets exceeding some internally defined one", () => {
		new TypeOctetString( "hello", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[octetString] );
	} );

	it( "accepts limit being reduced unless trimming existing value", () => {
		const value = new TypeOctetString( "foobar", 8 );
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.octetLimit.should.be.equal( 8 );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.octetLimit.should.be.equal( 7 );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.octetLimit.should.be.equal( 6 );

		( () => { value.octetLimit = 5; } ).should.throw();
		value.octetLimit.should.be.equal( 6 );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeOctetString( "foobar", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "can be adjusted using string values", () => {
		const value = new TypeOctetString( "a" );

		Types.forEach( from => {
			value.value = new TypeString( from, "foo" );
			value.value.should.be.deepEqual( Buffer.from( "foo" ) );
		} );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.deepEqual( Buffer.from( "bar" ) );
	} );

	it( "rejects to be adjusted using non-string values", () => {
		const value = new TypeOctetString( "a" );

		( () => { value.value = true; } ).should.throw();
		( () => { value.value = false; } ).should.throw();
		( () => { value.value = 1; } ).should.throw();
		( () => { value.value = 0; } ).should.throw();
		( () => { value.value = -300.456; } ).should.throw();
		( () => { value.value = {}; } ).should.throw();
		( () => { value.value = { toString: () => "foo" }; } ).should.throw();
		( () => { value.value = ["bar"]; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
		( () => { value.value = new TypeBoolean( false ); } ).should.throw();
	} );

	it( "accepts empty string or value cast to empty string on adjustment", () => {
		const value = new TypeOctetString( "a" );

		value.value = "";
		value.value.should.be.deepEqual( Buffer.alloc( 0 ) );
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeOctetString( "a" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "accepts strings containing non-ASCII characters on adjustment", () => {
		const value = new TypeOctetString( "foobar" );

		value.value = "überhöht";
		value.value.should.be.deepEqual( Buffer.from( "überhöht" ) );
		value.value = "ü";
		value.value.should.be.deepEqual( Buffer.from( "ü" ) );
		value.value = "Vâlúê";
		value.value.should.be.deepEqual( Buffer.from( "Vâlúê" ) );
	} );

	it( "rejects strings being trimmed on adjustment", () => {
		const value = new TypeOctetString( "foobar", 7 );

		( () => { value.value = "barfoo"; } ).should.not.throw();
		( () => { value.value = "foobarb"; } ).should.not.throw();
		( () => { value.value = "foobarba"; } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value's type is basically same string-type and wrapped values are matching case-sensitively", () => {
			const value = new TypeOctetString( "foo" );

			Types.forEach( from => {
				value.equals( new TypeString( from, "foo" ) ).should.be.false();
				value.equals( new TypeString( from, "foo" ).value ).should.be.true();
				value.equals( new TypeString( from, "FOO" ) ).should.be.false();
				value.equals( new TypeString( from, "FOO" ).value ).should.be.false();
			} );
		} );

		it( "is considered equal when provided value is suitable for casting to TypeOctetString", () => {
			new TypeOctetString( "true" ).equals( "true" ).should.be.true();
			new TypeOctetString( "bar" ).equals( "bar" ).should.be.true();
		} );

		it( "is considered different when provided value is not suitable for casting to TypeOctetString", () => {
			new TypeOctetString( "true" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeOctetString( "true" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeOctetString( "bar" ).equals( ["bar"] ).should.be.false();
			new TypeOctetString( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type suitable for casting to TypeOctetString", () => {
			new TypeOctetString( "bar" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value ).should.be.true();
		} );

		it( "is considered different when comparing with native value wrapped in mismatching type not suitable for casting to TypeOctetString", () => {
			new TypeOctetString( "true" ).equals( new TypeBoolean( true ).value ).should.be.false();
			new TypeOctetString( "true" ).equals( new TypeInteger( 1 ).value ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeOctetString( "foo" ).toString.should.be.a.Function();

		new TypeOctetString( "" ).toString().should.be.String().which.is.equal( "[octetString(0) ]" );
		new TypeOctetString( "foo" ).toString().should.be.String().which.is.equal( "[octetString(3) 666f6f]" );
		new TypeOctetString( "foofoofoofoofoofoofoofoo" ).toString().should.be.String().which.is.equal( "[octetString(24) 666f6f666f6f666f6f666f6f666f6f666f6f666f6f666f6f]" );
		new TypeOctetString( "foofoofoofoofoofoofoofoox" ).toString().should.be.String().which.is.equal( "[octetString(25) 666f6f666f6f666f6f666f6f666f6f666f6f666f6f666...]" );
	} );
} );
