/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const { MinInteger, MaxInteger } = require( "../../../../lib/data" );
const {
	TypeRangeOfInteger, TypeTextWithoutLanguage, TypeTextWithLanguage,
	TypeNameWithoutLanguage, TypeNameWithLanguage, TypeInteger, TypeBoolean,
	TypeEnum, AttributeType: { rangeOfInteger }
} = require( "../../../../" );


describe( "A rangeOfInteger-type value", () => {
	it( "can be instantiated", () => {
		new TypeRangeOfInteger( 1, 1 );
	} );

	it( "extends abstract Type", () => {
		new TypeRangeOfInteger( 1, 1 ).should.be.instanceOf( Type );
	} );

	it( "exposes type identifier", () => {
		new TypeRangeOfInteger( 1, 1 ).should.have.property( "type" ).which.is.equal( rangeOfInteger );
	} );

	it( "requires two arguments selecting either end of range on construction", () => {
		( () => new TypeRangeOfInteger() ).should.throw();
		( () => new TypeRangeOfInteger( 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, 1 ) ).should.not.throw();
	} );

	it( "exposes lower end of range in integer property `lower`", () => {
		new TypeRangeOfInteger( 1, 1 ).should.have.property( "lower" ).which.is.a.Number().and.is.equal( 1 );
	} );

	it( "exposes upper end of range in integer property `upper`", () => {
		new TypeRangeOfInteger( 1, 2 ).should.have.property( "upper" ).which.is.a.Number().and.is.equal( 2 );
	} );

	it( "implicitly flips values when defining range in wrong order", () => {
		new TypeRangeOfInteger( 2, 1 ).upper.should.be.equal( 2 );
		new TypeRangeOfInteger( 2, 1 ).lower.should.be.equal( 1 );
	} );

	it( "converts provided values to integers on construction", () => {
		new TypeRangeOfInteger( "1", "2" ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( "1", "2" ).upper.should.be.equal( 2 );
		new TypeRangeOfInteger( "1dpi", "2dpi" ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( "1dpi", "2dpi" ).upper.should.be.equal( 2 );
	} );

	it( "ignores any decimal part in provided values on construction", () => {
		new TypeRangeOfInteger( 1.2, 2.4 ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( 1.2, 2.4 ).upper.should.be.equal( 2 );
		new TypeRangeOfInteger( "1.2", "2.4" ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( "1.2", "2.4" ).upper.should.be.equal( 2 );
	} );

	it( "rejects values exceeding integer-type range on construction", () => {
		( () => new TypeRangeOfInteger( MinInteger, MinInteger ) ).should.not.throw();
		( () => new TypeRangeOfInteger( MinInteger - 1, MinInteger ) ).should.throw();
		( () => new TypeRangeOfInteger( MinInteger, MinInteger - 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( MinInteger - 1, MinInteger - 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( MaxInteger, MaxInteger ) ).should.not.throw();
		( () => new TypeRangeOfInteger( MaxInteger + 1, MaxInteger ) ).should.throw();
		( () => new TypeRangeOfInteger( MaxInteger, MaxInteger + 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( MaxInteger + 1, MaxInteger + 1 ) ).should.throw();
	} );

	it( "accepts values suitable for casting to integer", () => {
		new TypeRangeOfInteger( new TypeTextWithoutLanguage( "1" ), 2 ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( new TypeTextWithLanguage( "1", "de" ), 2 ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( new TypeNameWithoutLanguage( "1" ), 2 ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( new TypeNameWithLanguage( "1", "de" ), 2 ).lower.should.be.equal( 1 );
		new TypeRangeOfInteger( new TypeInteger( 1 ), 2 ).lower.should.be.equal( 1 );
	} );

	it( "rejects values for rangeOfIntegers not suitable for casting to integer", () => {
		( () => new TypeRangeOfInteger( 1, undefined ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, null ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, false ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, true ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, {} ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, [] ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, () => {} ) ).should.throw();
		( () => new TypeRangeOfInteger( 1, new TypeBoolean( 1 ) ) ).should.throw();

		( () => new TypeRangeOfInteger( undefined, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( null, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( false, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( true, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( {}, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( [], 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( () => {}, 1 ) ).should.throw();
		( () => new TypeRangeOfInteger( new TypeBoolean( 1 ), 1 ) ).should.throw();
	} );

	it( "accepts adjustment of either end of range", () => {
		const value = new TypeRangeOfInteger( 1, 2 );
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 2 );

		value.upper = 3;
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 3 );

		value.upper = " 200dpi ";
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 200 );

		value.lower = [-100];
		value.lower.should.be.equal( -100 );
		value.upper.should.be.equal( 200 );

		value.upper = [ 400, 500 ]; // accepted due to casting to string "400,500" prior to parsing as integer
		value.lower.should.be.equal( -100 );
		value.upper.should.be.equal( 400 );

		value.lower = new TypeRangeOfInteger( -400, 500 );
		value.lower.should.be.equal( -400 );
		value.upper.should.be.equal( 400 );
	} );

	it( "automatically flips adjusted end of range on assigning new limit to wrong end", () => {
		const value = new TypeRangeOfInteger( 1, 2 );
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 2 );

		value.lower = 3;
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 3 );

		value.upper = new TypeRangeOfInteger( -5, -6 );
		value.lower.should.be.equal( -5 );
		value.upper.should.be.equal( 3 );
	} );

	it( "ignores decimal part on adjustment", () => {
		const value = new TypeRangeOfInteger( 1, 2 );
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 2 );

		value.upper = 12.34;
		value.lower.should.be.equal( 1 );
		value.upper.should.be.equal( 12 );
	} );

	it( "rejects values not suitable for casting to integer on adjustment", () => {
		const value = new TypeRangeOfInteger( 1, 2 );

		( () => { value.lower = undefined; } ).should.throw();
		( () => { value.lower = null; } ).should.throw();
		( () => { value.lower = true; } ).should.throw();
		( () => { value.lower = false; } ).should.throw();
		( () => { value.lower = ""; } ).should.throw();
		( () => { value.lower = "some string"; } ).should.throw();
		( () => { value.lower = []; } ).should.throw();
		( () => { value.lower = {}; } ).should.throw();
		( () => { value.lower = { value: 123 }; } ).should.throw();
		( () => { value.lower = function() {}; } ).should.throw();

		( () => { value.upper = undefined; } ).should.throw();
		( () => { value.upper = null; } ).should.throw();
		( () => { value.upper = true; } ).should.throw();
		( () => { value.upper = false; } ).should.throw();
		( () => { value.upper = ""; } ).should.throw();
		( () => { value.upper = "some string"; } ).should.throw();
		( () => { value.upper = {}; } ).should.throw();
		( () => { value.upper = { value: 123 }; } ).should.throw();
		( () => { value.upper = function() {}; } ).should.throw();
	} );

	it( "rejects provided value exceeding range of 32-bit signed integers on adjustment", () => {
		const value = new TypeRangeOfInteger( 1, 2 );

		( () => { value.lower = MinInteger; } ).should.not.throw();
		( () => { value.lower = MinInteger - 1; } ).should.throw();
		( () => { value.lower = MaxInteger; } ).should.not.throw();
		( () => { value.lower = MaxInteger + 1; } ).should.throw();

		( () => { value.upper = MinInteger; } ).should.not.throw();
		( () => { value.upper = MinInteger - 1; } ).should.throw();
		( () => { value.upper = MaxInteger; } ).should.not.throw();
		( () => { value.upper = MaxInteger + 1; } ).should.throw();
	} );

	describe( "can be compared with another value so that it", () => {
		it( "is considered equal when provided value is another rangeOfInteger-type instance exposing same rangeOfIntegers and unit", () => {
			new TypeRangeOfInteger( 1, 2 ).equals( new TypeRangeOfInteger( 1, 2 ) ).should.be.true();
			new TypeRangeOfInteger( 2, 3 ).equals( new TypeRangeOfInteger( "2", "3dpi" ) ).should.be.true();
			new TypeRangeOfInteger( "3", "4dpi" ).equals( new TypeRangeOfInteger( 3, 4 ) ).should.be.true();
		} );

		it( "is considered different as soon as either provided rangeOfInteger value is different", () => {
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeRangeOfInteger( 1, 2 ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeRangeOfInteger( 2, 1 ) ).should.be.false();
			new TypeRangeOfInteger( 2, 3 ).equals( new TypeRangeOfInteger( "3", "3dpi" ) ).should.be.false();
			new TypeRangeOfInteger( "3", "5dpi" ).equals( new TypeRangeOfInteger( 3, 4 ) ).should.be.false();
		} );

		it( "is considered different when comparing with any other type of value", () => {
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeTextWithLanguage( "1", "de" ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeTextWithoutLanguage( "1" ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeNameWithLanguage( "1", "de" ) ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( new TypeNameWithoutLanguage( "1" ) ).should.be.false();
		} );

		it( "can't be compared with enumeration-value for those reject to cover supported values", () => {
			( () => new TypeRangeOfInteger( 1, 1 ).equals( new TypeEnum( 1, ["1"] ) ) ).should.throw();
		} );

		it( "is considered different when comparing with native values", () => {
			new TypeRangeOfInteger( 1, 1 ).equals( undefined ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( null ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( 0 ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( 1 ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( false ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( true ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( "1" ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( [] ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( [1] ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( ["1"] ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( {} ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( { value: 1 } ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( { value: "1" } ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( { toString: () => 1 } ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( { toString: () => "1" } ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( () => 1 ).should.be.false();
			new TypeRangeOfInteger( 1, 1 ).equals( () => "1" ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeRangeOfInteger( 1, 2 ).toString.should.be.a.Function();

		new TypeRangeOfInteger( 1, 1 ).toString().should.be.String().which.is.equal( "[rangeOfInteger 1-1]" );
		new TypeRangeOfInteger( 1, 2 ).toString().should.be.String().which.is.equal( "[rangeOfInteger 1-2]" );
		new TypeRangeOfInteger( 20, -20 ).toString().should.be.String().which.is.equal( "[rangeOfInteger -20-20]" );
	} );
} );
