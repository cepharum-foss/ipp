/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const { MaxInteger } = require( "../../../../lib/data" );
const {
	TypeResolution, TypeTextWithoutLanguage, TypeTextWithLanguage,
	TypeNameWithoutLanguage, TypeNameWithLanguage, TypeInteger, TypeBoolean,
	TypeEnum, AttributeType: { resolution }
} = require( "../../../../" );


describe( "A resolution-type value", () => {
	it( "can be instantiated", () => {
		new TypeResolution( 1 );
	} );

	it( "extends abstract Type", () => {
		new TypeResolution( 1 ).should.be.instanceOf( Type );
	} );

	it( "exposes type identifier", () => {
		new TypeResolution( 1 ).should.have.property( "type" ).which.is.equal( resolution );
	} );

	it( "requires at least one argument on construction", () => {
		( () => new TypeResolution() ).should.throw();
	} );

	it( "exposes horizontal resolution in integer property `x`", () => {
		new TypeResolution( 1 ).should.have.property( "x" ).which.is.a.Number().and.is.equal( 1 );
	} );

	it( "exposes vertical resolution in integer property `x`", () => {
		new TypeResolution( 1, 2 ).should.have.property( "y" ).which.is.a.Number().and.is.equal( 2 );
	} );

	it( "exposes unit for either resolution in property `unit`", () => {
		new TypeResolution( 1, 2 ).should.have.property( "unit" ).which.is.equal( TypeResolution.PerInch );
	} );

	it( "adopts values in sole provided resolution-type value provided on construction", () => {
		new TypeResolution( new TypeResolution( 1, 2, TypeResolution.PerCm ) ).x.should.be.equal( 1 );
		new TypeResolution( new TypeResolution( 1, 2, TypeResolution.PerCm ) ).y.should.be.equal( 2 );
		new TypeResolution( new TypeResolution( 1, 2, TypeResolution.PerCm ) ).unit.should.be.equal( TypeResolution.PerCm );
	} );

	it( "converts resolution values to integers on construction", () => {
		new TypeResolution( "1", "2" ).x.should.be.equal( 1 );
		new TypeResolution( "1", "2" ).y.should.be.equal( 2 );
		new TypeResolution( "1dpi", "2dpi" ).x.should.be.equal( 1 );
		new TypeResolution( "1dpi", "2dpi" ).y.should.be.equal( 2 );
	} );

	it( "ignores decimal part provided in resolution values on construction", () => {
		new TypeResolution( 1.2, 2.4, TypeResolution.PerCm ).x.should.be.equal( 1 );
		new TypeResolution( 1.2, 2.4, TypeResolution.PerCm ).y.should.be.equal( 2 );
		new TypeResolution( "1.2", "2.4", TypeResolution.PerCm ).x.should.be.equal( 1 );
		new TypeResolution( "1.2", "2.4", TypeResolution.PerCm ).y.should.be.equal( 2 );
	} );

	it( "ignores units provided in resolution values converted to integers on construction", () => {
		new TypeResolution( "1dpi", "2dpi", TypeResolution.PerCm ).unit.should.be.equal( TypeResolution.PerCm );
	} );

	it( "defaults vertical resolution to horizontal one when omitted on creation", () => {
		new TypeResolution( 1 ).y.should.be.equal( 1 );
		new TypeResolution( 2 ).y.should.be.equal( 2 );
	} );

	it( "rejects non-positive resolution values on construction", () => {
		( () => new TypeResolution( 1, 0 ) ).should.throw();
		( () => new TypeResolution( 0, 1 ) ).should.throw();
		( () => new TypeResolution( 0, 0 ) ).should.throw();
		( () => new TypeResolution( 1, -1 ) ).should.throw();
		( () => new TypeResolution( -1, 1 ) ).should.throw();
		( () => new TypeResolution( -1, -1 ) ).should.throw();
	} );

	it( "is limited to 32-bit signed integer values for resolution values provided on construction", () => {
		( () => new TypeResolution( MaxInteger, MaxInteger ) ).should.not.throw();
		( () => new TypeResolution( MaxInteger + 1, MaxInteger ) ).should.throw();
		( () => new TypeResolution( MaxInteger, MaxInteger + 1 ) ).should.throw();
	} );

	it( "accepts unit selected in third argument on construction", () => {
		( () => new TypeResolution( 1, 1, TypeResolution.PerInch ) ).should.not.throw();
		( () => new TypeResolution( 1, 1, TypeResolution.PerCm ) ).should.not.throw();
	} );

	it( "rejects invalid values for selecting unit in third argument on construction", () => {
		( () => new TypeResolution( 1, 1, "inch" ) ).should.throw();
		( () => new TypeResolution( 1, 1, "cm" ) ).should.throw();
		( () => new TypeResolution( 1, 1, 0 ) ).should.throw();
		( () => new TypeResolution( 1, 1, true ) ).should.throw();
		( () => new TypeResolution( 1, 1, false ) ).should.throw();
		( () => new TypeResolution( 1, 1, "" ) ).should.throw();
		( () => new TypeResolution( 1, 1, 1 ) ).should.throw();
	} );

	it( "accepts values for resolutions suitable for casting to integer", () => {
		new TypeResolution( new TypeTextWithoutLanguage( "1" ) ).x.should.be.equal( 1 );
		new TypeResolution( new TypeTextWithLanguage( "1", "de" ) ).x.should.be.equal( 1 );
		new TypeResolution( new TypeNameWithoutLanguage( "1" ) ).x.should.be.equal( 1 );
		new TypeResolution( new TypeNameWithLanguage( "1", "de" ) ).x.should.be.equal( 1 );
		new TypeResolution( new TypeInteger( 1 ) ).x.should.be.equal( 1 );
	} );

	it( "rejects values for resolutions not suitable for casting to integer", () => {
		( () => new TypeResolution( undefined ) ).should.throw();
		( () => new TypeResolution( null ) ).should.throw();
		( () => new TypeResolution( false ) ).should.throw();
		( () => new TypeResolution( true ) ).should.throw();
		( () => new TypeResolution( {} ) ).should.throw();
		( () => new TypeResolution( [] ) ).should.throw();
		( () => new TypeResolution( () => {} ) ).should.throw();
		( () => new TypeResolution( new TypeBoolean( 1 ) ) ).should.throw();
	} );

	it( "accepts adjustment of either resolution value", () => {
		const value = new TypeResolution( 1, 2 );
		value.x.should.be.equal( 1 );
		value.y.should.be.equal( 2 );

		value.x = 3;
		value.x.should.be.equal( 3 );
		value.y.should.be.equal( 2 );

		value.y = " 200dpi ";
		value.x.should.be.equal( 3 );
		value.y.should.be.equal( 200 );

		value.x = [300];
		value.x.should.be.equal( 300 );
		value.y.should.be.equal( 200 );

		value.x = new TypeResolution( 100, 400 );
		value.x.should.be.equal( 100 );
		value.y.should.be.equal( 200 );

		value.y = new TypeResolution( 150, 450 );
		value.x.should.be.equal( 100 );
		value.y.should.be.equal( 450 );
	} );

	it( "ignores decimal part on adjustment", () => {
		const value = new TypeResolution( 1 );
		value.x.should.be.equal( 1 );
		value.y.should.be.equal( 1 );

		value.x = 12.34;
		value.x.should.be.equal( 12 );
		value.y.should.be.equal( 1 );
	} );

	it( "ignores unit provided on adjustment", () => {
		const value = new TypeResolution( 1, 1, TypeResolution.PerCm );
		value.x.should.be.equal( 1 );
		value.y.should.be.equal( 1 );
		value.unit.should.be.equal( TypeResolution.PerCm );

		value.x = "200dpi";
		value.x.should.be.equal( 200 );
		value.y.should.be.equal( 1 );
		value.unit.should.be.equal( TypeResolution.PerCm );
	} );

	it( "rejects incompatible resolution values on adjustment", () => {
		const value = new TypeResolution( 1, 2 );

		( () => { value.x = undefined; } ).should.throw();
		( () => { value.x = null; } ).should.throw();
		( () => { value.x = true; } ).should.throw();
		( () => { value.x = false; } ).should.throw();
		( () => { value.x = ""; } ).should.throw();
		( () => { value.x = "some string"; } ).should.throw();
		( () => { value.x = []; } ).should.throw();
		( () => { value.x = [-123]; } ).should.throw();
		( () => { value.x = {}; } ).should.throw();
		( () => { value.x = { value: 123 }; } ).should.throw();
		( () => { value.x = function() {}; } ).should.throw();

		( () => { value.y = undefined; } ).should.throw();
		( () => { value.y = null; } ).should.throw();
		( () => { value.y = true; } ).should.throw();
		( () => { value.y = false; } ).should.throw();
		( () => { value.y = ""; } ).should.throw();
		( () => { value.y = "some string"; } ).should.throw();
		( () => { value.y = []; } ).should.throw();
		( () => { value.y = [-123]; } ).should.throw();
		( () => { value.y = {}; } ).should.throw();
		( () => { value.y = { value: 123 }; } ).should.throw();
		( () => { value.y = function() {}; } ).should.throw();
	} );

	it( "rejects either resolution value to exceed range of 32-bit signed integers on adjustment", () => {
		const value = new TypeResolution( 1 );

		( () => { value.x = MaxInteger; } ).should.not.throw();
		( () => { value.x = MaxInteger + 1; } ).should.throw();
		( () => { value.y = MaxInteger; } ).should.not.throw();
		( () => { value.y = MaxInteger + 1; } ).should.throw();
	} );

	it( "accepts adjustment of unit", () => {
		const value = new TypeResolution( 1, 2 );
		value.unit.should.be.equal( TypeResolution.PerInch );

		value.unit = TypeResolution.PerCm;
		value.unit.should.be.equal( TypeResolution.PerCm );

		value.unit = TypeResolution.PerInch;
		value.unit.should.be.equal( TypeResolution.PerInch );

		value.unit = new TypeResolution( 1, 1, TypeResolution.PerCm );
		value.unit.should.be.equal( TypeResolution.PerCm );

		value.unit = new TypeResolution( 1, 1, TypeResolution.PerInch );
		value.unit.should.be.equal( TypeResolution.PerInch );

		value.unit = new TypeInteger( TypeResolution.PerCm );
		value.unit.should.be.equal( TypeResolution.PerCm );
	} );

	it( "rejects adjustment of unit using invalid values", () => {
		const value = new TypeResolution( 1, 2 );
		value.unit.should.be.equal( TypeResolution.PerInch );

		( () => { value.unit = null; } ).should.throw();
		( () => { value.unit = undefined; } ).should.throw();
		( () => { value.unit = ""; } ).should.throw();
		( () => { value.unit = "cm"; } ).should.throw();
		( () => { value.unit = "inch"; } ).should.throw();
		( () => { value.unit = "dpcm"; } ).should.throw();
		( () => { value.unit = "dpi"; } ).should.throw();
		( () => { value.unit = 0; } ).should.throw();
		( () => { value.unit = 1; } ).should.throw();
		( () => { value.unit = false; } ).should.throw();
		( () => { value.unit = true; } ).should.throw();
		( () => { value.unit = []; } ).should.throw();
		( () => { value.unit = [TypeResolution.PerCm]; } ).should.throw();
		( () => { value.unit = [TypeResolution.PerInch]; } ).should.throw();
		( () => { value.unit = {}; } ).should.throw();
		( () => { value.unit = { value: TypeResolution.PerCm }; } ).should.throw();
		( () => { value.unit = { value: TypeResolution.PerInch }; } ).should.throw();
		( () => { value.unit = { unit: TypeResolution.PerCm }; } ).should.throw();
		( () => { value.unit = { unit: TypeResolution.PerInch }; } ).should.throw();
		( () => { value.unit = { toString: () => TypeResolution.PerCm }; } ).should.throw();
		( () => { value.unit = { toString: () => TypeResolution.PerInch }; } ).should.throw();
		( () => { value.unit = () => TypeResolution.PerCm; } ).should.throw();
		( () => { value.unit = () => TypeResolution.PerInch; } ).should.throw();
		( () => { value.unit = new TypeInteger( 1 ); } ).should.throw();
	} );

	describe( "can be compared with another value so that it", () => {
		it( "is considered equal when provided value is another resolution-type instance exposing same resolutions and unit", () => {
			new TypeResolution( 1 ).equals( new TypeResolution( 1 ) ).should.be.true();
			new TypeResolution( 2, 3 ).equals( new TypeResolution( "2", "3dpi" ) ).should.be.true();
			new TypeResolution( "3", "4dpi", TypeResolution.PerCm ).equals( new TypeResolution( 3, 4, TypeResolution.PerCm ) ).should.be.true();
		} );

		it( "is considered different as soon as either provided resolution value is different", () => {
			new TypeResolution( 1 ).equals( new TypeResolution( 2 ) ).should.be.false();
			new TypeResolution( 2, 3 ).equals( new TypeResolution( "3", "3dpi" ) ).should.be.false();
			new TypeResolution( "3", "5dpi", TypeResolution.PerCm ).equals( new TypeResolution( 3, 4, TypeResolution.PerCm ) ).should.be.false();
		} );

		it( "is considered different as soon as unit is different", () => {
			new TypeResolution( "3", "4dpi", TypeResolution.PerInch ).equals( new TypeResolution( 3, 4, TypeResolution.PerCm ) ).should.be.false();
		} );

		it( "is considered different when comparing with any other type of value", () => {
			new TypeResolution( 1 ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeResolution( 1 ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeResolution( 1 ).equals( new TypeTextWithLanguage( "1", "de" ) ).should.be.false();
			new TypeResolution( 1 ).equals( new TypeTextWithoutLanguage( "1" ) ).should.be.false();
			new TypeResolution( 1 ).equals( new TypeNameWithLanguage( "1", "de" ) ).should.be.false();
			new TypeResolution( 1 ).equals( new TypeNameWithoutLanguage( "1" ) ).should.be.false();
		} );

		it( "can't be compared with enumeration-value for those reject to cover supported values", () => {
			( () => new TypeResolution( 1 ).equals( new TypeEnum( 1, ["1"] ) ) ).should.throw();
		} );

		it( "is considered different when comparing with native values", () => {
			new TypeResolution( 1 ).equals( undefined ).should.be.false();
			new TypeResolution( 1 ).equals( null ).should.be.false();
			new TypeResolution( 1 ).equals( 0 ).should.be.false();
			new TypeResolution( 1 ).equals( 1 ).should.be.false();
			new TypeResolution( 1 ).equals( false ).should.be.false();
			new TypeResolution( 1 ).equals( true ).should.be.false();
			new TypeResolution( 1 ).equals( "1" ).should.be.false();
			new TypeResolution( 1 ).equals( [] ).should.be.false();
			new TypeResolution( 1 ).equals( [1] ).should.be.false();
			new TypeResolution( 1 ).equals( ["1"] ).should.be.false();
			new TypeResolution( 1 ).equals( {} ).should.be.false();
			new TypeResolution( 1 ).equals( { value: 1 } ).should.be.false();
			new TypeResolution( 1 ).equals( { value: "1" } ).should.be.false();
			new TypeResolution( 1 ).equals( { toString: () => 1 } ).should.be.false();
			new TypeResolution( 1 ).equals( { toString: () => "1" } ).should.be.false();
			new TypeResolution( 1 ).equals( () => 1 ).should.be.false();
			new TypeResolution( 1 ).equals( () => "1" ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeResolution( 1 ).toString.should.be.a.Function();

		new TypeResolution( 1 ).toString().should.be.String().which.is.equal( "[resolution 1x1dpi]" );
		new TypeResolution( 1, 1 ).toString().should.be.String().which.is.equal( "[resolution 1x1dpi]" );
		new TypeResolution( 1, 2 ).toString().should.be.String().which.is.equal( "[resolution 1x2dpi]" );
		new TypeResolution( 100, 400, TypeResolution.PerCm ).toString().should.be.String().which.is.equal( "[resolution 100x400dpcm]" );
		new TypeResolution( 400, 200, TypeResolution.PerInch ).toString().should.be.String().which.is.equal( "[resolution 400x200dpi]" );
	} );
} );
