/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const {
	TypeNameWithLanguage, TypeTextWithLanguage, TypeNaturalLanguage, TypeBoolean,
	TypeInteger, TypeEnum, TypeTextWithoutLanguage, TypeCharset, TypeMimeMediaType,
	TypeKeyword, TypeNameWithoutLanguage, TypeUriScheme, TypeUri, AttributeType
} = require( "../../../../" );
const { AttributeTypeLimit } = require( "../../../../lib/data" );
const { textWithLanguage } = AttributeType;


describe( "A textWithLanguage-type value", () => {
	it( "can be instantiated", () => {
		new TypeTextWithLanguage( "", "de" ).should.have.property( "text" ).which.is.equal( "" );
		new TypeTextWithLanguage( "hello", "en" ).should.have.property( "text" ).which.is.equal( "hello" );
		new TypeTextWithLanguage( "FooBar", "en" ).should.have.property( "text" ).which.is.equal( "FooBar" );
	} );

	it( "exposes proper type identifier", () => {
		new TypeTextWithLanguage( "", "de" ).should.have.property( "type" ).which.is.equal( textWithLanguage );
	} );

	it( "is derived from common type implementation", () => {
		new TypeTextWithLanguage( "", "de" ).should.be.instanceOf( Type );
	} );

	it( "rejects to be instantiated without second argument providing language tag", () => {
		( () => new TypeTextWithLanguage() ).should.throw();
		( () => new TypeTextWithLanguage( "" ) ).should.throw();
		( () => new TypeTextWithLanguage( "", "" ) ).should.throw();
		( () => new TypeTextWithLanguage( "", "." ) ).should.throw();
		( () => new TypeTextWithLanguage( "", "fóo" ) ).should.throw();

		( () => new TypeTextWithLanguage( "", "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeNaturalLanguage( "foo" ) ) ).should.not.throw();
	} );

	it( "exposes internally managed textWithoutLanguage instance as `value`", () => {
		new TypeTextWithLanguage( "foo", "bar" )
			.should.have.property( "value" )
			.which.is.a.String().and.is.equal( "foo" );
	} );

	it( "exposes internally managed naturalLanguage instance as `language`", () => {
		new TypeTextWithLanguage( "foo", "bar" )
			.should.have.property( "language" )
			.which.is.a.String().and.is.equal( "bar" );
	} );

	it( "exposes value of internally managed textWithoutLanguage instance as `text`", () => {
		new TypeTextWithLanguage( "foo", "bar" )
			.should.have.property( "text" )
			.which.is.a.String().and.is.equal( "foo" );
	} );

	it( "accepts any value suitable for creating textWithoutLanguage in first argument on construction", () => {
		( () => new TypeTextWithLanguage( "", "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "a", "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( [], "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( ["a"], "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( {}, "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( { toString: () => "a" }, "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( 1, "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( -4, "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( true, "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( false, "foo" ) ).should.not.throw();

		( () => new TypeTextWithLanguage( new TypeInteger( 0 ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeInteger( 1 ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeInteger( -23.456 ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeBoolean( true ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeBoolean( false ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeEnum( 2, [ "foo", "bar" ] ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeCharset( "utf-8" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeKeyword( "a" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeTextWithoutLanguage( "" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeNameWithoutLanguage( "a" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeUri( "urn:1" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeUriScheme( "ipp" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeNaturalLanguage( "de" ), "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( new TypeMimeMediaType( "text/html" ), "foo" ) ).should.not.throw();
	} );

	it( "rejects any value not suitable for creating textWithoutLanguage in first argument on construction", () => {
		( () => new TypeTextWithLanguage( () => "", "foo" ) ).should.throw();
		( () => new TypeTextWithLanguage( null, "foo" ) ).should.throw();
		( () => new TypeTextWithLanguage( undefined, "foo" ) ).should.throw();
	} );

	it( "accepts any value suitable for creating naturalLanguage in second argument on construction", () => {
		( () => new TypeTextWithLanguage( "", "foo" ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", true ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", false ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", { toString: () => "foo" } ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeCharset( "foo" ) ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeKeyword( "foo" ) ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeTextWithoutLanguage( "foo" ) ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeNameWithoutLanguage( "foo" ) ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeUriScheme( "foo" ) ) ).should.not.throw();
		( () => new TypeTextWithLanguage( "", new TypeNaturalLanguage( "foo" ) ) ).should.not.throw();
	} );

	it( "rejects any value not suitable for creating naturalLanguage in second argument on construction", () => {
		( () => new TypeTextWithLanguage( "", "" ) ).should.throw();
		( () => new TypeTextWithLanguage( "", null ) ).should.throw();
		( () => new TypeTextWithLanguage( "", undefined ) ).should.throw();
		( () => new TypeTextWithLanguage( "", 0 ) ).should.throw();
		( () => new TypeTextWithLanguage( "", -1 ) ).should.throw();
		( () => new TypeTextWithLanguage( "", 34.56 ) ).should.throw();
		( () => new TypeTextWithLanguage( "", () => "foo" ) ).should.throw();
		( () => new TypeTextWithLanguage( "", new TypeUri( "foo:1" ) ) ).should.throw();
		( () => new TypeTextWithLanguage( "", new TypeMimeMediaType( "foo/bar" ) ) ).should.throw();
	} );

	it( "accepts optionally provided limit on number of encoded octets applied on internally managed textWithoutLanguage", () => {
		new TypeTextWithLanguage( "", "de", 10 ).should.have.property( "octetLimit" ).which.is.equal( 10 );
	} );

	it( "instantly trimming value provided on construction to limit on number of encoded octets", () => {
		const value = new TypeTextWithLanguage( "", "de", 3 );

		value.should.have.property( "octetLimit" ).which.is.equal( 3 );

		value.value = "adjusted";
		value.value.should.be.equal( "adj" );
		value.text.should.be.equal( "adj" );

		value.text = "long";
		value.value.should.be.equal( "lon" );
		value.text.should.be.equal( "lon" );
	} );

	it( "accepts reduction of limit on number of encoded octets", () => {
		const value = new TypeTextWithLanguage( "", "de", 10 );

		value.should.have.property( "octetLimit" ).which.is.equal( 10 );

		value.octetLimit = 9;

		value.should.have.property( "octetLimit" ).which.is.equal( 9 );
	} );

	it( "rejects number of octets limited to 0", () => {
		( () => new TypeTextWithLanguage( "hello","de", 0 ) ).should.throw();
	} );

	it( "acceots number of octets limited to 1", () => {
		( () => new TypeTextWithLanguage( "hello", "de", 1 ) ).should.not.throw();
	} );

	it( "ignores limits on number of encoded octets exceeding some internally defined one", () => {
		new TypeTextWithLanguage( "hello", "de", 10000 ).octetLimit.should.be.equal( AttributeTypeLimit[textWithLanguage] );
	} );

	it( "accepts limit being reduced", () => {
		const value = new TypeTextWithLanguage( "überhöht", "de", 9 );
		value.octetLimit.should.be.equal( 9 );
		value.value.should.be.equal( "überhöh" );
		value.text.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 9; } ).should.not.throw();
		value.value.should.be.equal( "überhöh" );
		value.text.should.be.equal( "überhöh" );

		( () => { value.octetLimit = 8; } ).should.not.throw();
		value.value.should.be.equal( "überhö" );
		value.text.should.be.equal( "überhö" );

		( () => { value.octetLimit = 7; } ).should.not.throw();
		value.value.should.be.equal( "überh" );
		value.text.should.be.equal( "überh" );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.value.should.be.equal( "überh" );
		value.text.should.be.equal( "überh" );
	} );

	it( "rejects limit being increased", () => {
		const value = new TypeTextWithLanguage( "überhöht", "de", 9 );
		value.octetLimit.should.be.equal( 9 );

		for ( let i = 10; i < 2048; i++ ) {
			( () => { value.octetLimit = i; } ).should.throw();
		}
	} );

	it( "applies limit to text, only (ignoring limit on language)", () => {
		const value = new TypeTextWithLanguage( "überhöht", "de-deu-de-x-12345", 9 );
		value.octetLimit.should.be.equal( 9 );
		value.language.should.be.equal( "de-deu-de-x-12345" );

		( () => { value.octetLimit = 6; } ).should.not.throw();
		value.value.should.be.equal( "überh" );
		value.language.should.be.equal( "de-deu-de-x-12345" );
	} );

	it( "can be adjusted with values supported by inner textWithoutLanguage", () => {
		const value = new TypeTextWithLanguage( "", "de" );

		value.value = 1;
		value.value.should.be.equal( "1" );
		value.value = -256;
		value.value.should.be.equal( "-256" );
		value.value = true;
		value.value.should.be.equal( "true" );
		value.value = false;
		value.value.should.be.equal( "false" );
		value.value = {};
		value.value.should.be.equal( "[object Object]" );
		value.value = { toString: () => "foo" };
		value.value.should.be.equal( "foo" );
		value.value = [];
		value.value.should.be.equal( "" );
		value.value = [ "foo", "bar" ];
		value.value.should.be.equal( "foo,bar" );

		value.value = new TypeCharset( "utf-8" );
		value.value.should.be.equal( "utf-8" );

		value.value = new TypeKeyword( "bar" );
		value.value.should.be.equal( "bar" );

		value.value = new TypeMimeMediaType( "text/html" );
		value.value.should.be.equal( "text/html" );

		value.value = new TypeUri( "urn:1" );
		value.value.should.be.equal( "urn:1" );

		value.value = new TypeUriScheme( "ipp" );
		value.value.should.be.equal( "ipp" );

		value.value = new TypeNaturalLanguage( "de" );
		value.value.should.be.equal( "de" );

		value.value = new TypeInteger( 12345 );
		value.value.should.be.equal( "12345" );

		value.value = new TypeBoolean( false );
		value.value.should.be.equal( "false" );

		value.value = new TypeEnum( 2, [ "foo", "bar" ] );
		value.value.should.be.equal( "bar" );
	} );

	it( "rejects null-ish value on adjustment", () => {
		const value = new TypeTextWithLanguage( "", "de" );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
	} );

	it( "accepts attached language to be adjusted with values supported by inner nameWithoutLanguage", () => {
		const value = new TypeTextWithLanguage( "", "de" );

		value.language = true;
		value.language.should.be.equal( "true" );
		value.language = false;
		value.language.should.be.equal( "false" );
		value.language = { toString: () => "foo" };
		value.language.should.be.equal( "foo" );

		value.language = new TypeCharset( "utf-abc" );
		value.language.should.be.equal( "utf-abc" );

		value.language = new TypeKeyword( "bar" );
		value.language.should.be.equal( "bar" );

		value.language = new TypeUriScheme( "ipp" );
		value.language.should.be.equal( "ipp" );

		value.language = new TypeNaturalLanguage( "de" );
		value.language.should.be.equal( "de" );

		value.language = new TypeBoolean( false );
		value.language.should.be.equal( "false" );

		value.language = new TypeEnum( 2, [ "foo", "bar" ] );
		value.language.should.be.equal( "bar" );
	} );

	it( "rejects value not supported by inner nameWithoutLanguage on adjustment", () => {
		const value = new TypeTextWithLanguage( "", "de" );

		( () => { value.language = ""; } ).should.throw();
		( () => { value.language = []; } ).should.throw();
		( () => { value.language = undefined; } ).should.throw();
		( () => { value.language = null; } ).should.throw();
		( () => { value.language = [ "foo", "bar" ]; } ).should.throw();
		( () => { value.language = 1; } ).should.throw();
		( () => { value.language = -256; } ).should.throw();
		( () => { value.language = {}; } ).should.throw();
		( () => { value.language = new TypeMimeMediaType( "text/html" ); } ).should.throw();
		( () => { value.language = new TypeUri( "urn:1" ); } ).should.throw();
		( () => { value.language = new TypeInteger( 12345 ); } ).should.throw();
	} );

	describe( "supports comparing with separate value which", () => {
		it( "is considered equal when provided value is another instance of same type with same value and same language", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeTextWithLanguage( "foo", "en" ) ).should.be.false();
			value.equals( new TypeTextWithLanguage( "foo", "de" ) ).should.be.true();
		} );

		it( "is considered different when provided value is mismatching case-sensitively", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeTextWithLanguage( "FOO", "de" ) ).should.be.false();
			value.equals( new TypeTextWithLanguage( "Foo", "de" ) ).should.be.false();
			value.equals( new TypeTextWithLanguage( "foO", "de" ) ).should.be.false();
			value.equals( new TypeTextWithLanguage( "foo", "de" ) ).should.be.true();
		} );

		it( "is considered different when provided value comes without language attached", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeTextWithoutLanguage( "foo" ) ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "foo" ).value ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "FOO" ) ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "FOO" ).value ).should.be.false();
		} );

		it( "is considered equal when provided value comes without language attached but given default language is matching", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeTextWithoutLanguage( "foo" ), { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeTextWithoutLanguage( "foo" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeTextWithoutLanguage( "FOO" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "FOO" ).value, { defaultLanguage: "de" } ).should.be.false();
		} );

		it( "is considered different when provided value comes without language attached and given default language is mismatching", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeTextWithoutLanguage( "foo" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "foo" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "FOO" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeTextWithoutLanguage( "FOO" ).value, { defaultLanguage: "en" } ).should.be.false();
		} );

		it( "is considered different when provided value is a name", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeNameWithoutLanguage( "foo" ) ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "FOO" ) ).should.be.false();

			value.equals( new TypeNameWithLanguage( "foo", "de" ) ).should.be.false();
			value.equals( new TypeNameWithLanguage( "FOO", "de" ) ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "foo" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "FOO" ), { defaultLanguage: "de" } ).should.be.false();

			value.equals( new TypeNameWithLanguage( "foo", "en" ) ).should.be.false();
			value.equals( new TypeNameWithLanguage( "FOO", "en" ) ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "foo" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "FOO" ), { defaultLanguage: "en" } ).should.be.false();
		} );

		it( "is considered different when providing a name's wrapped value matching case-sensitively in context of mismatching default language", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeNameWithoutLanguage( "foo" ).value ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "FOO" ).value ).should.be.false();

			value.equals( new TypeNameWithoutLanguage( "foo" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNameWithoutLanguage( "FOO" ).value, { defaultLanguage: "en" } ).should.be.false();
		} );

		it( "is considered equal when providing a name's wrapped value matching case-sensitively in context of smatching default language", () => {
			const value = new TypeTextWithLanguage( "foo", "de" );

			value.equals( new TypeNameWithoutLanguage( "foo" ).value, { defaultLanguage: "de-de" } ).should.be.true();
			value.equals( new TypeNameWithoutLanguage( "FOO" ).value, { defaultLanguage: "de-de" } ).should.be.false();
		} );

		it( "never matches a `keyword` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "foo", "de-deu-de" );

			value.equals( new TypeKeyword( "foo" ) ).should.be.false();
			value.equals( new TypeKeyword( "foo" ).value ).should.be.false();
			value.equals( new TypeKeyword( "foo" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeKeyword( "foo" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeKeyword( "foo" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeKeyword( "foo" ).value, { defaultLanguage: "de" } ).should.be.true();
		} );

		it( "never matches a `charset` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "foo", "de-deu-de" );

			value.equals( new TypeCharset( "foo" ) ).should.be.false();
			value.equals( new TypeCharset( "foo" ).value ).should.be.false();
			value.equals( new TypeCharset( "FOO" ) ).should.be.false();
			value.equals( new TypeCharset( "FOO" ).value ).should.be.false();
			value.equals( new TypeCharset( "foo" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeCharset( "foo" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeCharset( "FOO" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeCharset( "FOO" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeCharset( "foo" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeCharset( "foo" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeCharset( "FOO" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeCharset( "FOO" ).value, { defaultLanguage: "de" } ).should.be.true(); // value is converted to lowercase by TypeCharset
		} );

		it( "never matches a `naturalLanguage` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "foo", "de-deu-de" );

			value.equals( new TypeNaturalLanguage( "foo" ) ).should.be.false();
			value.equals( new TypeNaturalLanguage( "foo" ).value ).should.be.false();
			value.equals( new TypeNaturalLanguage( "FOO" ) ).should.be.false();
			value.equals( new TypeNaturalLanguage( "FOO" ).value ).should.be.false();
			value.equals( new TypeNaturalLanguage( "foo" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "foo" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "FOO" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "FOO" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "foo" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "foo" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeNaturalLanguage( "FOO" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeNaturalLanguage( "FOO" ).value, { defaultLanguage: "de" } ).should.be.true(); // value is converted to lowercase by TypeNaturalLanguage
		} );

		it( "never matches a `uri` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "urn:a", "de-deu-de" );

			value.equals( new TypeUri( "urn:a" ) ).should.be.false();
			value.equals( new TypeUri( "urn:a" ).value ).should.be.false();
			value.equals( new TypeUri( "URN:a" ) ).should.be.false();
			value.equals( new TypeUri( "URN:a" ).value ).should.be.false();
			value.equals( new TypeUri( "urn:a" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUri( "urn:a" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUri( "URN:a" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUri( "URN:a" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUri( "urn:a" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeUri( "urn:a" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeUri( "URN:a" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeUri( "URN:a" ).value, { defaultLanguage: "de" } ).should.be.true(); // scheme of URL is converted to lowercase by TypeUri
			value.equals( new TypeUri( "urn:A" ).value, { defaultLanguage: "de" } ).should.be.false(); // path of URL isn't converted to lowercase by TypeUri
		} );

		it( "never matches a `uriScheme` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "urn", "de-deu-de" );

			value.equals( new TypeUriScheme( "urn" ) ).should.be.false();
			value.equals( new TypeUriScheme( "urn" ).value ).should.be.false();
			value.equals( new TypeUriScheme( "URN" ) ).should.be.false();
			value.equals( new TypeUriScheme( "URN" ).value ).should.be.false();
			value.equals( new TypeUriScheme( "urn" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUriScheme( "urn" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUriScheme( "URN" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUriScheme( "URN" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeUriScheme( "urn" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeUriScheme( "urn" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeUriScheme( "URN" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeUriScheme( "URN" ).value, { defaultLanguage: "de" } ).should.be.true(); // value is converted to lowercase by TypeUriScheme
		} );

		it( "never matches a `mimeMediaType` unless comparing with its wrapped value in context of matching default language", () => {
			const value = new TypeTextWithLanguage( "text/html", "de-deu-de" );

			value.equals( new TypeMimeMediaType( "text/html" ) ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/html" ).value ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/HTML" ) ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/HTML" ).value ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/html" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/html" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/HTML" ), { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/HTML" ).value, { defaultLanguage: "en" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/html" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/html" ).value, { defaultLanguage: "de" } ).should.be.true();
			value.equals( new TypeMimeMediaType( "text/HTML" ), { defaultLanguage: "de" } ).should.be.false();
			value.equals( new TypeMimeMediaType( "text/HTML" ).value, { defaultLanguage: "de" } ).should.be.false();
		} );

		it( "is considered different when providing value isn't string-typed at all", () => {
			new TypeTextWithLanguage( "true", "de" ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeTextWithLanguage( "1", "de" ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeTextWithLanguage( "bar", "de" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ) ).should.be.false();
		} );

		it( "is considered different when comparing with native value wrapped in mismatching type in context of mismatching default language", () => {
			new TypeTextWithLanguage( "true", "de" ).equals( new TypeBoolean( true ).value, { defaultLanguage: "en" } ).should.be.false();
			new TypeTextWithLanguage( "1", "de" ).equals( new TypeInteger( 1 ).value, { defaultLanguage: "en" } ).should.be.false();
			new TypeTextWithLanguage( "bar", "de" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value, { defaultLanguage: "en" } ).should.be.false();
		} );

		it( "is considered equal when comparing with native value wrapped in mismatching type in context of matching default language", () => {
			new TypeTextWithLanguage( "true", "de" ).equals( new TypeBoolean( true ).value, { defaultLanguage: "de" } ).should.be.true();
			new TypeTextWithLanguage( "1", "de" ).equals( new TypeInteger( 1 ).value, { defaultLanguage: "de" } ).should.be.true();
			new TypeTextWithLanguage( "bar", "de" ).equals( new TypeEnum( 2, [ "foo", "bar" ] ).value, { defaultLanguage: "de" } ).should.be.true();
		} );
	} );

	it( "renders value as string", () => {
		new TypeTextWithLanguage( "foo", "bar" ).toString.should.be.a.Function();

		new TypeTextWithLanguage( "", "bar" ).toString().should.be.String().which.is.equal( "[textWithLanguage(0)  (bar)]" );
		new TypeTextWithLanguage( "foo", "bar" ).toString().should.be.String().which.is.equal( "[textWithLanguage(3) foo (bar)]" );
		new TypeTextWithLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo", "de" ).toString().should.be.String().which.is.equal( "[textWithLanguage(48) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoo (de)]" );
		new TypeTextWithLanguage( "foofoofoofoofoofoofoofoofoofoofoofoofoofoofoofoox", "de" ).toString().should.be.String().which.is.equal( "[textWithLanguage(49) foofoofoofoofoofoofoofoofoofoofoofoofoofoofoo... (de)]" );
	} );
} );
