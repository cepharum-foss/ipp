/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );
require( "should" );

const Type = require( "../../../../lib/types/abstract" );
const {
	TypeDateTime, TypeTextWithoutLanguage, TypeTextWithLanguage,
	TypeNameWithoutLanguage, TypeNameWithLanguage, TypeInteger, TypeBoolean,
	TypeEnum, AttributeType: { dateTime }
} = require( "../../../../" );


describe( "A dateTime-type value", () => {
	it( "can be instantiated", () => {
		new TypeDateTime();
	} );

	it( "extends abstract Type", () => {
		new TypeDateTime().should.be.instanceOf( Type );
	} );

	it( "exposes type identifier", () => {
		new TypeDateTime().should.have.property( "type" ).which.is.equal( dateTime );
	} );

	it( "exposes instance of Date in property `value`", () => {
		new TypeDateTime().should.have.property( "value" ).which.is.instanceOf( Date );
	} );

	it( "represents time of its creation by default", () => {
		( Date.now() - new TypeDateTime().value.getTime() ).should.be.lessThan( 5 );
	} );

	it( "accepts initial timestamp on construction provided as instance of `Date`", () => {
		const now = new Date();

		new TypeDateTime( now ).value.getTime().should.be.equal( now.getTime() );
	} );

	it( "accepts initial timestamp on construction provided as number of _seconds_ since Unix Epoch", () => {
		const now = Math.floor( Date.now() / 1000 );

		new TypeDateTime( now ).value.getTime().should.be.equal( now * 1000 );

		new TypeDateTime( 0 ).value.toISOString().should.be.equal( "1970-01-01T00:00:00.000Z" );
		new TypeDateTime( 1 ).value.toISOString().should.be.equal( "1970-01-01T00:00:01.000Z" );
		new TypeDateTime( 30 ).value.toISOString().should.be.equal( "1970-01-01T00:00:30.000Z" );
		new TypeDateTime( -1 ).value.toISOString().should.be.equal( "1969-12-31T23:59:59.000Z" );
		new TypeDateTime( -20 ).value.toISOString().should.be.equal( "1969-12-31T23:59:40.000Z" );
	} );

	it( "omits milliseconds in numeric value provided on construction", () => {
		const now = Date.now() / 1000;

		new TypeDateTime( now ).value.getTime().should.be.equal( Math.floor( now ) * 1000 );
	} );

	it( "accepts values suitable for casting to `Date` on construction", () => {
		new TypeDateTime( "1" ).value.getTime().should.be.equal( 1000 );
		new TypeDateTime( new TypeInteger( 1 ) ).value.getTime().should.be.equal( 1000 );
		new TypeDateTime( new TypeTextWithoutLanguage( "1" ) ).value.getTime().should.be.equal( 1000 );
		new TypeDateTime( new TypeTextWithLanguage( "1", "de" ) ).value.getTime().should.be.equal( 1000 );
		new TypeDateTime( "2020-01-01T12:34:56Z" ).value.toISOString().should.be.equal( "2020-01-01T12:34:56.000Z" );
		new TypeDateTime( new TypeTextWithoutLanguage( "2020-01-01T12:34:56Z" ) ).value.toISOString().should.be.equal( "2020-01-01T12:34:56.000Z" );
		new TypeDateTime( new TypeTextWithLanguage( "2020-01-01T12:34:56Z", "de" ) ).value.toISOString().should.be.equal( "2020-01-01T12:34:56.000Z" );
	} );

	it( "rejects values not suitable for casting to `Date`", () => {
		( () => new TypeDateTime( false ) ).should.throw();
		( () => new TypeDateTime( true ) ).should.throw();
		( () => new TypeDateTime( {} ) ).should.throw();
		( () => new TypeDateTime( { value: 1 } ) ).should.throw();
		( () => new TypeDateTime( { date: 1 } ) ).should.throw();
		( () => new TypeDateTime( { toString: () => "1" } ) ).should.throw();
		( () => new TypeDateTime( [] ) ).should.throw();
		( () => new TypeDateTime( () => {} ) ).should.throw();
		( () => new TypeDateTime( () => 1 ) ).should.throw();
		( () => new TypeDateTime( new TypeBoolean( 1 ) ) ).should.throw();
	} );

	it( "accepts adjustment of timestamp", () => {
		const now = new Date();

		const value = new TypeDateTime( now );
		value.value.getTime().should.be.equal( now.getTime() );

		value.value = 1;
		value.value.getTime().should.be.equal( 1000 );

		value.value = new TypeInteger( 2 );
		value.value.getTime().should.be.equal( 2000 );

		value.value = new Date( "2020-12-12T13:45:12Z" );
		value.value.toISOString().should.be.equal( "2020-12-12T13:45:12.000Z" );

		value.value = "2020-12-12T13:46:12Z";
		value.value.toISOString().should.be.equal( "2020-12-12T13:46:12.000Z" );

		value.value = new TypeTextWithoutLanguage( "2020-12-12T13:46:13Z" );
		value.value.toISOString().should.be.equal( "2020-12-12T13:46:13.000Z" );

		value.value = new TypeTextWithLanguage( "2020-12-12T13:46:10Z", "de" );
		value.value.toISOString().should.be.equal( "2020-12-12T13:46:10.000Z" );

		value.value = new TypeNameWithoutLanguage( "2020-11-12T13:46:13Z" );
		value.value.toISOString().should.be.equal( "2020-11-12T13:46:13.000Z" );

		value.value = new TypeNameWithLanguage( "2020-11-12T14:46:13Z", "en" );
		value.value.toISOString().should.be.equal( "2020-11-12T14:46:13.000Z" );
	} );

	it( "ignores decimal part on adjustment", () => {
		const value = new TypeDateTime( 1 );
		value.value.getTime().should.be.equal( 1000 );

		value.value = 2.3;
		value.value.getTime().should.be.equal( 2000 );

		value.value = new TypeInteger( 5.4 );
		value.value.getTime().should.be.equal( 5000 );
	} );

	it( "rejects values not suitable for casting to `Date` on adjustment", () => {
		const value = new TypeDateTime( 1 );

		( () => { value.value = undefined; } ).should.throw();
		( () => { value.value = null; } ).should.throw();
		( () => { value.value = true; } ).should.throw();
		( () => { value.value = false; } ).should.throw();
		( () => { value.value = ""; } ).should.throw();
		( () => { value.value = "some string"; } ).should.throw();
		( () => { value.value = []; } ).should.throw();
		( () => { value.value = {}; } ).should.throw();
		( () => { value.value = { value: 123 }; } ).should.throw();
		( () => { value.value = function() {}; } ).should.throw();
	} );

	describe( "can be compared with another value so that it", () => {
		it( "is considered equal when provided value is another dateTime-type instance exposing same dateTimes and unit", () => {
			new TypeDateTime( 1 ).equals( new TypeDateTime( "1" ) ).should.be.true();
			new TypeDateTime( 0 ).equals( new TypeDateTime( "1970-01-01T00:00:00Z" ) ).should.be.true();
			new TypeDateTime( "1970-01-01T00:00:02Z" ).equals( new TypeDateTime( 2 ) ).should.be.true();
		} );

		it( "is considered different as soon as either provided dateTime value is different", () => {
			new TypeDateTime( 1 ).equals( new TypeDateTime( 2 ) ).should.be.false();
			new TypeDateTime( 2 ).equals( new TypeDateTime( "3" ) ).should.be.false();
			new TypeDateTime( "3" ).equals( new TypeDateTime( 4 ) ).should.be.false();
		} );

		it( "is considered different when comparing with any other type of value", () => {
			new TypeDateTime( 1 ).equals( new TypeInteger( 1 ) ).should.be.false();
			new TypeDateTime( 1 ).equals( new TypeBoolean( true ) ).should.be.false();
			new TypeDateTime( 1 ).equals( new TypeTextWithLanguage( "1", "de" ) ).should.be.false();
			new TypeDateTime( 1 ).equals( new TypeTextWithoutLanguage( "1" ) ).should.be.false();
			new TypeDateTime( 1 ).equals( new TypeNameWithLanguage( "1", "de" ) ).should.be.false();
			new TypeDateTime( 1 ).equals( new TypeNameWithoutLanguage( "1" ) ).should.be.false();
		} );

		it( "can't be compared with enumeration-value for those reject to cover supported values", () => {
			( () => new TypeDateTime( 1 ).equals( new TypeEnum( 1, ["1"] ) ) ).should.throw();
			( () => new TypeDateTime( 1 ).equals( new TypeEnum( 1, ["1970-01-01T00:00:01Z"] ) ) ).should.throw();
		} );

		it( "is considered different when comparing with native values", () => {
			new TypeDateTime( 1 ).equals( undefined ).should.be.false();
			new TypeDateTime( 1 ).equals( null ).should.be.false();
			new TypeDateTime( 1 ).equals( 0 ).should.be.false();
			new TypeDateTime( 1 ).equals( 1 ).should.be.false();
			new TypeDateTime( 1 ).equals( false ).should.be.false();
			new TypeDateTime( 1 ).equals( true ).should.be.false();
			new TypeDateTime( 1 ).equals( "1" ).should.be.false();
			new TypeDateTime( 1 ).equals( [] ).should.be.false();
			new TypeDateTime( 1 ).equals( [1] ).should.be.false();
			new TypeDateTime( 1 ).equals( ["1"] ).should.be.false();
			new TypeDateTime( 1 ).equals( {} ).should.be.false();
			new TypeDateTime( 1 ).equals( { value: 1 } ).should.be.false();
			new TypeDateTime( 1 ).equals( { value: "1" } ).should.be.false();
			new TypeDateTime( 1 ).equals( { toString: () => 1 } ).should.be.false();
			new TypeDateTime( 1 ).equals( { toString: () => "1" } ).should.be.false();
			new TypeDateTime( 1 ).equals( () => 1 ).should.be.false();
			new TypeDateTime( 1 ).equals( () => "1" ).should.be.false();
		} );
	} );

	it( "renders value as string", () => {
		new TypeDateTime( 0 ).toString.should.be.a.Function();

		new TypeDateTime( 0 ).toString().should.be.String().which.is.equal( "[dateTime 1970-01-01T00:00:00.000Z]" );
		new TypeDateTime( 1 ).toString().should.be.String().which.is.equal( "[dateTime 1970-01-01T00:00:01.000Z]" );
	} );
} );
