/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { describe, it } = require( "mocha" );

require( "should" );

const Module = require( "../../" );

describe( "Module @cepharum/ipp", () => {
	it( "exposes class `IPPMessage`", () => {
		Module.should.have.property( "IPPMessage" );
		( () => new Module.IPPMessage() ).should.not.throw();
	} );

	it( "exposes class `IPPMessageStreamParser`", () => {
		Module.should.have.property( "IPPMessageStreamParser" );
		( () => new Module.IPPMessageStreamParser() ).should.not.throw();
	} );

	it( "exposes class `GuidedAttributes`", () => {
		Module.should.have.property( "GuidedAttributes" );
		( () => new Module.GuidedAttributes() ).should.not.throw();
	} );

	it( "exposes `AttributeType` collection", () => {
		Module.should.have.property( "AttributeType" ).which.is.an.Object();
		Module.AttributeType.should.have.properties(
			"unsupported",
			"default",
			"unknown",
			"noValue",
			"integer",
			"boolean",
			"enum",
			"octetString",
			"dateTime",
			"resolution",
			"rangeOfInteger",
			"textWithLanguage",
			"nameWithLanguage",
			"textWithoutLanguage",
			"nameWithoutLanguage",
			"keyword",
			"uri",
			"uriScheme",
			"charset",
			"naturalLanguage",
			"mimeMediaType"
		);
	} );

	it( "exposes `Operation` collection", () => {
		Module.should.have.property( "Operation" ).which.is.an.Object();
		Module.Operation.should.have.properties(
			"PrintJob",
			"PrintURI",
			"ValidateJob",
			"CreateJob",
			"GetPrinterAttributes",
			"GetJobs",
			"PausePrinter",
			"ResumePrinter",
			"PurgeJobs",
			"SendDocument",
			"SendURI",
			"CancelJob",
			"GetJobAttributes",
			"HoldJob",
			"ReleaseJob",
			"RestartJob"
		);
	} );

	it( "exposes `Status` collection", () => {
		Module.should.have.property( "Status" ).which.is.an.Object();
		Module.Status.should.have.properties(
			"successfulOk",
			"successfulOkIgnoredOrSubstitutedAttributes",
			"successfulOkConflictingAttributes",
			"clientErrorBadRequest",
			"clientErrorForbidden",
			"clientErrorNotAuthenticated",
			"clientErrorNotAuthorized",
			"clientErrorNotPossible",
			"clientErrorTimeout",
			"clientErrorNotFound",
			"clientErrorGone",
			"clientErrorRequestEntityTooLarge",
			"clientErrorRequestValueTooLong",
			"clientErrorDocumentFormatNotSupported",
			"clientErrorAttributesOrValuesNotSupported",
			"clientErrorUriSchemeNotSupported",
			"clientErrorCharsetNotSupported",
			"clientErrorConflictingAttributes",
			"clientErrorCompressionNotSupported",
			"clientErrorCompressionError",
			"clientErrorDocumentFormatError",
			"clientErrorDocumentAccessError",
			"serverErrorInternalError",
			"serverErrorOperationNotSupported",
			"serverErrorServiceUnavailable",
			"serverErrorVersionNotSupported",
			"serverErrorDeviceError",
			"serverErrorTemporaryError",
			"serverErrorNotAcceptingJobs",
			"serverErrorBusy",
			"serverErrorJobCanceled",
			"serverErrorMultipleDocumentJobsNotSupported"
		);
	} );

	it( "exposes `AttributeGroup` collection", () => {
		Module.should.have.property( "AttributeGroup" ).which.is.an.Object();
	} );

	it( "exposes enumeration of `operations-supported` values as `EnumOperations`", () => {
		Module.should.have.property( "EnumOperations" ).which.is.an.Array();
	} );

	it( "exposes enumeration of `printer-state` values as `EnumPrinterStates`", () => {
		Module.should.have.property( "EnumPrinterStates" ).which.is.an.Array();
	} );

	it( "exposes class `Type`", () => {
		Module.should.have.property( "Type" );
		( () => new Module.Type() ).should.not.throw();
	} );

	it( "exposes class `TypeDefault`", () => {
		Module.should.have.property( "TypeDefault" );
		( () => new Module.TypeDefault() ).should.not.throw();
	} );

	it( "exposes class `TypeUnknown`", () => {
		Module.should.have.property( "TypeUnknown" );
		( () => new Module.TypeUnknown() ).should.not.throw();
	} );

	it( "exposes class `TypeNoValue`", () => {
		Module.should.have.property( "TypeNoValue" );
		( () => new Module.TypeNoValue() ).should.not.throw();
	} );

	it( "exposes class `TypeInteger`", () => {
		Module.should.have.property( "TypeInteger" );
		( () => new Module.TypeInteger( 0 ) ).should.not.throw();
	} );

	it( "exposes class `TypeBoolean`", () => {
		Module.should.have.property( "TypeBoolean" );
		( () => new Module.TypeBoolean( false ) ).should.not.throw();
	} );

	it( "exposes class `TypeEnum`", () => {
		Module.should.have.property( "TypeEnum" );
		( () => new Module.TypeEnum( "foo", [ "foo", "bar" ] ) ).should.not.throw();
	} );

	it( "exposes class `TypeOctetString`", () => {
		Module.should.have.property( "TypeOctetString" );
		( () => new Module.TypeOctetString( Buffer.alloc( 0 ) ) ).should.not.throw();
	} );

	it( "exposes class `TypeDateTime`", () => {
		Module.should.have.property( "TypeDateTime" );
		( () => new Module.TypeDateTime( new Date() ) ).should.not.throw();
	} );

	it( "exposes class `TypeRangeOfInteger`", () => {
		Module.should.have.property( "TypeRangeOfInteger" );
		( () => new Module.TypeRangeOfInteger( 0, 1 ) ).should.not.throw();
	} );

	it( "exposes class `TypeResolution`", () => {
		Module.should.have.property( "TypeResolution" );
		( () => new Module.TypeResolution( 150 ) ).should.not.throw();
	} );

	it( "exposes class `TypeTextWithLanguage`", () => {
		Module.should.have.property( "TypeTextWithLanguage" );
		( () => new Module.TypeTextWithLanguage( "foo", "de" ) ).should.not.throw();
	} );

	it( "exposes class `TypeNameWithLanguage`", () => {
		Module.should.have.property( "TypeNameWithLanguage" );
		( () => new Module.TypeNameWithLanguage( "foo", "de" ) ).should.not.throw();
	} );

	it( "exposes class `TypeTextWithoutLanguage`", () => {
		Module.should.have.property( "TypeTextWithoutLanguage" );
		( () => new Module.TypeTextWithoutLanguage( "foo" ) ).should.not.throw();
	} );

	it( "exposes class `TypeNameWithoutLanguage`", () => {
		Module.should.have.property( "TypeNameWithoutLanguage" );
		( () => new Module.TypeNameWithoutLanguage( "foo" ) ).should.not.throw();
	} );

	it( "exposes class `TypeKeyword`", () => {
		Module.should.have.property( "TypeKeyword" );
		( () => new Module.TypeKeyword( "foo" ) ).should.not.throw();
	} );

	it( "exposes class `TypeUri`", () => {
		Module.should.have.property( "TypeUri" );
		( () => new Module.TypeUri( "foo:" ) ).should.not.throw();
	} );

	it( "exposes class `TypeUriScheme`", () => {
		Module.should.have.property( "TypeUriScheme" );
		( () => new Module.TypeUriScheme( "foo" ) ).should.not.throw();
	} );

	it( "exposes class `TypeCharset`", () => {
		Module.should.have.property( "TypeCharset" );
		( () => new Module.TypeCharset( "utf-8" ) ).should.not.throw();
	} );

	it( "exposes class `TypeNaturalLanguage`", () => {
		Module.should.have.property( "TypeNaturalLanguage" );
		( () => new Module.TypeNaturalLanguage( "de" ) ).should.not.throw();
	} );

	it( "exposes class `TypeMimeMediaType`", () => {
		Module.should.have.property( "TypeMimeMediaType" );
		( () => new Module.TypeMimeMediaType( "text/plain" ) ).should.not.throw();
	} );
} );
