/**
 * (c) 2020 cepharum GmbH, Berlin, http://cepharum.de
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2020 cepharum GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * @author: cepharum
 */

"use strict";

const { Readable } = require( "stream" );

/**
 * Creates readable stream emitting provided buffer in chunks of select size at
 * most.
 *
 * @param {Buffer} message buffer to provide
 * @param {int} chunkSize maximum size of either chunk provided
 * @param {Buffer} payload payload to send after sending IPP message
 * @returns {Readable} readable stream instance
 */
function emitBuffer( message, chunkSize = 1, payload = null ) {
	const numMessageOctets = message.length;
	const numPayloadOctets = payload ? payload.length : 0;
	let cursor = 0;

	return new Readable( {
		read( size ) {
			const push = () => {
				if ( cursor < numMessageOctets ) {
					const pushSize = Math.min( chunkSize, size );
					const chunk = message.slice( cursor, cursor + pushSize );
					const more = this.push( chunk );

					cursor += pushSize;

					if ( more ) {
						setTimeout( push, 5 );
					}
				} else if ( cursor - numMessageOctets < numPayloadOctets ) {
					const pushSize = Math.min( chunkSize, size );
					const chunk = payload.slice( cursor - numMessageOctets, cursor - numMessageOctets + pushSize );
					const more = this.push( chunk );

					cursor += pushSize;

					if ( more ) {
						setTimeout( push, 5 );
					}
				} else {
					this.push( null );
				}
			};

			process.nextTick( push );
		}
	} );
}

/**
 * Consumes provided stream promising all received data.
 *
 * @param {Readable} stream stream to consume
 * @returns {Promise<Buffer>} promises raw data received from stream
 */
function consume( stream ) {
	return new Promise( ( resolve, reject ) => {
		const chunks = [];

		stream.on( "data", chunk => { chunks.push( chunk ); } );
		stream.once( "end", () => {
			resolve( Buffer.concat( chunks ) );
		} );
		stream.once( "error", reject );
	} );
}

module.exports = {
	emitBuffer,
	consume,
};
